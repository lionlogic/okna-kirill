
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/korporativnym-klientam/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:05:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?><div class="main" style="background: url(https://kislorod-ok.ru/wp-content/themes/src/assets/img/corporate/bg.jpg) no-repeat center; background-size: cover;">
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		<?include 'nav.php'?>		

<div class="container">
</div>
	<div class="overlay" id="modal-gift" style="display: none;">
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">
			<div class="title-form title-gift-modal">
				Подарок действует при <br>
				заказе с сайта. <br>
				Заполните форму для <br>
				консультации
			</div>

			<form action="obrob.php" method="POST" onsubmit="yaCounter56025331.reachGoal('zayavka')">
					<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - АКЦИИ ">
				<div class="form-item">
					<!--  -->
					<div class="form-item-block form-item-block-title">
						<div class="form-item-block-num"></div>
						<div class="form-item-block-inp">
							<div class="form-item-block-text">
								Выберите акцию:
							</div>
						</div>
					</div>
					<div class="form-item-block">
						<div class="form-item-block-num n1">1</div>
						<div class="form-item-block-inp">
							<!-- <input type="text" name="typeR" class="input input-sm select-input" required placeholder="Поменять прокладку"> -->

							<select name="DATA[UF_CRM_1572883488181]" class="input input-sm select-input">
																<option value="Набор по уходу за окнами">Набор по уходу за окнами</option>
																<option value="Сумка пляжная с личным логотипом компании">Сумка пляжная с личным логотипом компании</option>
																<option value="Брелок-фонарик с логотипом mosremokna.ru ">Брелок-фонарик с логотипом mosremokna.ru </option>
																<option value=" Ограничитель открывания окна (гребенка) "> Ограничитель открывания окна (гребенка) </option>
																							</select>
						</div>
					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
						<div class="form-item-block-num"></div>
						<div class="form-item-block-inp">
							<div class="form-item-block-text">
								Выберите вид ремонта:
							</div>
						</div>
					</div>
					<div class="form-item-block">
						<div class="form-item-block-num n2">2</div>
						<div class="form-item-block-inp">
							<!-- <input type="text" name='raion' class="input input-sm select-input" required placeholder="Центр"> -->
							<select name="DATA[UF_CRM_1572883588931]" class="input input-sm select-input">
																<option value="Набор по уходу за окнами">Набор по уходу за окнами</option>
																<option value="Сумка пляжная с личным логотипом компании">Сумка пляжная с личным логотипом компании</option>
																<option value="Брелок-фонарик с логотипом mosremokna.ru ">Брелок-фонарик с логотипом mosremokna.ru </option>
																<option value=" Ограничитель открывания окна (гребенка) "> Ограничитель открывания окна (гребенка) </option>
																							</select>
						</div>
					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<div class="form-item-block-num n3">3</div>
						<div class="form-item-block-inp">
							<input type="text" name='DATA[NAME]' class="input input-sm" required placeholder="Ваше имя ">
						</div>
					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<div class="form-item-block-num n4">4</div>
						<div class="form-item-block-inp">
							<input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">
						</div>
					</div>
					<!--  -->

					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<div class="form-item-block-num"></div>
						<div class="form-item-block-inp">
							<button class="btn btn-modal-sm">ХОЧУ ПОДАРОК при РЕМОНТЕ</button>
							<div class="form-small">
								Среднее время ответа 2 минуты
							</div>
						</div>
					</div>
					<!--  -->
				</div>
			</form>
		</div>
	</div>
</div>

<!-- thanks -->
<div class="overlay" id="modal-thanks" style="display: none;">
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">

			<div class="thanks-img">
				<img src="./wp-content/themes/src/assets/img/modal/thanks.png" alt="">
			</div>
			<div class="title-form">
				Спасибо за заявку!
			</div>
			<p>
				Мы прямо сейчас обрабатываем Ваш
				запрос и ответим Вам в ближайшие
				1-10 минут
			</p>
		</div>
	</div>
</div>

<!-- предложение -->

<div class="overlay" id="modal-comer" style="display: none;">
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">
			<div class="title-form">
				Предложение для <br>
				Юридических лиц 
			</div>

			<div class="img-comer">
				<img src="./wp-content/themes/src/assets/img/modal/comer.png" alt="">
			</div>

			<div class="title-form">
				Заполните данные
			</div>

			<form action="obrob.php" method="POST" onsubmit="yaCounter56025331.reachGoal('zayavka')">
				<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - Предложение для ЮРЛИЦ ">
				<div class="form-item">
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">

						<input type="text" name='DATA[UF_CRM_1572883775687]' class="input input-sm" required placeholder="Название организации">
					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">

						<input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">

					</div>
					<!--  -->

					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<button class="btn btn-mod">
							Загрузить предложение
							<span class="btn-small">Pdf 2.1 Mb</span>
						</button>
					</div>
					<div class="checkbox-wrap test-question__checkbox">
						<input type="checkbox" id="no2" class="radio-btn checkbox-inp" value="Обещаем не спамить" name="noSpam" checked>
						<label for="no2" class="radio-label">
							<span class="checkbox-psevdo">
								<span class="checkbox-psevdo-active">
									<img src="./wp-content/themes/src/assets/img/check.png" alt="">
								</span>
							</span>
							Обещаем не спамить
						</label>
					</div>
					<!--  -->
				</div>
			</form>
		</div>
	</div>
</div>

<!-- прайс -->

<div class="overlay" id="modal-price" style="display: none;">
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">
			<div class="title-form">
				Чтобы загрузить прайс
				зарегистрируйтесь в 1 клик
			</div>

			<div class="modal-img">
				<img src="./wp-content/themes/src/assets/img/modal/price.png" alt="">
			</div>

			<div class="date-text modal-date">
				Обновлен: <span class="date-s">17.05.2019</span>
			</div>

			<form action="obrob.php" method="POST" onsubmit="yaCounter56025331.reachGoal('zayavka')">
			    <input type="hidden" name="formname" value="price">
				<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - Прайс ">
				<div class="form-item">
					<!--  -->
					<div class="form-item-block">

						<input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">

					</div>
					<!--  -->

					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<button class="btn btn-mod">
							ЗАГРУЗИТЬ ПРАЙС
							<span class="btn-small">Pdf 2.1 Mb</span>
						</button>
					</div>
					<div class="checkbox-wrap test-question__checkbox">
						<input type="checkbox" id="no3" class="radio-btn checkbox-inp" value="Обещаем не спамить" name="noSpam" checked>
						<label for="no3" class="radio-label">
							<span class="checkbox-psevdo">
								<span class="checkbox-psevdo-active">
									<img src="./wp-content/themes/src/assets/img/check.png" alt="">
								</span>
							</span>
							Обещаем не спамить
						</label>
					</div>
					<!--  -->
				</div>
			</form>
		</div>
	</div>
</div>

<!-- всплывашка -->

<div class="overlay" id="modal-up" style="display: none;">
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">
			<div class="title-form">
				Скачайте БЕСПЛАТНО <br>
				"Рекомендация по уходу за <br>
				окнами"
			</div>

			<div class="modal-img">
				<img src="./wp-content/themes/src/assets/img/modal/up-book.png" alt="">
			</div>

			<div class="title-form">
				Куда отправить
			</div>

			<form action="obrob.php" method="POST" onsubmit="yaCounter56025331.reachGoal('zayavka')">
			<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
			<input type="hidden" name="formname" value="catalog">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - Рекомендации (поп-ап окно) ">
				<div class="form-item">

					<div class="form-item-block">

						<select name="DATA[UF_CRM_1572884339362]" class="input input-sm select-input">
														<option value="WhatsApp">WhatsApp</option>
														<option value="E-mail">E-mail</option>
														<option value="Тел">Тел</option>
																				</select>

					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">

						<input type="text" name='DATA[NAME]' class="input input-sm" required placeholder="Ваше имя">
					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">

						<input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">

					</div>
					<!--  -->

					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<button class="btn btn-mod">
							ПОЛУЧИТЬ бесплатно
							<span class="btn-small">Pdf 2.1 Mb</span>
						</button>
					</div>
					<div class="checkbox-wrap test-question__checkbox">
						<input type="checkbox" id="no4" class="radio-btn checkbox-inp" value="Обещаем не спамить" name="noSpam" checked>
						<label for="no4" class="radio-label">
							<span class="checkbox-psevdo">
								<span class="checkbox-psevdo-active">
									<img src="./wp-content/themes/src/assets/img/check.png" alt="">
								</span>
							</span>
							Обещаем не спамить
						</label>
					</div>
					<!--  -->
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Вызвать мастера -->

<div class="overlay" id="open-master-js" style="display: none;" >
	<div class="modal-wrap">
		<div class="close"></div>

		<div class="big-form">
			<div class="title-form">
				Вызов мастера
			</div>


			<form action="obrob.php" method="POST" onsubmit="yaCounter56025331.reachGoal('zayavka')">
			    	<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - ВЫЗОВ МАСТЕРА (поп-ап на внутр.стр.)">
				<div class="form-item">
													<input type="hidden" name="formname" value="modals-master-easy">

							
					<div class="form-item-block">
						
					<input type="text" name='DATA[NAME]' class="input input-sm" required placeholder="Ваше имя">

					</div>
					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">

						<input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">

					</div>
					<!--  -->

					<!--  -->
					<div class="form-item-block form-item-block-title">
					</div>
					<div class="form-item-block">
						<button class="btn btn-mod">
							Вызвать мастера (бесплатно)
						</button>
					</div>
					<div class="checkbox-wrap test-question__checkbox">
						<input type="checkbox" id="no4" class="radio-btn checkbox-inp" value="Обещаем не спамить" name="noSpam" checked>
						<label for="no4" class="radio-label">
							<span class="checkbox-psevdo">
								<span class="checkbox-psevdo-active">
									<img src="./wp-content/themes/src/assets/img/check.png" alt="">
								</span>
							</span>
							Обещаем не спамить
						</label>
					</div>
					<!--  -->
				</div>
			</form>
		</div>
	</div>
</div>

<!-- video -->

<div class="overlay" id="video-modal" style="display: none;">
	<div class="modal-video">
		<div class="close close-video"></div>
		<div class="modal-video-body"></div>
	</div>
</div>	<!-- svg -->
	<div class="svg-img" style="display: none;">
	<!-- vk -->
	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="12" viewBox="0 0 20 12"><g id='vk'><g><path d="M9.785 11.833h1.196s.36-.04.545-.247c.17-.19.164-.547.164-.547s-.023-1.67.721-1.917c.735-.242 1.677 1.616 2.676 2.33.755.539 1.33.42 1.33.42l2.67-.039s1.398-.089.735-1.232c-.054-.095-.386-.846-1.986-2.396-1.675-1.619-1.45-1.356.567-4.158 1.229-1.705 1.72-2.746 1.566-3.193-.146-.425-1.05-.312-1.05-.312l-3.007.019s-.223-.032-.388.071c-.162.1-.266.337-.266.337s-.476 1.318-1.11 2.442c-1.34 2.369-1.875 2.494-2.094 2.347-.51-.342-.382-1.377-.382-2.112 0-2.297.334-3.253-.651-3.5C10.694.063 10.453.01 9.617 0 8.544-.013 7.636.002 7.122.265 6.779.44 6.515.83 6.676.85c.2.029.65.128.888.465.308.439.297 1.417.297 1.417s.178 2.704-.413 3.04c-.406.23-.962-.24-2.156-2.39-.611-1.1-1.073-2.316-1.073-2.316S4.13.839 3.97.718C3.778.57 3.509.522 3.509.522L.651.542S.222.553.065.75c-.14.172-.012.53-.012.53s2.238 5.452 4.771 8.2c2.324 2.52 4.961 2.354 4.961 2.354z"/></g></g></svg>

	<!-- inst -->
	<svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19"><g id="inst"><g><path d="M19 5.24v8.515a5.249 5.249 0 0 1-5.244 5.242H5.243A5.249 5.249 0 0 1 0 13.755V5.24A5.249 5.249 0 0 1 5.243-.003h8.513A5.249 5.249 0 0 1 19 5.24zm-1.686 8.515V5.24a3.562 3.562 0 0 0-3.558-3.557H5.243A3.562 3.562 0 0 0 1.685 5.24v8.515a3.562 3.562 0 0 0 3.558 3.557h8.513a3.562 3.562 0 0 0 3.558-3.557zm-2.919-4.257A4.9 4.9 0 0 1 9.5 14.392a4.9 4.9 0 0 1-4.896-4.894c0-2.7 2.196-4.896 4.896-4.896 2.7 0 4.895 2.196 4.895 4.896zm-1.685 0c0-1.771-1.44-3.21-3.21-3.21a3.213 3.213 0 0 0-3.21 3.21c0 1.77 1.44 3.209 3.21 3.209s3.21-1.44 3.21-3.21zm2.765-5.964c.23.229.362.549.362.875 0 .324-.132.643-.362.873-.23.23-.548.363-.874.363-.325 0-.644-.133-.874-.363-.23-.23-.363-.549-.363-.873 0-.326.133-.646.363-.875.23-.23.549-.362.874-.362.326 0 .645.132.874.362z"/></g></g></svg>

	<!-- youtube -->
	<svg xmlns="http://www.w3.org/2000/svg" width="19" height="23" viewBox="0 0 19 23"><g id="youtube"><g><path d="M16.071 16.821H14.82l.006-.732c0-.327.265-.593.589-.593h.08c.325 0 .59.266.59.593zm-4.114-1.092v3.567c0 .262-.26.476-.58.476-.317 0-.576-.214-.576-.476v-3.567c0-.264.26-.479.577-.479.318 0 .579.215.579.479zM19 13.256v6.783c0 1.627-1.399 2.958-3.109 2.958H3.108C1.398 22.997 0 21.667 0 20.04v-6.783c0-1.628 1.29-2.258 3-2.258h13c1.71 0 3 .63 3 2.258zM4 20.998v-7h2v-1H1v1h2v7zm4.711-6.304L8 14.998v4c0 .553-.577.34-.612.438-.107.295-.2 1.14-.388.562-.032-.101 0-.476 0-1v-4H6v3c.001.58-.018 1.804 0 2 .032.347-.32.77 0 1 1.47-.13 1.715-.383 2-1v1h1v-6zm4.227 4.37l-.003-3.176c-.001-1.21-.899-1.935-2.117-.956L11 12.999h-1v7.999l.582-.291.099-.508c1.387 1.283 2.259.405 2.257-1.135zm4.134-.421l-.99.005c-.001.04-.003.086-.003.135v.558c0 .3-.244.541-.541.541h-.194a.542.542 0 0 1-.542-.54v-1.467h2.268v-.862c0-.629-.016-1.258-.067-1.618-.163-1.138-1.746-1.319-2.546-.737a1.508 1.508 0 0 0-.555.753c-.113.328-.168.775-.168 1.343v1.894c0 3.148 3.79 2.702 3.338-.005zM11.864 7.836a15.26 15.26 0 0 1-.025-1.087V2.487h1.21v4.917c0 .266.218.484.482.484a.485.485 0 0 0 .483-.484V2.487h1.16v6.318H13.7l.025-.524c-.1.213-.224.372-.37.479a.833.833 0 0 1-.507.158.912.912 0 0 1-.54-.15.906.906 0 0 1-.318-.405 1.87 1.87 0 0 1-.127-.527zM7.528 4.795c0-.607.05-1.08.15-1.422.1-.34.28-.614.541-.82.26-.206.593-.311.999-.311.34 0 .632.068.876.2.245.133.434.305.565.518.134.214.224.433.273.658.05.227.073.571.073 1.034V6.25c0 .586-.023 1.018-.068 1.29-.045.275-.141.53-.29.77a1.383 1.383 0 0 1-.567.524 1.794 1.794 0 0 1-.8.169c-.335 0-.618-.046-.85-.144a1.173 1.173 0 0 1-.544-.438 1.957 1.957 0 0 1-.276-.708c-.056-.277-.082-.693-.082-1.245V4.795zm1.154 2.508c0 .358.263.651.585.651.322 0 .585-.293.585-.65V3.94c0-.357-.263-.65-.585-.65-.322 0-.585.293-.585.65zM4.604 4.16L2.857-.002h1.504l.885 3.092.873-3.083h1.52l-1.642 4.15-.002 4.845H4.606z"/></g></g></svg>
	

	<!-- btn-man -->
	<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"><g id="man-btn"><g><path d="M13.348 24.984c0 .561-.454 1.016-1.014 1.016H3.107A3.088 3.088 0 0 1 .094 22.28a13.254 13.254 0 0 1 4.542-7.503 13.184 13.184 0 0 1 4.251-2.34 6.901 6.901 0 0 1-2.772-5.53C6.115 3.098 9.21 0 13.015 0c3.803 0 6.898 3.098 6.898 6.906s-3.095 6.907-6.899 6.907c-.087 0-.173-.003-.258-.006-5.156.12-9.622 3.82-10.676 8.886-.065.316.013.64.215.89.118.144.379.386.812.386h9.227c.56 0 1.014.454 1.014 1.015zm-.571-13.21l.237-.002c.04 0 .077.003.115.008a4.878 4.878 0 0 0 4.755-4.874 4.878 4.878 0 0 0-4.87-4.874 4.878 4.878 0 0 0-4.87 4.874 4.877 4.877 0 0 0 4.633 4.869zm9.57 8.082c0 .56-.454 1.015-1.014 1.015h-1.47c-.561 0-1.015-.454-1.015-1.015v-2.083a1.015 1.015 0 1 1 2.029 0v1.067h.456c.56 0 1.015.455 1.015 1.016zm3.653 0C26 23.244 23.246 26 19.862 26s-6.138-2.756-6.138-6.144c0-3.389 2.754-6.145 6.138-6.145S26 16.467 26 19.856zm-2.029 0a4.115 4.115 0 0 0-4.109-4.114 4.115 4.115 0 0 0-4.109 4.114 4.116 4.116 0 0 0 4.11 4.113 4.116 4.116 0 0 0 4.108-4.113z"/></g></g></svg>
	<!-- btn рука -->
	<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26"><g id="hand-svg"><g><path d="M23.969 16.758v-4.61c0-.79-.66-.874-.943-.874-.42 0-.647.353-.707.462-.314.57-.766.655-1.007.655-.268 0-.911-.104-1.097-1.064-.031-.16-.047-.312-.05-.329v-.016c-.08-.137-.422-.466-.868-.466a.862.862 0 0 0-.688.353 8.242 8.242 0 0 0-.016.103 4.365 4.365 0 0 1-.059.325c-.156.658-.66.89-1.044.89-.383 0-1.045-.255-1.097-1.214-.021-.388-.412-.715-.854-.715a.875.875 0 0 0-.837.655 1.004 1.004 0 0 1-.026.084v.376a1.015 1.015 0 1 1-2.031 0V2.895a.864.864 0 0 0-1.727 0v12.617c0 1.547-1.099 3.259-2.874 3.327a1.95 1.95 0 0 1-.834-.161L3.337 16.9a1.017 1.017 0 0 0-1.047 1.651l.713.51 4.876 3.041a12.298 12.298 0 0 0 6.514 1.865h5.945a3.621 3.621 0 0 0 3.431-2.443 1.016 1.016 0 0 1 1.923.657 5.652 5.652 0 0 1-5.354 3.817h-5.945c-2.686 0-5.31-.751-7.59-2.173l-4.902-3.058a.955.955 0 0 1-.052-.035l-.814-.581a1.02 1.02 0 0 1-.121-.101A3.024 3.024 0 0 1 0 17.875a3.05 3.05 0 0 1 4.037-2.882c.032.01.063.023.094.037l3.871 1.776c.55-.05.885-.779.885-1.294V2.895A2.898 2.898 0 0 1 11.78 0a2.897 2.897 0 0 1 2.895 2.894v5.464a2.94 2.94 0 0 1 2.908.695c.49-.364 1.089-.57 1.713-.57.704 0 1.405.258 1.973.728.15.123.283.256.399.393a2.745 2.745 0 0 1 1.357-.362c1.723 0 2.974 1.222 2.974 2.905v4.61a1.015 1.015 0 1 1-2.031 0zm-7.46-14.596A1.015 1.015 0 0 1 17.136.87l2.336-.813a1.015 1.015 0 1 1 .667 1.918l-2.336.813a1.012 1.012 0 0 1-1.292-.626zm0 2.735c.185-.53.764-.81 1.293-.625l2.336.812a1.016 1.016 0 0 1-.667 1.919l-2.336-.813a1.015 1.015 0 0 1-.625-1.293zM2.8.683c.184-.53.763-.81 1.292-.626L6.427.87a1.015 1.015 0 1 1-.667 1.918l-2.336-.813A1.015 1.015 0 0 1 2.8.683zm-.007 5.726a1.016 1.016 0 0 1 .65-1.28l2.337-.762a1.016 1.016 0 0 1 .63 1.931l-2.336.762a1.016 1.016 0 0 1-1.28-.651z"/></g></g></svg>
</div>
	<!-- scripts -->
		<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/plugins.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/slick/slick.min.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/fancy/jquery.fancybox.min.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/fancySelect.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/jquery.maskedinput.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/stepper.min.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/sly.min.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/libs/parallax.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/loadmore.js'></script>
<script type='text/javascript' src='./wp-content/themes/src/assets/js/scriptf9e3.js?v=1.1'></script>
<script type='text/javascript' src='./wp-includes/js/wp-embed.min6619.js?ver=5.2.5'></script>


		<script>
			if($('body').find('.contacts').length > 0){
			(function(){
				var offsetHeight = $(".contacts").offset().top - $(window).height();
				var mapFooter = false;
				var urlMap = $('.map-link').data('src');
				mapActivate ();
				 $(window).scroll(function(event) {
				    mapActivate ();
				  });
				function mapActivate (){
				  if(!mapFooter){
				  if($(document).scrollTop() > offsetHeight){
				    $("#map").append('<iframe src="'+ urlMap +'"style="width: 100%; height: 100%;" frameborder="0"></iframe>');
				    mapFooter = true;
				    }
				  }
				}
			}());
			}

			if($('body').find('.first').length > 0){
			  $('.first-btns').on('click', function(e) {
			  	e.preventDefault();
			    var msg = 'first-page=1';
			    var href = $(this).attr('href');
			    var action = './wp-admin/admin-ajaxe2c6.php?action=firstOpen';

			    $.ajax({
			        type: "GET",
			        url: action,
			        data: msg,
			        success: function(data) {
			        	console.log(data);
			        	window.location = href;
			        },
			    });
			  });
			}



			$('form').submit(function(event) {

			    event.preventDefault();

			    var action = "./wp-admin/admin-ajax85ba.php?action=formModal";
			    var msg = $(this).serialize();
			    var formThis = $(this);

			    $.ajax({
			        type: "POST",
			        url: action,
			        data: msg,
			        success: function(data) {


			            if(formThis.find('input[type="hidden"]').val() === "price" ){
			              var link = document.createElement('a');
			              link.setAttribute('href', "./wp-content/uploads/2019/12/kislorod-prai-s.pdf.pdf");
			              link.setAttribute('target', "_blank");
			              link.setAttribute('download','');

			                if(navigator.userAgent.indexOf('Mac') > 0){
			                  
			                  window.location = "./wp-content/uploads/2019/12/kislorod-prai-s.pdf.pdf"
			                }else{
			                  simulate( link, "click");

			                }

			              $(".overlay").fadeOut();
			              $('html').addClass('stop');
			              $("#modal-thanks").fadeIn();

			            }else if(formThis.find('input[type="hidden"]').val() === "catalog" ){
			              var link = document.createElement('a');
			              link.setAttribute('href', "./wp-content/uploads/2019/12/top-10.pdf");
			              link.setAttribute('target', "_blank");
			              link.setAttribute('download','');

			              if(navigator.userAgent.indexOf('Mac') > 0){
			                  window.location = "./wp-content/uploads/2019/12/top-10.pdf";
			                }else{
			                  simulate( link, "click");
			                }


			              $('html').addClass('stop');
			              $(".overlay").fadeOut();
			              $("#modal-thanks").fadeIn();

			            }else if(formThis.find('input[type="hidden"]').val() === "test"){
			              $("#started_the_calculation").fadeIn();
			              $('html').addClass('stop');
			              formThis.find('input').attr({
			                'disabled': 'true',
			              });
			              formThis.find('button').attr({
			                'disabled': 'true',
			              });
			              formThis.find('.input-label').css({'background': 'transparent'});
			              $('.test-prev , .test-next').attr({
			                'disabled': 'true',
			              });
			            }else{
			            	$('html').addClass('stop');
			            	$(".overlay").fadeOut();
			            	$("#modal-thanks").fadeIn();
			            }

			            $('form').trigger('reset');


			        },
			        error: function(xhr, str) {

			            alert('Произошла ошибка, попробуйте немного позже');
			        }
			    });
			});

		</script>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>