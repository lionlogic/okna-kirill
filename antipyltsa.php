
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Антипыльца</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Москитная сетка Антипыль			</h1>
            </div>





            <div itemprop="articleBody">
                <p>Компания «Помощь окнам» предлагает купить изготовленные на заказ москитные сетки от пыли на окна, Антипыль, Антипыльца от пыльцы (Micro mesh), услуги оказываем в Москве и области. Сеточное полотно вставляется в рамочную конструкцию. Рамки из алюминия могут быть различных размеров, цветов и конфигураций.</p>
                <ul class="ul-inline ul-utepalum">
                    <li><div id="sigplus_1001" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/e8aff422be4b3ef9fd378167e1ff6c5e.jpg" title="Обычное полотно для москитной сетки (1/1)" data-summary="Обычное полотно для москитной сетки (1/1)" data-fancybox="sigplus_1001"><img class="sigplus-preview" src="<?=$pathToImg?>/e8aff422be4b3ef9fd378167e1ff6c5e.jpg" width="204" height="190" alt="Обычное полотно для москитной сетки" sizes="204px"></a></li></ul></div><br>Обычное полотно</li>
                    <li><div id="sigplus_1002" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/395c2b433d5e7b60ef351f2b7e79d258.jpg" title="Полотно антипыль для москитной сетки (1/1)" data-summary="Полотно антипыль для москитной сетки (1/1)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="<?=$pathToImg?>/395c2b433d5e7b60ef351f2b7e79d258.jpg" width="204" height="190" alt="Полотно антипыль для москитной сетки" sizes="204px"></a></li></ul></div><br>Полотно антипыль</li>
                    <li><div id="sigplus_1003" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/4740c6d7310529f61f720ffdfe9600e1.jpg" title="Полотно антипыльца для москитной сетки (1/1)" data-summary="Полотно антипыльца для москитной сетки (1/1)" data-fancybox="sigplus_1003"><img class="sigplus-preview" src="<?=$pathToImg?>/4740c6d7310529f61f720ffdfe9600e1.jpg" width="204" height="190" alt="Полотно антипыльца для москитной сетки" sizes="204px"></a></li></ul></div><br>Полотно антипыльца</li>
                </ul>
                <p>Крепления в ассортименте допускают монтаж москитных сеток на пластиковые, деревянные и алюминиевые окна, двери, балконные рамы и блоки. Структура и оттенки сетки не уменьшают светового освещения помещения.</p>
                <h2>Особенности сетки Poll-Tex</h2>
                <p><img class="img_right" src="<?=$pathToImg?>/allergy.png" alt="Полотно антипыльца"></p>
                <p>Мы предлагаем приобрести у нас сетки Poll-Tex «антипыльца». Мембрана антипыльца действительно помогает, в особенности людям, страдающим аллергическими заболеваниям вызванными пылью и пыльцой растений. Уникальная форма ячейки прямоугольной формы (0,15*0,1 мм.) способна задерживать мелкие вредные частицы и одновременно пропускать в помещение достаточное количество свежего воздуха.</p>
                <p>В отличие от других видов сеточного полотна москитка «антипыльца» изготавливается из нейлонового волокна, обладающего статическим притяжением, служит как фильтр.</p>
                <div class="img_right"><div id="sigplus_1004" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/images/moskitki/sert/120.jpg" title=" (1/1)" data-summary=" (1/1)" data-fancybox="sigplus_1004"><img class="sigplus-preview" src="/cache/preview/d51dba25fab0f11facfbd0bc50dfddd9.jpg" width="150" height="210" alt="" srcset="/cache/preview/811f8931a865b8981b48dc84c927d03e.jpg 300w, /cache/preview/d51dba25fab0f11facfbd0bc50dfddd9.jpg 150w, /cache/thumbs/6a0dba1cd3c68900e502630372500245.jpg 60w" sizes="150px"></a></li></ul></div></div>
                <p>Мелкие фракции уличной грязи, тополиный пух и другое притягиваются к сетке, не загрязняя воздух в помещении. Поэтому такую москитку требуется чаще очищать под струей воды, можно с мыльным раствором. На продолжительность службы полотна это не влияет. <a title="Крепежи москитных сеток антипыльца" href="/moskitnye-setki/krepezh.html">Крепления для таких видов сеток</a> можно выбрать любые, от стандартных Z-профилей, до плунжерного.</p>
                <h2>Особенности сетки Антипыль</h2>
                <p>Ячейки таких видов сеток отличаются размером (0.25*0,9 мм.). Антипыль задерживает больше пылевых частиц и мусора, чем стандартное полотно от комаров, но меньше чем сетка Антипыльца.</p>
                <p class="comment">Время изготовления 2-3 и монтажа – 1 рабочий день! Мастер на замер выезжает <strong>Бесплатно*, но с учётом заключения договора</strong>. График приёма заявок <b>с 8 до 22<sup>00</sup>без выходных.</b></p>
                <h2>Цены на москитные сетки от пыли, Антипыль</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup> сетки при самовывозе</td>
                        <td>Цена м<sup>2</sup> с замером, доставкой и установкой в пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.</td>
                        <td>2000 руб.</td>
                        <td>3100 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>4000 руб.</td>
                        <td>5700 руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>6000 руб.</td>
                        <td>8300 руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">Цена договорная</span></td>
                    </tr>
                    <tr>
                        <td class="table-comment" colspan="3">Цены в зависимости от количества покупаемых изделий</td>
                    </tr>
                    </tbody>
                </table>
                <h2>Цены на москитные сетки от пыльцы, Антипыльца</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup> сетки при самовывозе</td>
                        <td>Цена м<sup>2</sup> с замером, доставкой и установкой в пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.</td>
                        <td>2500 руб.</td>
                        <td>3600 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>5000 руб.</td>
                        <td>6700 руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>7500 руб.</td>
                        <td>9800 руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">Цена договорная</span></td>
                    </tr>
                    <tr>
                        <td class="table-comment" colspan="3">Цены в зависимости от количества покупаемых изделий</td>
                    </tr>
                    </tbody>
                </table>
                <p><a href="https://vk.com/remont_okn" target="blank"><img src="/images/moskitki/01.jpg" alt=""></a></p>
                <p class="comment">* Получите скидку до 10% на москитные сетки с установкой, став участником нашей <a class="vk_in_text" href="https://vk.com/remont_okn" target="blank">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 5 шт </b>.</p>
                <p><img class="img_right" src="/images/god.png" alt="Гарантия 1 год">Мы качественно изготовим и установим все противомоскитные конструкции для вашего дома, офиса, квартиры, торгового зала или дачи. На все изготовленные нами москитные сетки от пыли, мы даем гарантию качества.</p>
                <h3>Стоимость работ по замеру и установке москитных сеток</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Вид работ</td>
                        <td>Цена</td>
                    </tr>
                    <tr>
                        <td>Замер в Москва</td>
                        <td>бесплатно (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км. от МКАД</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500 руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Установка на плунжерное крепление + фетр/щётка самоклейка, скрывает неровности и зазоры проёмов</td>
                        <td>+700 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>
                <h3>Наценка на москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Цвет профиля</td>
                        <td>Наценка</td>
                    </tr>
                    <tr>
                        <td>Белый/коричневый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 40% к цене</td>
                    </tr>
                    </tbody>
                </table>

                <div id="otz" class="otz_in">
                    <h2>Отзывы клиентов</h2>
                    <div class="otz2">
                        <div class="otz">
                            <p class="desc">Поменяли во всем доме полотна москиток с обычных на антипыль, жена говорит действительно дома чище и воздух свежее. Летом проверим, насколько хорошо работает. Цена устроила, мастера адекватные и установили все быстро. Спасибо.</p>
                            <p class="date"><span id="today1">6 февраля</span>, Екатерина</p>
                        </div>
                        <div class="otz">
                            <p class="desc">Жена и я аллегики, поэтому постоянно боремся за нормальную весну. Поставили сетки еще в марте, уже конец лето, а в доме действительно теперь находится приятнее. И тополиный пух пережили, и цветы. Сначала не верил, но "антипыльца" работает! Спасибо!</p>
                            <p class="date"><span id="today2">21 января</span>, Георгий</p>
                        </div>
                    </div>
                    <br> </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>