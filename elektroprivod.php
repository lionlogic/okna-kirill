
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Электропривод для окон				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">

        <p>Осуществляем установку, обслуживание и ремонт электроприводов для  открывания окон всех видов. Специалисты нашей компании обладают большим опытом и способны работать на высоте, в том числе используя снаряжение альпинистов.</p>
        <img src="<?=$pathToImg?>/electroprivod.jpg" alt=""/>
        <h2>Виды оконных электроприводов</h2>
        <p>Мы установим любые приборы марки Giesse на пластиковые окна, фрамуги деревянные, алюминиевые и металлические, или отремонтируем оконные электроприводы следующих типов:</p>

        <ul>
            <li>цепные,</li>
            <li>штоковые,</li>
            <li>накладные,</li>
            <li>врезные.</li>
        </ul>
        <img src="<?=$pathToImg?>/e39bc8c9b58339a9920df56f55c89bca.jpg" alt=""/>
        <img src="<?=$pathToImg?>/f3ffc491a64ae0a5d08dd6bffd7a62df.jpg" alt=""/>
        <img src="<?=$pathToImg?>/134e85533ce268f8888303498166236c.jpg" alt=""/>
        <p class="comment">Вы можете получить бесплатную консультацию или задать любой интересующий вопрос по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a>. Наши специалисты помогут вам подобрать наиболее подходящий варант.</p>

        <h2>Цены на установку оконных электроприводов</h2>
        <p>В нашем каталоге вы можете подобрать механизм для каждого случая.</p>
        <p>Цены указаны за комплект с работой по установке электропривода "под ключ":</p>
        <ul>
            <li>цепные электроприводы Varia - 25.000 рублей;</li>
            <li>штоковые приводы для окон - 28.000 рублей;</li>
            <li>реечные приводы для окон - 32.500 рублей.</li>
        </ul>

        <img src="<?=$pathToImg?>/9b475d5b9a525e0255457a3c8af8e8ce.jpg" alt=""/>
        <img src="<?=$pathToImg?>/0f1eef5ddc2cab26363e3bcfbab4579f.jpg" alt=""/>
        <img src="<?=$pathToImg?>/93f50bc3307cca6bf1a32e139033be80.jpg" alt=""/>

        <p class="comment">В отличии от многих компаний способных только продавать электроприводы, мы готовы взять на себя весь комплекс услуг по подбору, установке, обслуживанию и ремонту автоматических оконных систем.</p>

        <h2>Причины установки</h2>
        <p>Анализируя опыт своей работы мы выделили 4 основные причины, когда люди оснащают свои окна электроприводами:</p>
        <ul>
            <li>окна расположены высоко от пола,</li>
            <li>окна имеют слишком большие габариты и массу,</li>
            <li>оконные конструкции подвергаются серьезным нагрузкам от ветра,</li>
            <li>невозможность открытия окон людьми с ограниченными возможностями,</li>
            <li>пожарные требования (дымоудаление).</li>
        </ul>


        <img src="<?=$pathToImg?>/8cb424168679713319ed87f09bde1d23.jpg" alt=""/>
        <img src="<?=$pathToImg?>/53408e2d4e329f5a3a2f04654885bd19.jpg" alt=""/>
        <img src="<?=$pathToImg?>/81ddebb017dea78a990e67fe1f93f612.jpg" alt=""/>
        <img src="<?=$pathToImg?>/693f7e0eeec285f5b627bcaf2b13e6af.jpg" alt=""/>
        <img src="<?=$pathToImg?>/9e0e9957054d6d403d82f8369f405586.jpg" alt=""/>
        <img src="<?=$pathToImg?>/4d41eb6db3d86ad372e17dcf5b05f6a0.jpg" alt=""/>

        <h2>Обслуживание и ремонт</h2>
        <p>Электроприводы достаточно надежны, но как любой механизм требуют смазки, регулировки и иногда ремонта.</p>
        <p>Наши мастера рекомендуют уделять внимание оконной автоматике не реже 1 раза в год, это гарантирует длительный и не проблемный срок эксплуатации механизмов.</p>

        <p class="comment">Получить бесплатную консультацию или вызвать специалиста на дом вы можете по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a> или с помощью <a href="#" class="zakaz">формы обратной связи</a>.</p>

        <div class="bcont bcont-email">
            <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
            <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
            <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
            <p><strong>E-mail: </strong><span id="cloak66b21db48ea4887a770e72e8634e262a"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                    document.getElementById('cloak66b21db48ea4887a770e72e8634e262a').innerHTML = '';
                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                    var path = 'hr' + 'ef' + '=';
                    var addy66b21db48ea4887a770e72e8634e262a = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                    addy66b21db48ea4887a770e72e8634e262a = addy66b21db48ea4887a770e72e8634e262a + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                    var addy_text66b21db48ea4887a770e72e8634e262a = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak66b21db48ea4887a770e72e8634e262a').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy66b21db48ea4887a770e72e8634e262a + '\'>'+addy_text66b21db48ea4887a770e72e8634e262a+'<\/a>';
                </script></p>
        </div>
    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>

    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
