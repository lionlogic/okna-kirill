
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Производство жалюзи для окон в Москве				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">





            <p>В нашей компании вы можете заказать производство и установку жалюзи различных видов, расцветок и изготовленные из различных материалов.</p>
            <img src="<?=$pathToImg?>/Izgotovlenie-zhalyuzi.JPG" alt="Каталог цветов и фактур для жалюзи" title="Каталог цветов и фактур для жалюзи" class="margall">
            <p class="comment">Вызвав нашего замерщика (по Москве бесплатно) по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a>, вы сможете познакомится с каталогом цветов и фактур.</p>

            <h2>Виды жалюзи</h2>
            <p>Наибольшей популярностью у наших клиентов пользуются жалюзи следующих видов:</p>
            <ul>
                <li><a href="#a1">вертикальные;</li>
                <li><a href="#a2">горизонтальные;</a></li>
                <li><a href="#a3">плиссе;</a></li>
                <li><a href="#a4">рулонные;</a></li>
                <li><a href="#a5">мансардные;</a></li>
                <li><a href="#a6">мультифактурные.</a></li>
            </ul>
            <p class="comment">Размеры окон не играют роли, мы готовы делать как маленькие, так и большие жалюзи любого типа.</p>
            <h2>Почему у нас выгоднее</h2>
            <ul>
                <li>Одни из самых низких цен по Москве.</li>
                <li>Гарантия на все виды жалюзи 12 месяцев.</li>
                <li>Не нужно долго ждать установки, срок замера, изготовления и монтажа всего 2-3 дня.</li>
                <li>Большой выбор видов, фактур, цветов и материалов жалюзи.</li>
                <li>Профессиональные сотрудники. Более 50% обращений в наш фирму приходится на рекомендации родных и знакомых.</li>
            </ul>
            <p class="comment">Чтобы совершить заказ позвоните по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a> или заполните <a href="#" class="zakaz">форму обратной связи</a>. </p>

            <h2>Виды жалюзи</h2>
            <p>Мы производим жалюзи по индивидуальным заказам, профессионально сочетая цвета и используя только качественные материалы. Наши мастера способны воплотить в жизнь вашу мечту об идеальных жалюзи. Смело беремся за производство таких видов, как:</p>
            <h3><a name="a1"></a>Вертикальные жалюзи</h3>

            <img src="<?=$pathToImg?>/2e42900a9172f6cee6dc13ac02088d0f.jpg" alt=""/>
            <img src="<?=$pathToImg?>/ee472e9aee7013fa8f1f7d3ba9a92ed2.jpg" alt=""/>
            <img src="<?=$pathToImg?>/0cbdfbee818dd235392e24c206ab0563.jpg" alt=""/>
            <img src="<?=$pathToImg?>/fa9cf989fbb7b96c2b5fb9023cf45c03.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Чаще всего изготавливаются из материала на тканевой основе.</li>
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>
                <li>Цена – 499 руб/м<sup>2</sup> </li>

            </ul>




            <h3><a name="a2"></a>Горизонтальные жалюзи</h3>
            <img src="<?=$pathToImg?>/516907eb5daf8be0f72b031e6214980c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/e4a5ba033009372dd31d0180de2276ba.jpg" alt=""/>
            <img src="<?=$pathToImg?>/692c3a196daf68c5a27f134aede9d06a.jpg" alt=""/>
            <img src="<?=$pathToImg?>/7e5f5f5a895b5310755e2eee5a8a49f1.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Чаще всего бывают изготовлены из алюминия или дерева, хотя возможны и другие материалы.</li>
                <li>Жалюзи изолайт, 16 мм (для окон ПВХ) –&nbsp;1900 руб/м<sup>2</sup></li>
                <li>Жалюзи изотра хит, 16мм (для окон ПВХ) –&nbsp;1575&nbsp;руб/м<sup>2</sup></li>
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>

            </ul>



            <h3><a name="a3"></a>Плиссе</h3>
            <img src="<?=$pathToImg?>/a023846db2a4c2423decf37f1464b3a4.jpg" alt=""/>
            <img src="<?=$pathToImg?>/f5cfb0c162684cfac67445efe369db7f.jpg" alt=""/>
            <img src="<?=$pathToImg?>/d20ff9d9075424a3f24252156df197fe.jpg" alt=""/>
            <img src="<?=$pathToImg?>/b3ac8eeb62001eece5b25c96dddd5e16.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>
                <li>Цена – 499 руб/м<sup>2</sup> </li>
            </ul>


            <h3><a name="a4"></a>Рулонные жалюзи (ролл-шторы)</h3>
            <img src="<?=$pathToImg?>/60945616778141249e7ac95722283bdc.jpg" alt=""/>
            <img src="<?=$pathToImg?>/29c2049eaa07688fef9fa39961e02b3d.jpg" alt=""/>
            <img src="<?=$pathToImg?>/2dbe2f8698121b4bedbea0633bd24928.jpg" alt=""/>
            <img src="<?=$pathToImg?>/e0e258b545798dabd7c6df4abb432ffc.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Рулонные жалюзи изготавливаются из цельного полотна, скручивающегося в рулон при сворачивании.</li>
                <li>Классические рулонные жалюзи – 1499 руб/м<sup>2</sup></li>
                <li>Рулонные жалюзи Uni (для окон пвх) - 2700 руб/м<sup>2</sup></li>
                <li>Рулонные жалюзи Uni-2 (для окон пвх) - 2999 руб/м<sup>2</sup></li>
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>
            </ul>


            <h3><a name="a5"></a>Мансардные жалюзи</h3>
            <img src="<?=$pathToImg?>/5f82929b7b1d1c0625578c91c4289f6c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/6846009fa46c587f3253e045e5a8f936.jpg" alt=""/>
            <img src="<?=$pathToImg?>/db5e13deb3ee4e8bb4e8a49bf6eb3e5b.jpg" alt=""/>
            <img src="<?=$pathToImg?>/03efc71214d77cbd2d58eff775a5ac47.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Особенность жалюзей данного вида в том, что они способны удержаться и не провисать под любым углом.</li>
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>
                <li>Цена – 3590 руб/м<sup>2</sup></li>
            </ul>


            <h3><a name="a6"></a>Мультифактурные жалюзи</h3>
            <img src="<?=$pathToImg?>/c53d0f5ffe2043a10bd90bdec6d5b9ec.jpg" alt=""/>
            <img src="<?=$pathToImg?>/092687143f43b8155b2dc3e010f4cba3.jpg" alt=""/>
            <img src="<?=$pathToImg?>/120a4dd08e396fc8a8c8fb707c0d834d.jpg" alt=""/>
            <img src="<?=$pathToImg?>/80754d50a63d76f1830172bf1524fbb2.jpg" alt=""/>
            <ul class="best_vm blue_line">
                <li>Чаще всего бывают вертикальными. Представляют собой комбинацию из различных цветов и даже материалов.</li>
                <li>Срок изготовления – от 2 до 4 рабочих дней.</li>
                <li>Цена – 499 руб/м<sup>2</sup></li>
            </ul>




    </div>
</div>

    </div>
<?php include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->

