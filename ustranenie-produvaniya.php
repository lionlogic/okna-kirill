
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			<div class="social header__social">
				<div class="text-small">
					Задайте вопрос напрямую в: <br>
					(среднее время ответа 24 сек)
				</div>

				<div class="social__wrap">
					<div class="social__item">
						<a href="https://wa.me/79857275045?text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5%21+%D0%98%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D1%83%D0%B5%D1%82+%D1%80%D0%B5%D0%BC%D0%BE%D0%BD%D1%82+%D0%BE%D0%BA%D0%BE%D0%BD..."  class="link-social wa"><img src="./wp-content/themes/src/assets/img/main/wa.svg" alt=""><span>WhatsApp</span></a>
					</div>
					<div class="social__item">

						<a href="#" class="link-social skype"><img src="./wp-content/themes/src/assets/img/main/skype.svg" alt=""><span>Skype</span></a>
					</div>

					<div class="social__item ">

						<a href="mailto:{{EMAIL}}" class="link-social e-mail"><img src="./wp-content/themes/src/assets/img/main/Mail.svg" alt=""> <span>E-mail</span></a>
					</div>
				</div>
			</div>


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Дует из пластиковых окон, что делать?!
				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>




	
	<div class="container">
        <div itemprop="articleBody">
            <p>Если дует из пластикового окна или продувает балконная дверь, <strong>нужен ремонт</strong>. Осуществляем наладку окон ПВХ в Москве, Новой Москве и всей области, в том числе устраняем более сложные проблемы оконных и дверных конструкций.</p>
            <p>Звуки с улицы стали вас раздражать, появились щели, зазоры, дует из под подоконников, со стороны петель, в комнате стало заметно прохладнее, стоит задуматься о диагностике и ремонте возникших неполадок.</p>
            <p class="comment">Если дует из ПВХ окна, наши сервисные инженеры, качественно и с гарантией устранят все неудобства и нарекания, связанные со светопрозрачными системами.</p>

            <h2>Причины продувания и сквозняков окон ПВХ</h2>
            <ul>
                <li>дефекты в работе прижимных устройств;</li>
                <li>нарушения герметичности монтажных швов;</li>
                <li>высыхание оконных уплотнителей;</li>
                <li>износ или неправильная установка уплотнителя;</li>
                <li>поломка фурнитуры;</li>
                <li>перекосы створок окна;</li>
                <li>не правильная установка оконных конструкций.</li>
            </ul>
            <p>Наиболее частыми видами работ для устранения продувания и сквозняков от пластиковых окон являются:</p>
            <ul>
                <li>регулировка или замена фурнитуры окна;</li>
                <li>замена уплотнителя;</li>
                <li>герметизация&nbsp;монтажных швов;</li>
                <li>пропенивание пустот под отливами.</li>
            </ul>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloak97f5393cda698ba6d30a81a648b0843b"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloak97f5393cda698ba6d30a81a648b0843b').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy97f5393cda698ba6d30a81a648b0843b = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy97f5393cda698ba6d30a81a648b0843b = addy97f5393cda698ba6d30a81a648b0843b + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text97f5393cda698ba6d30a81a648b0843b = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak97f5393cda698ba6d30a81a648b0843b').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy97f5393cda698ba6d30a81a648b0843b + '\'>'+addy_text97f5393cda698ba6d30a81a648b0843b+'<\/a>';
                    </script></p>
            </div>
            <h3>Продувает пластиковое окно, как устранить?</h3>
            <p>Сервисное обслуживание - комплекс мер, позволяющий не только устранить продувание и щели вашего окна, но и предотвратить их появление в будущем.</p>
            <p>Оно включает в себя:</p>
            <ul>
                <li>консультацию и бесплатный выезд мастера;</li>
                <li>диагностику состояния оконных конструкций;</li>
                <li>регулировку, чистку, смазку фурнитуры, профилактику провисания;</li>
                <li>замену &nbsp;оконного уплотнителя и герметизацию.</li>
            </ul>
            <p class="comment">Стоимость сервисного обслуживания <b>1700 рублей/створка</b> с учетом всех материалов и работ ( в сумму входит замена 8 м/п. притворного уплотнителя по 150 руб.+ регулировка и смазка замков).
            </p>
            <img src="/files/imges/plastic_windows/4c88e119c3afa573325d78ebe5cb020b.jpg" alt=""/>
            <img src="/files/imges/plastic_windows/17dae1e12407564bc105b05ffb5cdb76.jpg" alt=""/>
            <img src="/files/imges/plastic_windows/4f8fbc0ad715a28863881921375247ad.jpg" alt=""/>
            <img src="/files/imges/plastic_windows/e3004b0aea18493930b779eaa1da1fba.jpg" alt=""/>
            <h2>Разница между продуванием и холодком</h2>
            <p>Не редко, холод от окон, путают с продуванием, по тем или иным причинам, распространяющимся от стеклопакета. Чаще всего, такое явление можно наблюдать зимой, когда воздух охлаждается, скользя по поверхности стеклопакета сверху вниз. Подставив ладонь внизу створки окна, вы почувствуете легкое ощущение сквозняка, это конвекция.</p>
            <h3>Основные причины холода от окон</h3>
            <ul>
                <li>однокамерный стеклопакет с низкими теплозащитными свойствами,</li>
                <li>широкий подоконник, закрывающий отопительный прибор,</li>
                <li>слабое отопление,</li>
                <li>чрезмерная вентиляция.</li>
            </ul>
            <p class="comment">Внимание! Устранение причин продувания пластиковых окон требует специальных знаний, поэтому попытки ремонта неквалифицированными мастерами могут привести к фатальной поломке ваших оконных конструкций. Доверяйте свои окна только специалистам:</p>



            </div>
            <p><a name="napr"></a></p> 	</div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>