
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_wood.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		a<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Реставрация деревянных окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <p>Специалисты сервисной службы «Помощь окнам» быстро, качественно и с учетом пожеланий клиента осуществят реставрацию деревянных окон евростандарта со стеклопакетами.</p>
            <p><img src="<?=$pathToImg?>/1.jpg" alt="Реставрация окон из дерева"></p>
            <p>Процесс восстановления требует профессиональный подход и знания технологии реставрационных работ, которыми обладают работники нашей службы.</p>

            <p class="comment">Бесплатно вызвать мастера и задать все интересующие вопросы вы можете по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>

            <h2>Виды работ</h2>

            <p>Сотрудниками сервисного центра была разработана технология реставрации старых деревянных окон с учетом применения специального оборудования и свойств материалов. Технология проведения комплексной реставрации включает следующие работы:</p>
            <img src="<?=$pathToImg?>/3w.jpg" alt="Покраска современных окон" class="img_right desctop">
            <ul>
                <li>шпаклевку трещин, вмятин, сколов;</li>
                <li>замену лакокрасочного покрытия;</li>
                <li>установку новых откосов, отливов, козырьков;</li>
                <li>восстановление геометрии оконной конструкции;</li>
                <li>утепление деревянных окон;</li>
                <li>замену стеклопакетов на стекла специального назначения: армированные, энергосберегающие, шумопоглощающие, витражные, триплекс;</li>
                <li>установку москитных сеток;</li>
                <li>замену уплотнителя;</li>
                <li>регулировку и замену фурнитуры.</li>
            </ul>

            <h2>Основные этапы восстановления старых деревянных окон</h2>

            <p><img src="<?=$pathToImg?>/2w.jpg" alt="Реставрация деревянных окон"></p>
            <p>Процесс реставрации деревянных оконных блоков включает семь основных этапов.</p>
            <ol>
                <li>Подготовительные работы. Первый этап заключается в удалении с оконных поверхностей лакокрасочных материалов с помощью химического раствора или термофена.</li>
                <li>Восстановление геометрии конструкции окна путем перерасклинивания створки.</li>
                <li>Нанесение антисептиков. С целью предотвращения грибкового заражения рамы пропитываются защитными составами «Неомид», «Просепт».</li>
                <li>Шпаклевка. Для работ используется специальная шпаклевочная смесь по дереву. Шпаклевкой замазываются все трещины, зазоры, щели, неровности. После ее высыхания производится зачистка поверхностей и нанесение грунтовки, обеспечивающей хорошее сцепление краски с оконной поверхностью.</li>
                <li>Покрытие лакокрасочными материалами. Подобранные в тон по шкале RAL краски, лаки наносятся в несколько слоев. Каждый слой подвергается тщательной просушке.</li>
                <li>Замена резинового уплотнителя для деревянных евроокон. Потерявший эластичность уплотнитель заменяется новым.</li>
                <li>Герметизация швов монтажной пеной, регулировка прижатия створки окна к&nbsp;раме.</li>
                <li>Монтаж фурнитуры. Завершающим этапом реставрационных работ является установка ручек.</li>
            </ol>

            <p>Все работы осуществляются с применением современного, высокотехнологичного оборудования и качественных прогрессивных материалов зарубежного или отечественного производства.</p>
            <p class="comment">Реставрация окон в Москве от сервисного центра «Помощь окнам» - уникальная возможность надолго повысить функциональность и эстетическую привлекательность любимых окон! Обращайтесь по телефону по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a> или с помощью формы обратной связи.</p>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>