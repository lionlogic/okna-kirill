
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Раздвижные москитные сетки</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Раздвижные москитные сетки			</h1>
            </div>





            <div itemprop="articleBody">
                <p><img class="img_right" title="Раздвижные москитные сетки" src="<?=$pathToImg?>/Razdvizhnye-moskitnye-setki.jpg" alt="Раздвижные москитные сетки, производство, установка">Мы изготавливаем раздвижные москитные сетки для установки на балконах, лоджиях и террасах, застекленных алюминиевыми рамами с раздвижной системой открытия (обычно Provedal или KRAUSS).</p>
                <p>Принцип действия – боковое передвижение рамки прямоугольной формы по направляющим. В случае необходимости полного открытия окна, сетку легко передвигают в любую сторону.</p>

                <p>Для <a title="Крепления для раздвижных сеток" href="/moskitnye-setki/krepezh.html#poloz">установки сетки нужно всего два полоза</a>, надежно закрепленные по всей длине рамы балкона, окна. Полозья обеспечивают беспрепятственное передвижение рамки сетки (или нескольких рамок).</p>

                <div class="img_right"><div id="sigplus_1001" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/42e7ac479f53f35560720790fce6c8d1.png" title=" (1/1)" data-summary=" (1/1)" data-fancybox="sigplus_1001"><img class="sigplus-preview" src="<?=$pathToImg?>/42e7ac479f53f35560720790fce6c8d1.png" width="240" height="180" alt="" srcset="/cache/preview/42e7ac479f53f35560720790fce6c8d1.png 480w, /cache/preview/7a9ea7841c21b3c635d4296867651484.png 240w, /cache/thumbs/dd08ba7e6271096f513d79bd022ff35a.png 60w" sizes="240px"></a></li></ul></div></div>
                <p>Стандартный цвет изделия – белый (в цвет обычных оконных конструкций), но по желанию может быть окрашен в другой необходимый оттенок. Таблица возможных цветов представлена справа (нажмите левой кнопкой мыши по картинке для её увеличения).</p>

                <p>Профиль сеток снабжён специальным фетровым уплотнителем (щеткой) по периметру, который обеспечивает очистку сетки и максимальное прилегание к конструкции остекления.</p>
                <p><a href="https://vk.com/remont_okn" target="blank"><img src="/images/moskitki/01.jpg" alt=""></a></p>

                <p class="comment">* Получите скидку до 10% на москитные сетки с установкой, став участником нашей   <a href="https://vk.com/remont_okn" target="blank" class="vk_in_text">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 3 шт </b>.</p>



                <h2>Область применения</h2>
                <div class="img50 margall"><div id="sigplus_1002" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/raz_1.jpg" title=" (1/4)" data-summary=" (1/4)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="<?=$pathToImg?>/" width="225" height="200" alt="" sizes="225px"></a></li><li><a class="sigplus-image" href="/images/article/12/2.jpg" title=" (2/4)" data-summary=" (2/4)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="/cache/preview/f05518a2d8fe1473222b556f412282d3.jpg" width="225" height="200" alt="" srcset="/cache/preview/7b19f2cf34f77680669474ace82dc0b9.jpg 450w, /cache/preview/f05518a2d8fe1473222b556f412282d3.jpg 225w, /cache/thumbs/2e32544d9dd1a1e76d0e6c81ec16557f.jpg 60w" sizes="225px"></a></li><li><a class="sigplus-image" href="/images/article/12/3.jpg" title=" (3/4)" data-summary=" (3/4)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="/cache/preview/078e128e087873aca1b4295031f50198.jpg" width="225" height="200" alt="" srcset="/cache/preview/2b4d26faa7917697cd1c108ed7db3ae2.jpg 450w, /cache/preview/078e128e087873aca1b4295031f50198.jpg 225w, /cache/thumbs/fd9a7f210257566cfcfc4c6d7af474c5.jpg 60w" sizes="225px"></a></li><li><a class="sigplus-image" href="/images/article/12/4.jpg" title=" (4/4)" data-summary=" (4/4)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="/cache/preview/b404d5f4c9fcf7a927f585c2a06de407.jpg" width="225" height="200" alt="" srcset="/cache/preview/6fcd0d151a886800febfb771d4e1ff98.jpg 450w, /cache/preview/b404d5f4c9fcf7a927f585c2a06de407.jpg 225w, /cache/thumbs/bdbb019a4ecdcf2ad2d204069430a574.jpg 60w" sizes="225px"></a></li></ul></div></div>
                <p>Помимо балконов, раздвижные сетки можно устанавливать на террасах и в дверных проемах, если высота конструкции не будет превышать двух метров.</p>
                <p class="comment">Раздвижная конструкция более двух метров будет довольно «хлипкая», плохо прилегающая и, следовательно, неполноценно защищающая от насекомых.</p>
                <p>К тому же раздвижная дверь-сетка должна иметь достаточно места для раздвижения. Исключения составляют французские оконные системы, где раздвижные окна имеют размер от потолка до пола и для которых раздвижные москитные сетки – идеальный вариант.</p>
                <p>В случае необходимости в большей высоте сеточной защиты, мы предлагаем другой вид изделия, например, рулонный.</p>



                <h2>Ремонт раздвижных сеток</h2>
                <p>Помимо производства новых сеток, мы предлагаем их ремонт, включающий:</p>
                <ul>
                    <li>перетяжка провисшего полотна,</li>
                    <li>замена сеточного полотна,</li>
                    <li>ремонт неисправной раздвижной фурнитуры,</li>
                    <li>замена поломанной фурнитуры.</li>
                </ul>
                <p>Наши специалисты в короткие сроки отремонтируют или изготовят москитную сетку на заказ для любой оконной системы. Мы гарантируем качественную установку готовой рамки на балконах, лоджиях на любой высоте. Для нас не существует недоступных оконных конструкций!</p>
                <ul class="icon102 ul-inline">
                    <li class="garant">Гарантия на все виды работ и материалы 1 год.</li>
                    <li class="money101">Доступные цены и гибкая система скидок.</li>
                    <li class="good">100% выполнение гарантийных и постгарантийных обязательств.</li>
                </ul>


                <h2>Цены на сетки москитные со стандартным полотном</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Кол-во сеток</td><td>Цена м<sup>2</sup> сетки при самовывозе</td><td>Цена м<sup>2</sup> с замером, доставкой и установкой в пределах МКАД</td></tr>
                    <tr>
                        <td>1 шт.</td>
                        <td>2500 руб.</td>
                        <td>3500 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>5000 руб.</td>
                        <td>6500&nbsp;руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>7500 руб.</td>
                        <td>9500&nbsp;руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">* Цена договорная со скидкой</span></td>
                    </tr>
                    </tbody>
                </table>

                <p class="comment">В Москве изготовим за 2-3 рабочих дня, а установим раздвижную сетку – за 30 минут ! Выезд мастера на замер осуществляется <b>БЕСПЛАТНО*, но с учётом заключения договора</b>.</p>


                <h3>Стоимость работ по замеру и установке москитных сеток</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Вид работ</td><td>Цена</td></tr>
                    <tr>
                        <td>Замер в Москва</td>
                        <td>бесплатно (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500 руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Срочное изготовление</td>
                        <td>+700 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500&nbsp;руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км</td>
                    </tr>
                    </tbody>
                </table>


                <h3>Наценка на москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Вид полотна</td><td>Наценка за сетку до 1 кв.м.</td></tr>
                    <tr>
                        <td>"Антикошка" (Pet Screen) (Saint Gobain, Канада) цвет черный</td>
                        <td>+ 1000 руб./м.кв.</td>
                    </tr>
                    <tr>
                        <td>"Антикошка" (Pet Screen) (Phifer, США) цвет светло - серый</td>
                        <td>+ 1100 руб./м.кв.</td>
                    </tr>
                    <tr>
                        <td>"Антипыль" (MICRO MESH)</td>
                        <td>Договорная</td>
                    </tr>
                    </tbody>
                </table>


                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Цвет профиля</td><td>Наценка</td></tr>
                    <tr>
                        <td>Белый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 3500 к цене</td>
                    </tr>
                    </tbody>
                </table>


                <div class="otz_in" id="otz">
                    <h2>Отзывы клиентов</h2>

                    <div class="otz3">
                        <div class="otz">
                            <p class="desc">Перетянули полотно на старой раздвижной москитной сетке. Выглядит и работает лучше, чем новое. Я доволен, отличная работа.</p>
                            <p class="date"><span id="today1">7 февраля</span>, Владимир</p>

                        </div>

                        <div class="otz">
                            <p class="desc">Расшатались полозья на балконной сетке, пришлось вызывать мастера. Все починили быстро, заодно мне еще и на кухне окошко отремонтировали. Спасибо.</p>
                            <p class="date"><span id="today2">20 января</span>, Арина</p>
                        </div>

                        <div class="otz">
                            <p class="desc">Когда появилась необходимость поставить москитные сетки на мои раздвижные алюминиевые окна, оказалось что они нужны какие-то особенные. Ни в одной организации мне не смогли точно сказать сколько они будут стоить и чем отличаются. В "Помощи" мастер сразу назвал цену за кв. метр, объяснил про профили и записал меня на замер. Через три дня уже любовалась новыми сетками по приятной цене. Отличная консультация и работают качественно.</p>
                            <p class="date"><span id="today3">17 января</span>, Альфия</p>
                        </div>
                    </div>
                    <br>

                </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>