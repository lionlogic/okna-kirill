
if($('body').find('#price-prx').length > 0){

  var scene_1 = document.getElementById('price-prx');
  var parallax1 = new Parallax(scene_1);
}
if($('body').find('.gift').length > 0){

  var scene_2 = document.getElementById('prx-gift-1');
  var parallax2 = new Parallax(scene_2);
  var scene_3 = document.getElementById('prx-gift-2');
  var parallax3 = new Parallax(scene_3);
}
 $('.select-input').fancySelect();
if($('body').find('.frame').length > 0){

(function () {
		var $frame = $('#cycleitems');
		var $wrap  = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			horizontal: 1,
			itemNav: 'basic',
			smart: 1,
			activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBar: $('.process__content').find('.scrollbar'),
			scrollBy: 1,
			speed: 3100,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,

			// Cycling
			cycleBy: 'items',
			cycleInterval: 2500,
			pauseOnHover: 1,
      forward: $('.process__slider').find('.forward'),
      backward: $('.process__slider').find('.backward'),

		});

	}());
}


$('.work-slider').each(function(index, el) {
	$(this).slick({
		//dots: true,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 600,
		lazyLoad: 'ondemand',
		prevArrow: '<button class="arrows slidePrev"></button>',
		nextArrow: '<button class="arrows slideNext"></button>',
		responsive: [{

	      breakpoint: 992,
	      settings: {
	        slidesToShow: 1,
	        swipe: true,
	        centerMode: false
	      }

	    }]
	});

	$(this).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
	    var number = nextSlide;
	    $(this).parents('.work__sert').find('.slider-text-item').hide();
	    $(this).parents('.work__sert').find('.slider-text-item').eq(number).fadeIn();
	});
});



var idVideo;

function playYouModal(e) {
    e.preventDefault();
    $(".modal-video-body").append('<iframe></iframe>');
    var iframe = $(".modal-video-body").find('iframe');

    
    $('html').addClass('stop');
    $('#video-modal').fadeIn();
    if ($(this).data('play') != null) {
        idVideo = $(this).data("play");
    }
    var iframe_url = "https://www.youtube.com/embed/" + idVideo + "?enablejsapi=1&autoplay=1&autohide=1&rel=0";

    if ($(this).attr("data-params")) iframe_url += '&' + $(this).attr("data-params");
    
    iframe.attr({
            src: iframe_url,
            frameborder: '0',
            allowfullscreen: 'allowfullscreen'
        })
        .css({
            width: '100%',
            height: '100%'
        });
    
}



// клики
$('.feedback-play').on('click', playYouModal);
$('.play-statistic').on('click', playYou);
$('.modal-play').on('click', playYou);
$('.director__play').on('click', playYouModal);

var labelVideo = false;
function playYou() {
    $(this).parents(".video-ifr").addClass('dark').append('<iframe></iframe>');
    $(this).parents(".video-ifr").addClass('dark');
    var iframe = $(this).parents(".video-ifr").find('iframe');

    if ($(this).data('play') != null) {
        idVideo = $(this).data("play");
    }
    var iframe_url = "https://www.youtube.com/embed/" + idVideo + "?enablejsapi=1&autoplay=1&autohide=1&rel=0";

    if ($(this).attr("data-params")) iframe_url += '&' + $(this).attr("data-params");
    
    iframe.attr({
            src: iframe_url,
            frameborder: '0',
            allowfullscreen: 'allowfullscreen'
        })
        .css({
            width: '100%',
            height: '100%'
        });
    
    $(this).hide();
}
$('.work-play').on('click', playYou);
$('.blogplay').on('click', playYou);

$('.close').on('click', function(event) {
	event.preventDefault();
	if($(this).hasClass('close-video')){
		$('.modal-video-body iframe').remove();
	}if($(this).hasClass('close-nav')){
		$(".nav").removeClass('active');
	}
	$(".overlay").fadeOut();
	// modalOpen();
 //  if(numbOverlay<2){
      $('html').removeClass('stop');
  // }
});

$('.close-nav').on('click', function(event) {
  event.preventDefault();

  $(".nav").fadeOut();
  // modalOpen();
  // if(numbOverlay<2){
      $('html').removeClass('stop');
  // }
});

if ($("body").find(".main-label").length > 0) {

     var closeMod = false;
     $(document).mouseleave(function(event) {
         event = event || window.event;
         if (event.clientY < 0 || event.clientY < 3) {
             if (!closeMod) {
                 $('#modal-up').fadeIn();
                 $('html').addClass('stop');
                 closeMod = true;
             }

         }
     });

}


 $('.overlay').not('#modal-page, #test').mouseup(function(e){
    var container = $('.modal-wrap');
    if (container.has(e.target).length === 0 && !container.is(e.target)){
        $('html').removeClass('stop');
        $('.overlay').fadeOut();
    }
});

// клики
var numbOverlay = 0;
function modalOpen(){
    numbOverlay = 0;
    $('.overlay').each(function(index, el) {
        if($(this).css("display") !== 'none'){
            numbOverlay++;
        }
    });
}
$('.consalt-1').on('click', function(event) {
	event.preventDefault();
	$("#recall-modal").fadeIn();
	$("html").addClass('stop');
});

$('.burger__wrap').on('click', function(event) {
	event.preventDefault();
	$(".nav").fadeIn().addClass('active');
  $("html").addClass('stop');
});

// $('.master-open').on('click', function(event) {
//   event.preventDefault();
//   $("#open-master-js").fadeIn();
//   $("html").addClass('stop');
// });
$('.priceDow-open').on('click', function(event) {
  event.preventDefault();
  $("#modal-price").fadeIn();
  $("html").addClass('stop');
});
$('.comers-open').on('click', function(event) {
  event.preventDefault();
  $("#modal-comer").fadeIn();
  $("html").addClass('stop');
});
$('.gifts-open').on('click', function(event) {
  event.preventDefault();
  $("#modal-gift").fadeIn();
  $("html").addClass('stop');
});

var listCalc = [];
$('.master-calc-open').on('click', function(event) {
  event.preventDefault();
  var value;
  $('.tabl-inputs').each(function(index, el) {
    if($(this).prop('checked')){
      value = $(this).val().trim() + ' - ' + $(this).data('summ')+'р' + " / " +
       $(this).parents('tr').find('.stepper__input').val().trim() +' шт';
      listCalc.push(value);
    }
  });
  var str = listCalc.join(' , ');

  $('#open-master-js').find('input[name="vibor"]').val(str);
  $('#open-master-js').find('input[name="price"]').val($('#final-sum').text().trim() + ' руб');

  $("#open-master-js").fadeIn();
  $("html").addClass('stop');
});

$(document).ready(function($) {
  $(".section-title").not('.title-first').each(animeFade);
  $(".section-desc").not('.subtitle-first').each(animeFade);
});

function animeFade(){
  var offsetTop = $(this).offset().top - $(window).height();
  var thisTitle = $(this);
  $(window).scroll(function(event) {
    if($(document).scrollTop() > offsetTop ){
      thisTitle.addClass('fade_in');
    }
  });
}

$(".nav a").not('a[href="#none"]').on("click", function (event) {
    if($(this).parents(".nav").hasClass('active')){
        $(".nav").removeClass("active").fadeOut();
        // $('html').removeClass('stop');
    }else{}
    var id = $(this).attr('href'), top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1000);
});



// $(".visa-scroll").on("click", function (event) {
//     var id = $(this).attr('href'), top = $(id).offset().top;
//     $('body,html').animate({scrollTop: top}, 1000);
// });

$('.fancy-class').fancybox({
  buttons : [ 
    'slideShow',
    'zoom',
    'fullScreen',
    'close'
  ],
  animationEffect: "zoom-in-out",
  animationDuration: 600,
  transitionEffect: "circular",
  transitionDuration: 420,
 
});

$(".to-top").on("click", function (event) {
    var  top = 0;
    $('body,html').animate({scrollTop: top}, 400);
});

var labelToTop = false;
var labelRight = false;
$(window).on('scroll', function(event) {
  if($(window).scrollTop() > $(window).height() * 2 && !labelToTop){
    $('.to-top').addClass('active');
    labelToTop = !labelToTop;
  }else if($(window).scrollTop() < $(window).height() * 2 && labelToTop){
    $('.to-top').removeClass('active');
    labelToTop = !labelToTop;
  }

  if($(window).scrollTop() > $(window).height() * 2 && !labelRight){
    $('.right-bar').addClass('noact');
    labelRight = !labelRight;
  }else if($(window).scrollTop() < $(window).height() * 2 && labelRight){
    $('.right-bar').removeClass('noact');
    labelRight = !labelRight;
  }
});




// inp-phone
$(".input-tel, .input-phone").mask("+7-(999)-999-99-99");



if($('body').find('.amenities').length > 0){

  showItem();

  if($('body').find('.amenities__item').length < 9){
    $('.btn-show-wrap').remove();
  }

  $('.btn-show').on('click', function(event) {
    event.preventDefault();
    if(!$('.btn-show').hasClass('open')){

      $('.amenities__item').each(function(index, el) {
        if($(this).hasClass('hiddenItem')){
          $(this).removeClass('hiddenItem').fadeIn();
        }
      });

      $(this).addClass('open').text('Скрыть');
    }else{
      showItem();
      $(this).removeClass('open').text('Показать все услуги');
    }
  });


  function showItem(){
    $('.amenities__item').each(function(index, el) {
      if($(this).index() > 9){
        $(this).addClass('hiddenItem').hide();
      }
    });
  }
}

if($('body').find('.date-s').length > 0){
  let month = ["Января", "Февраля", "Марта","Апреля","Мая","Июня","Июля","Августа","Сентября","Октября","Ноября","Декабря"];

  var now = new Date();
  var dayWeek = now.getMonth();

  var day = now.getDate();
  var mounth = now.getMonth() + 1;
  if(mounth === 13){
    mounth = now.getMonth() + 1;
  }
  if(mounth < 10){
    mounth = '0'+ mounth ;
  }

  if(day < 10){
    day = '0'+ day ;
  }

  // $('.date-s').text(" / от "+ now.getDate() +" " + month[dayWeek] +" "+ now.getFullYear() +"г /");
  $('.date-s').text(day + "."+ mounth + '.' + now.getFullYear());
}



function playProcess() {
    if($('.process__video-content').find('iframe').length > 0){
      $(".process__video-content").find('iframe').remove();
    }
    $(".process__video-content").php('<iframe></iframe>');
    var iframe = $(".process__video-content").find('iframe');

    if ($(this).data('play') != null) {
        idVideo = $(this).data("play");
    }
    var iframe_url = "https://www.youtube.com/embed/" + idVideo + "?enablejsapi=1&autoplay=1&autohide=1&rel=0";

    if ($(this).attr("data-params")) iframe_url += '&' + $(this).attr("data-params");
    
    iframe.attr({
            src: iframe_url,
            frameborder: '0',
            allowfullscreen: 'allowfullscreen'
        })
        .css({
            width: '100%',
            height: '100%'
        });

    
    if($(this).hasClass('process-play-main')){
      $(this).hide();
    }
    // else{
    //  $('.process__video-text-wrap').hide(); 
    // }

    if($(this).hasClass('play-mini')){
      
      $('.process__video-text').hide();
      $('.process__video-text').eq($(this).index()).fadeIn();
    }
    
}


$('.process__video-item').on('click', playProcess);
$('.process-play-main').on('click', playProcess);



if($('body').find('.director-wrap').length > 0){
  $('.director-wrap').slick({
    //dots: true,
    infinite: false,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 600,
    autoplay: true,
    autoplaySpeed: 3100
  });
}



if($('body').find('.projects').length > 0){

  $('.projects__card').each(function(index, el) {
    var sliderTop = $(this).find('.projects__slider-top');
    var sliderBot = $(this).find('.projects__slider-bot');

    sliderTop.slick({
     infinite: true,
     slidesToShow: 1,
     slidesToScroll: 1,
     speed: 600,
     asNavFor: sliderBot,
     lazyLoad: 'ondemand',
     arrows: false,
    });

    sliderBot.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: sliderTop,
      lazyLoad: 'ondemand',
      focusOnSelect: true,
      arrows: false,
      responsive: [{

           breakpoint: 992,
           settings: {
             slidesToShow: 2,
           }

         }]
    });

    var relValue = index;
    $(this).find(".fancy-class").attr({
      "data-fancybox": "objects"+relValue,
    });
  });
  
}

if($('body').find('.projects').length > 0){
  $('.feedback-play').each(function(index, el) {
    if($(this).data('play') === ''){
      $(this).remove();
    }
  });
}
if($('body').find('.calc').length > 0){
  $(".calc__tab-header").on("click", function() {

      // if ($(this).parents('.calc__tab-item').hasClass('active')) {
      //     $(".calc__tab-item").removeClass('active');
      //     $(".calc__tab-content").slideUp();
      // } else {
      //     $(".calc__tab-item").removeClass('active');
      //     $(".calc__tab-content").slideUp();
      //     var $this = $(this);
      //     setTimeout(function(){
      //       $this.parents('.calc__tab-item').addClass('active');
      //     },100);
          
      //     $(this).next().slideDown();
      // }

      $(this).parents('.calc__tab-item').addClass('active');
      $(this).next().slideToggle();
    });



  var allSumm1 = 0;
  var allSumm2 = 0;
  var allSumm3 = 0;
  var allSumm4 = 0;
  var allSumm5 = 0;
  var allSumm6 = 0;
  var allSumm7 = 0;
  var allSumm8 = 0;
   var allSumm9 = 0;

  summItems( allSumm1 , '.tabl-inp1', '.stepper__input-1' );
  summItems( allSumm2 , '.tabl-inp2', '.stepper__input-2' );
  summItems( allSumm3 , '.tabl-inp3', '.stepper__input-3' );
  summItems( allSumm4 , '.tabl-inp4', '.stepper__input-4' );
  summItems( allSumm5 , '.tabl-inp5', '.stepper__input-5' );
  summItems( allSumm6 , '.tabl-inp6', '.stepper__input-6' );
  summItems( allSumm7 , '.tabl-inp7', '.stepper__input-7' );
  summItems( allSumm8 , '.tabl-inp8', '.stepper__input-8' );
  summItems( allSumm9 , '.tabl-inp9', '.stepper__input-9' );
  
  $('.stepper__input').attr({'disabled':'true'});
  var finalSumm = 0;
  var $this;
  function summItems( vars, selector, selInput ){
    $(selector).on('change, input', function() {

      if($(this).prop('checked')){
        $(this).parents('tr').find('.bloked').hide();
        var sum = $(this).data('money') * $(this).parents('tr').find('.stepper__input').val();
        $(this).data('summ', sum);
        $(this).parents('tr').find('.stepper__input').removeAttr('disabled');
      }else{
        $(this).parents('tr').find('.bloked').show();
        var sum = $(this).data('money') * $(this).parents('tr').find('.stepper__input').val();
        // vars = vars - sum;
        $(this).data('summ', 0);
        $(this).parents('tr').find('.stepper__input').attr({'disabled':'true'});

      }
      $this = $(this);
      // vars = 0;
      // $(this).parents('.calc__tab-content').find('td').each(function(index, el) {

      //   if($(this).children(selector).length > 0){

      //     vars = vars + Number.parseInt($(this).children(selector).data('summ'));
      //     // console.log(vars);
      //   }

      // });

      // $(this).parents('table').find('.end-sum').text(vars);


      // finalSumm = 0;

      // $('.calc__tab-content').each(function(index, el) {
      //   finalSumm = finalSumm + Number.parseInt($(this).find(".end-sum").text().trim());
      //   $('#final-sum').text(finalSumm);
      // });

      tdInputSumm(selector);

    });


    $(selInput).on('change, input', function() {

      var valueSum = $(this).val() * Number.parseInt($(this).parents('tr').find(selector).data('money'));
      $(this).parents('tr').find(selector).data('summ', valueSum);

      // vars = 0;
      $this = $(this);
      // $(this).parents('.calc__tab-content').find('td').each(function(index, el) {

      //   if($(this).children(selector).length > 0){
      //     vars = vars + Number.parseInt($(this).children(selector).data('summ'));
      //     console.log(vars);
      //   }

      // });
      // $(this).parents('table').find('.end-sum').text(vars);

      // finalSumm = 0;
      // $('.calc__tab-content').each(function(index, el) {
      //   finalSumm = finalSumm + Number.parseInt($(this).find(".end-sum").text().trim());
      //   $('#final-sum').text(finalSumm);
      // });
      tdInputSumm(selector);

    });

    $(selector).parents('.calc__tab-content').find('.down-btn, .up-btn').on('click', function() {

      var valueSum = $(this).parent().find('.stepper__input').val() * Number.parseInt($(this).parents('tr').find(selector).data('money'));
      $(this).parents('tr').find(selector).data('summ', valueSum);
      $this = $(this);
      tdInputSumm(selector);
      // vars = 0;
      // $(this).parents('.calc__tab-content').find('td').each(function(index, el) {

      //   if($(this).children(selector).length > 0){
      //     vars = vars + Number.parseInt($(this).children(selector).data('summ'));
      //   }

      // });
      // $(this).parents('table').find('.end-sum').text(vars);
      
      // finalSumm = 0;
      // $('.calc__tab-content').each(function(index, el) {
      //   finalSumm = finalSumm + Number.parseInt($(this).find(".end-sum").text().trim());
      //   $('#final-sum').text(finalSumm);
      // });

    });
  }


  function tdInputSumm(selector){
    vars = 0;
    $this.parents('.calc__tab-content').find('td').each(function(index, el) {

      if($(this).children(selector).length > 0){
        vars = vars + Number.parseInt($(this).children(selector).data('summ'));
      }

    });
    $this.parents('table').find('.end-sum').text(vars);

    finalSumm = 0;
      $('.calc__tab-content').each(function(index, el) {
        finalSumm = finalSumm + Number.parseInt($(this).find(".end-sum").text().trim());
        $('#final-sum').text(finalSumm);
      });
  }


}

if($('body').find('.trust').length > 0){
  $('.trust__advas-item').each(function(index, el) {
    if($(this).find('.trust-link').attr('href').trim() === ''){
      $(this).find('.trust-link').remove();
    }
  });
}

$('.slider-text').find('.slider-text-item').each(function(index, el) {
    if($(this).index() > 0){
      $(this).hide();
    }
});
if($('body').find('.process__video-text-wrap').length > 0){
  $('.process__video-text-wrap').find('.process__video-text').each(function(index, el) {
      if($(this).index() > 0){
        $(this).hide();
      }
  });
}

if($('body').find('.calc__tab-content').length > 0){
  $('.calc__tab-content tr').each(function(index, el) {
    if($(this).index() !== 1){
      $(this).find('.end-col').remove();
    }
  });
}

$('.open-master-js').on('click', function() {
  var name = $(this).parents('.amenities__item').find('.aminites-title-block').text().trim();
  $('#open-master-js').find('input[name="vibor"]').val(name);
});


$('a[href="#none"]').on('click', function(event) {
  event.preventDefault();
});










function simulate(element, eventName)
{
    var options = extend(defaultOptions, arguments[2] || {});
    var oEvent, eventType = null;

    for (var name in eventMatchers)
    {
        if (eventMatchers[name].test(eventName)) { eventType = name; break; }
    }

    if (!eventType)
        throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

    if (document.createEvent)
    {
        oEvent = document.createEvent(eventType);
        if (eventType == 'HTMLEvents')
        {
            oEvent.initEvent(eventName, options.bubbles, options.cancelable);
        }
        else
        {
            oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
            options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
            options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
        }
        element.dispatchEvent(oEvent);
    }
    else
    {
        options.clientX = options.pointerX;
        options.clientY = options.pointerY;
        var evt = document.createEventObject();
        oEvent = extend(evt, options);
        element.fireEvent('on' + eventName, oEvent);
    }
    return element;
}

function extend(destination, source) {
    for (var property in source)
      destination[property] = source[property];
    return destination;
}

var eventMatchers = {
    'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
    'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
}
var defaultOptions = {
    pointerX: 0,
    pointerY: 0,
    button: 0,
    ctrlKey: false,
    altKey: false,
    shiftKey: false,
    metaKey: false,
    bubbles: true,
    cancelable: true
}

// if($(window).width() > 991){
 $(document).mouseup(function(e){
    var container = $('.nav-list li');
    if (container.has(e.target).length === 0 && !container.is(e.target)){
        $('.nav-list li ul').removeClass('act');
    }
});
$('.nav-list li a[href="#none"]').on('click', function(event) {
    $('.nav-list li ul').removeClass('act');
  $(this).parents('li').find('ul').toggleClass('act');
});
// }