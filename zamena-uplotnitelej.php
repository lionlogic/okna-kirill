
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена уплотнителя алюминиевых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <p><img class="img_right" title="Уплотнитель для алюминиевых окон" src="/files/imges/plastic_windows/1uplotnitel dlj alyuminievyh okon.jpg" alt="Уплотнитель для алюминиевых окон в Москве">Компания производит ремонт и сервисное обслуживание любых видов алюминиевых окон в Москве и Московской области.</p>
            <p>Современное окно из алюминиевого профиля – это высококачественные, долговечные и надёжные конструкции. Алюминиевые окна отлично противостоят негативным атмосферным воздействиям и другим отрицательным факторам. Они герметичны, замечательно удерживают тепло и не пропускают шум улицы. </p>
            <p>Одной из главных составляющих является уплотнитель алюминиевых окон.</p>
            <p>Именно он, будучи прочным, эластичным и плотным, защищает помещения, обеспечивает его надежную тепло и шумоизоляцию, а также изолирует от влажности.</p>



            <h2>Виды и материалы уплотнителей</h2>

            <p>Чем качественнее уплотнитель для алюминиевого профиля, тем выше эксплуатационные характеристики ваших окон.</p>
            <p>В качестве специального материала уплотнителей применяют этилен, пропилен или другие уплотнительные материалы,  которые позволяют добиться отличных показателей герметичности.</p>
            <p>Специальные уплотнители для алюминиевых окон различают по следующим видам:</p>
            <ul>
                <li><p>термопластичный эластомер (ТРЕ) или термоэластопласт (ТЭП) – полимерный материал, который в условиях эксплуатации способен, деформироваться и восстанавливать свое прежнее состояние.</p></li>
                <li><p>этиленпропиленовый каучук (ЕРМ) - сополимер этилена и пропилена.</p></li>
                <li><p>силиконовый каучук (VMQ) - содержит винилзамещенные и метилзамещенные группы.</p></li>
                <li><p>хлоропреновый каучук (CR).</p></li>
            </ul>
            <p>Все они предназначены для использования приблизительно в одинаковых условиях. Разница лишь в нижней температуре, которая колеблется от -45° (ТРЕ, CR) до -60° (VMQ). Верхняя планка примерно одинакова +80° и +70° для ТРЕ.</p>

            <h3>Срок службы резинового уплотнителя</h3>
            <p>При своевременном и должном уходе это значение может составлять от шести до десяти лет. Здесь также имеет значение климатические условия, перепады температур, правильность эксплуатации, уход за уплотнениями. В любом случае для каждого окна эта цифра индивидуальна.</p>
            <p>Чаще всего замечает, что окно перестает служить как раньше, сам владелец. От окон веет холодом, во время дождя в помещение может проникать влага, на откосах появляется пыль с улицы, а шум машин становится громче, чем раньше. Всё это указывает на то-что, пришло время заменить уплотнители алюминиевых окон.</p>
            <p>Не рекомендуем делать это самостоятельно. Положитесь на компетентных работников нашей фирмы, имеющих опыт работы и специализированное оборудование для данной процедуры.</p>
            <p class="comment">Специалисты нашей службы осуществляют замену уплотнителя и регулировку оконной фурнитуры, качественно быстро и с гарантией в 365 дней.</p>

            <h2 id="price">Цены на уплотнители для алюминиевых окон</h2>


            <p>У нас представлен большой выбор уплотнителей для оконных систем: <a href="#tatprof">Татпроф</a>, <a href="#vsmpo">ВСМПО</a>, <a href="#newtec">NewTec</a>, <a href="#agrisovgaz">Агрисовгаз</a>, <a href="#sial">Сиал</a>, <a href="#realit">Реалит</a>, <a href="#aluteh">Алютех</a>, <a href="#alumark">Алюмарк </a>.</p>
            <div class="pricea">
                <div>
                    <p>*Стоимость указана за пагонный метр уплотнителя с учетом работы по его установке.</p>
                    <table class="table table-text-center table-uplot3" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr><td>Параметры и материал</td><td>Назначение</td></tr>

                        <tr><td colspan="2" class="grei"><h4 id="tatprof">Уплотнитель для оконных систем ТАТПРОФ, от <span class="text-line-through">270</span> <span class="color-red">250&nbsp;руб/п.м.</span></h4></td></tr>


                        <tr>
                            <td><img src="<?=$pathToImg?>/6ae3dd21f6752e971d4e945261a222d6.png" alt=""/><br>
                                <p>ТПУ-6001 (М=0.066кг/м)</p></td>
                            <td>Наружное уплотнение стекла</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/c08ae064151633064eb25bc7b83ca300.png" alt=""/><br>
                                <p>ТПУ-004ММ <br> (М=0.055кг/м) </p></td>
                            <td>Внутреннее уплотнение штапика</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/0073894b25d7bed41d33423c85f13c54.png" alt=""/><br><p>
                                    ТПУ-006-01ММ (М=0.031кг/м)</p></td>
                            <td>Притвор (внутренний)</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/8bb460a8bfd8a8428775d669d054c824.png" alt=""/><br><p>
                                    ТПУ-8901 (М=0.113кг/м)</p></td>
                            <td>Уплотнение внутреннее (средний контур) рамное</td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="vsmpo">Для оконных систем ВСМПО, от 250&nbsp;руб/п.м.</h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/0992cbeb94d6c87d919bb7d8817719e4.png" alt=""/><br><p>
                                    770.16.001</p></td>
                            <td>Уплотнитель стекла и профиля </td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/ff4750f51aa1c948c5b3153b337efca9.png" alt=""/> <br> <p>
                                    770.16.004</p></td>
                            <td>Уплотнение стекла и  штапика</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/3a0e2a43739eb1c581d8a357ad053cd9.png" alt="" /><br> <p>
                                    770.16.007А</p></td>
                            <td>Уплотнение рамы и створки (притвор) внутреннее </td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/b777c065a6854b2a440178b97fa55e5a.png" alt=""/> <br> <p>
                                    770.16.008</p></td>
                            <td>Уплотнение рамы и створки наружнее</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/4d80a6320bc90f1b9187fb2f5adce666.png" alt=""/> <br> <p>
                                    ТПУ-004</p></td>
                            <td>Уплотнение штапика и стекла</td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="newtec">Для систем NewTec, от <span class="text-line-through">270</span> <span class="color-red">250&nbsp;руб/п.м.</span></h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/7991e4bb1d31e306462575a62c7932d6.png" alt=""/> <br> <p>
                                    695001 (G001P)</p></td>
                            <td>Притвор и штапик</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/38aab94e57aa310635d83793c218e6d7.png" alt=""/> <br> <p>
                                    695003 (G001D)</p></td>
                            <td>Внутреннее уплотнение для штапика (3 мм)</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/ded38e0964b07ec8c9def0a5f43777a6.png" alt=""/> <br> <p>
                                    695012 (G012D)</p></td>
                            <td>Уплотнение среднее</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/30e9ceff826be88cd2595f4ab3d844f0.png" alt=""/> <br> <p>
                                    695017 (G017D)</p></td>
                            <td>Уплотнение притвора</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/fa2d6eece9ee2d82350e9edd4fbb08d8.png" alt=""/> <br> <p>
                                    695045 (G045D (G007D))</p></td>
                            <td>Уплотнение внешнее (3 мм)</td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="agrisovgaz">Для окон Агрисовгаз, от 250&nbsp;руб/п.м.</h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/51c00f73208cc5aa831e089539881666.png" alt=""/> <br> <p>
                                    G 003 D</p></td>
                            <td>Уплотнитель стеклопакета в ассортименте</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/7ef404b6a91a5aaefa4b74bd205be4c5.png" alt=""/> <br> <p>
                                    G 010 D</p></td>
                            <td>Уплотнение среднего контура</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/a8d157da7d14113245b8030afa4ecb9e.png" alt=""/> <br> <p>
                                    G 012 D</p></td>
                            <td>Уплотнитель средний АГС</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/12d1936f2b4a48a46474c7f223926cb4.png" alt=""/> <br> <p>
                                    G 017 D</p></td>
                            <td>Уплотнитель притворный створка, рама </td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="sial">Для оконных систем Сиал, от 250&nbsp;руб/п.м.</h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/173e0c94a7c48f08654a91f877e7e974.png" alt=""/> <br> <p>
                                    ТПУ-004мм</p></td>
                            <td>Уплотнитель заполнения (стеклопакета) в ассортименте</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/7b40e288464dd9299a00fecf646bcef5.png" alt=""/> <br> <p>
                                    ТПУ-006мм</p></td>
                            <td>Уплотнитель притвора входных дверей</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/bf407e98223557c84220f563944ce0a2.png" alt=""/> <br> <p>
                                    КПУ-17-1</p></td>
                            <td>Уплотнение основное, средний контур</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/406e2b3721784849e1e4ecda886968b3.png" alt=""/> <br> <p>
                                    Р-5</p></td>
                            <td>Уплотнитель оконный створочный, притворный</td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="realit">Для систем Реалит, от <span class="text-line-through">270</span> <span class="color-red">250&nbsp;руб/п.м.</span></h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/61d5f9d9df08b8c9cbd566e51f2db4fd.png" alt=""/> <br> <p>
                                    REG 001</p></td>
                            <td>Уплотнитель глухого остекления</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/02b53e6cb631afacd4f164e4dba1f235.png" alt=""/> <br> <p>
                                    REG 012</p></td>
                            <td>Уплотнение притворное</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/5e6658317329c27e200f7a773b78e828.png" alt=""/> <br> <p>
                                    REG 013</p></td>
                            <td>Уплотнитель ригельный </td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="aluteh">Для окон марки Алютех, от 250&nbsp;руб/п.м.</h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/cc05921b2329b19577b4179f9d297537.png" alt=""/> <br> <p>
                                    FRK98</p></td>
                            <td>Уплотнитель притвора оконный</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/94998b8ebb7c8eabc7026e206f60c209.png" alt=""/> <br> <p>
                                    FRK07</p></td>
                            <td>Уплотнитель притвора дверной створка, рама</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/a29573d7ccf5a85d9af0edcb76c3ef0b.png" alt=""/> <br> <p>
                                    FRK36</p></td>
                            <td>Уплотнитель стеклопакета в ассортименте</td>
                        </tr>

                        <tr><td colspan="2" class="grei"><h4 id="alumark">Для Алюмарк, от <span class="text-line-through">270</span> <span class="color-red">250&nbsp;руб/п.м.</span></h4></td></tr>

                        <tr>
                            <td><img src="<?=$pathToImg?>/3e342a182c1af4dc34ea2b41c091c148.png" alt=""/> <br> <p>
                                    ALM750072</p></td>
                            <td>Уплотнитель средний для распашных створок</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/c2b26034f7806128f53d936449665930.png" alt=""/> <br> <p>
                                    ALM770001</p></td>
                            <td>Уплотнитель притворный оконный для распашных створок</td>
                        </tr>
                        <tr>
                            <td><img src="<?=$pathToImg?>/05a7186b556e434ededd0c88ddcc6da4" alt=""/> <br> <p>
                                    ALM770020</p></td>
                            <td>Уплотнитель дверной </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <p class="comment">Если вы знаете, название марки ваших алюминиевых окон и она отсутствуют в данной таблице, не переживайте, свяжитесь с нами и мы обязательно решим вашу проблему.</p>


            <h2>Телефоны для связи:</h2>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloakdbb365b012299be99caa638324d62545"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloakdbb365b012299be99caa638324d62545').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addydbb365b012299be99caa638324d62545 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addydbb365b012299be99caa638324d62545 = addydbb365b012299be99caa638324d62545 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_textdbb365b012299be99caa638324d62545 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakdbb365b012299be99caa638324d62545').innerHTML += '<a ' + path + '\'' + prefix + ':' + addydbb365b012299be99caa638324d62545 + '\'>'+addy_textdbb365b012299be99caa638324d62545+'<\/a>';
                    </script></p>
            </div>
        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>