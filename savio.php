
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Ремонт окон и дверей с фурнитурой SAVIO				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>







	
	<div class="container">
        <div itemprop="articleBody">
            <img src="<?=$pathToImg?>/savio.png" alt="Логотип фурнитуры для окон и дверей Savio" title="Логотип фурнитуры для окон и дверей Savio" class="img_right">
            <p>Компания предлагает услуги по ремонту алюминиевых окон и дверей с элементами фурнитуры SAVIO. Мы модернизируем ваши окна, врежем форточки, фрамуги, замки безопасности.</p>

            <p>Мы ремонтируем, меняем и модернизируем детали Savio (Савио). В нашем арсенале полные комплекты для современных дверных и оконных алюминиевых конструкций (наклонно-сдвижных, параллельно-сдвижных, портальных), окон с европазом. </p>

            <p class="comment">Обратиться за бесплатной консультацией специалиста по ремонту окон или заказать выезд мастера на дом можно по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a>.</p>


            <h2>Особенности SAVIO</h2>
            <p>В первую очередь фурнитура SAVIO – это неповторимый дизайн и надежное открывание:</p>
            <ul>
                <li>поворотное;</li>
                <li>поворотно-откидное;</li>
                <li>скрытое поворотное;</li>
                <li>фрамуги различной конфигурации.</li>
            </ul>

            <h2>Ассортимент Savio</h2>

            <p>Сегодня мы предлагаем нашим заказчикам: петли, ручки для окон и дверей, ручки нажимные, стационарные и офисные ручки-скобы, ограничители, фрамужные и поворотно-откидные&nbsp; механизмы( Ribanta-5), системы экстренного открывания, щеколды, шпингалеты (Impreverto), защелки, аксессуары для раздвижных окон, крепления ручек на стекле.</p>
            <p><img src="<?=$pathToImg?>/savio-furnitura.jpg" alt="Фурнитура Savio" style="padding:0; border:0;"></p>
            <p>В широком ассортименте, пользующиеся спросом петли серии Mechanica, ручки Manon, Bernini.</p>

            <p>Фурнитура представлена в оттеночном диапазоне: окрашенные по шкале RAL, неокрашенные, нержавейка, матированная сталь, покрытие под золото, латунь, полированную сталь.</p>


            <p class="comment">Наше сервисное обслуживание всегда быстро, качественно, с гарантией от 1 год.</p>

            <h3>Фурнитура Savio для тяжелых створок</h3>

            <p>Комплекты предлагаются для профильной системы ALUTECH ALTC48, ALTC43, ALTVC62, ALTW65, ALTW72. Вес створки может быть в диапазоне (110-170 кг). Время установки комплекта от 3 до 5 минут. Простые естественные регулировки в трех плоскостях.</p>




 	</div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>