
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_wood.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена стеклопакета				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <p>Стеклопакет вакуумный – конструкционная часть евроокна. Изготавливается из двух и более листов стёкол, склеенных по периметру, дистанционные рамки образуют герметичные камеры, которые заполнены осушенным воздухом или газом.</p>

            <p><img src="<?=$pathToImg?>/steklopaket.jpg" alt="Производство"></p>

            <p>Изготовим светопрозрачные конструкции любой формы (треугольник, трапеция, арка, круг). Производим изделия с любыми видами формул ( 4 24 4; 4 12 4 12 4) стёкол и покрытий: защитные, триплекс, армированные, витраж и т. п.</p>
            <br>

            <h2><a name="uluschit"></a>Оказываемые услуги:</h2>

            <ul>
                <li>ремонт стеклопакетов, замена битых стеклопакетов в окнах, на заказ по размерам, балконных дверях, в том числе на высоте в зенитных фонарях, зимних садах, витринах, фасадных системах с использованием специального оборудования;</li>
                <li>замена стекла в пластиковом окне, обычного стекла на стеклопакет или однослойного стеклопакета на двойной;</li>
                <li>замена обычных пакетов на многофункциональные (тонирование, триплекс, антивандальные, армированные, многокамерные, шумопоглощающие, энергосберегающие);</li>
                <li>замена 24 стеклопакета окна на сэндвич-панель;</li>
                <li>устанавливаем декоративные (цветные, фальш-переплет, раскладки);</li>
                <li>замена глухого оконного стеклопакета на открывающуюся створку;</li>
                <li>изготавливаем конструкции с отверстием под кондиционер.</li>
            </ul>

            <p class="comment"><a></a>Позвоните нам <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a> или оставьте заявку на сайте. Всем нашим клиентам предоставляется гарантия на работы от 1 года!</p>

            <p><img src="<?=$pathToImg?>/02.jpg" alt="Конструкция стеклопакета"></p>

            <a name="price"></a>
            <h2>Цены на стеклопакеты без рамы</h2>

            <p>*Указана стоимость изготовления за м<sup>2</sup> с работой по его установке. Информация не является публичной офертой, окончательную стоимость работ определит мастер сервиса после проведения осмотра и замеров.</p>

            <table class="table table-text-center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td><span class="mobile">Пластик</span><span class="desctop">Пластиковые конструкции</span></td>
                    <td><span class="mobile">Алюминий</span><span class="desctop">Алюминиевые конструкции</span></td>
                    <td><span class="mobile">Дерево</span><span class="desctop">Деревянные конструкции</span></td>
                </tr>
                <tr class="grei">
                    <td colspan="3"> Стеклопакеты однокамерные, (СПО) одинарный, толщина от&nbsp;14&nbsp;до&nbsp;30&nbsp;мм.</td>
                </tr>
                <tr>
                    <td>3500</td>
                    <td>4500</td>
                    <td>5500</td>
                </tr>
                <tr class="grei">
                    <td colspan="3"> Стеклопакеты двухкамерные, (СПД) двойной от&nbsp;32&nbsp;до&nbsp;44&nbsp;мм.</td>
                </tr>
                <tr>
                    <td>4500</td>
                    <td>5000</td>
                    <td>6000</td>
                </tr>
                <tr class="grei">
                    <td colspan="3">Оконное стекло толщина от 4 до 6 мм.</td>
                </tr>
                <tr>
                    <td colspan="3">2000 - 2500</td>
                </tr>

                <tr>
                    <td colspan="3" class="table-comment">Цены указаны в рублях за м<sup>2</sup></td>
                </tr>
                </tbody>
            </table>



            <table class="table table-text-center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="grei">
                    <td colspan="4">Дополнительные работы</td>
                </tr>
                <tr>
                    <td>Стеклопакет триплекс</td>
                    <td>+ 2000</td>
                </tr>
                <tr>
                    <td>Энергосберегающий, низкоэмиссионный i-стекло, К-стекло: (4 10 4 10 4i)</td>
                    <td><b>Акция!</b></td>
                </tr>
                <tr>
                    <td>Стёкла закалённые</td>
                    <td>+ 2000</td>
                </tr>
                <tr>
                    <td>Шумопоглощающие, бесшумные, шумозащитные, пример формулы: (6 12 4 12 6)</td>
                    <td>+ 1500</td>
                </tr>
                <tr>
                    <td>Тонированные стеклопакеты</td>
                    <td>от 1000</td>
                </tr>
                <tr>
                    <td>Сложные формы стёкол (трапеция, треугольник, арка)</td>
                    <td>от 35% до 55%</td>
                </tr>
                <tr>
                    <td>Декоративные раскладки внутри</td>
                    <td>от 350 за 1 секцию</td>
                </tr>
                <tr>
                    <td>Размер, стёкол менее 1 м<sup>2</sup></td>
                    <td>считаем как 1 м<sup>2</sup></td>
                </tr>
                <tr>
                    <td>Размер стёкол от 1,2 м<sup>2</sup> до 2,5 м<sup>2</sup></td>
                    <td>+ 50%</td>
                </tr>
                <tr>
                    <td>Замена большого стеклопакета-джамбо (от 2,5 м)<sup>2</sup></td>
                    <td>по факту замера</td>
                </tr>
                <tr>
                    <td colspan="3" class="table-comment">Цены указаны в рублях за м<sup>2</sup></td>
                </tr>
                </tbody>
            </table>



            <table class="table table-text-center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td colspan="2">Доставка</td>
                </tr>

                <tr>
                    <td>Доставка легковой машиной&nbsp;</td>
                    <td>2000 руб.</td>
                </tr>
                <tr>
                    <td>Доставка Газель (Москва)</td>
                    <td>3500 руб.</td>
                </tr>
                <tr>
                    <td>Доставка за пределы МКАД</td>
                    <td>+ 50 рублей за км.</td>
                </tr>
                </tbody>
            </table>
            <h2>Работы по видам профиля</h2>

            <h3 id="pvh">Замена разбитого стеклопакета в пластиковом окне</h3>

            <img src="<?=$pathToImg?>/2.jpg" alt="Замена стеклопакета в ПВХ окне" class="img_right">
            <p>При первом выезде, замерщик производит снятие размеров стекла с точностью до мм. т. к. у каждого стеклопакета свой персональный размер. Далее мастер отправляет замер на производство, изготовление новой конструкции, доставка и установка занимает от 1 до 6 рабочих дней (срок зависит от конфигурации, размеров, состава и загрузки производства).</p>
            <p>Демонтаж и установка нового однокамерного или двойного стеклопакета:</p>
            <ul>
                <li>Мастер производит выемку удерживающих штапиков специальным инструментом (чтобы не повредить поверхность окна, двери);</li>
                <li>Извлекает расколотый пакет, очищает раму от мусора и осколков;</li>
                <li>Производит установку нового стеклопакета, расклинивает специальными подкладками;</li>
                <li>Монтирует штапики в раму, створку, проверяет функциональность фурнитуры окна (открытие, закрытие, откидывание).
                </li><li>Специалист выносит разбитый не далее лестничной площадки, работы завершает подпись договора, гарантия на работы 365 дней.</li>
            </ul>

            <h3 id="alum">Замена стеклопакетов в алюминиевых окнах, дверях, офисных перегородках, фасадах, зимних садах.</h3>

            <p><img src="<?=$pathToImg?>/3.jpg" alt="Замена стеклопакета в окне из алюминия" class="img_right">В алюминиевых системах (тёплых, холодных) со штапиками (удерживающие планки), замер и установка новых стеклопакетов  происходит аналогично пластиковым окнам. Небольшая разница в демонтаже (вначале извлекается резиновый уплотнитель, затем штапики, стеклопакет).</p>
            <p>В фасадных окнах и зимних садах штапики отсутствуют, замена вышедших из строя стёкол происходит с уличной стороны, на высоких этажах  работают наши промышленные альпинисты, также применяем спец-технику.</p>

            <h3 id="derevo">Замена стеклопакетов в деревянных окнах современного типа</h3>
            <p><img src="<?=$pathToImg?>/1.JPG" alt="Замена стеклопакета в окне из дерева" class="img_right">Заменить разбитый стеклопакет в деревянном окне сложней, чем в других конструкциях. Это связанно с конструктивным решением крепления стёкол. Стеклопакет и удерживающие штапики вклеены в рамы, створки на силиконовый герметик. Соответственно для демонтажа требуются большие трудозатраты.</p>

            <p>Установка-вклейка производится на прозрачный силикон, далее:</p>
            <ul>
                <li>расклинка рихтовочными подкладками;</li>
                <li>установка фтапиков на силикон, также на тонкие гвозди;</li>
                <li>монтаж створки.</li>
            </ul>



            <h2><a name="zamena"></a>Как заказать?</h2>
            <ol>
                <li>Позвоните <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a> или <a href="#" class="zakaz">оставьте заявку на сайте</a>.</li>
                <li>Через несколько часов встретьте мастера, выполняющего диагностику и замер.</li>
                <li>Изготовление на производстве займет 1-2 дня.</li>
                <li>Повторный визит мастера и проведение работ займет до 1 часа.</li>
            </ol>


            <a name="vremia"></a>
            <h2>Срочная замена стеклопакета</h2>
            <p>Мы также предоставляем услугу срочного ремонта окон. Осуществим изготовление нового и замену вашего старого стеклопакета в течении суток после обращения.</p>
            <table class="table table-text-center" cellspacing="0" cellpadding="0">
                <tbody>
                <tr><td>Наименование работ, услуг</td><td>Срочный ремонт</td><td>Обычный ремонт</td></tr>
                <tr>
                    <td>Вызов мастера, замерщика по телефону</td>
                    <td>1-2 минуты</td>
                    <td>1-2 минуты</td>
                </tr>
                <tr>
                    <td>Приезд мастера</td>
                    <td>в течении 3-часов</td>
                    <td>в течении дня</td>
                </tr>
                <tr>
                    <td>Замер 1 окна, стекла</td>
                    <td>в течении 1 часа</td>
                    <td>в течении 1 часа</td>
                </tr>
                <tr>
                    <td>Изготовление стеклопакета</td>
                    <td>минимум 12 часов (вне очереди)</td>
                    <td>2-3 дня</td>
                </tr>
                <tr>
                    <td>Доставка конструкции</td>
                    <td>в течении дня</td>
                    <td>в течении дня</td>
                </tr>
                <tr>
                    <td>Демонтаж старого, монтаж нового</td>
                    <td>1 день</td>
                    <td>1 день</td>
                </tr>
                <tr>
                    <td>Итого в среднем</td>
                    <td>1-2 дня</td>
                    <td>3-5 дней</td>
                </tr>
                </tbody>
            </table>

            <p class="img240">
                <img src="<?=$pathToImg?>/Zamena-steklopaketa.jpg" alt="Замена разбитого стеклопакета в Москве">
                <img src="<?=$pathToImg?>/Zamena-razbitogo-steklopaketa.JPG" alt="Изготовление больших стёкол">
                <img src="<?=$pathToImg?>/Zamena-steklopaketa-v-okne.jpg" alt="Ремонт разбитого стеклопакета">
                <img src="<?=$pathToImg?>/Zamena-steklopaketa-plastikovogo-okna.jpg" alt="Замена стёкол в пластиковых окнах" preview_height="164" border="0">
            </p>
            <a name="steklo"></a>

            <h2>Ремонт стекла или новый стеклопакет?</h2>

            <p>Замена разбитого стекла в стеклопакете окна не означает, что ремонт произойдет быстрее. В любом случае стеклопакет, как цельную конструкцию следует демонтировать полностью. Замена разбитого в нём стекла, установка его в рамку, склеивание, заполнение внутреннего пространства аргоном, сушка – процессы которые должны происходить в производственном цехе.</p>

            <p class="comment">Стоимость нового стеклопакета, не сильно отличается от цены замены одного стекла. Выгодно это только в том случае если пакет содержит дорогие составляющие – например, триплекс, витраж, орнамент, или тонирование.</p>

            <h2>Замена однокамерного стеклопакета на двухкамерный</h2>

            <p>Для такой замены также требуется полный демонтаж старых стёкол и штапиков. Двойные стеклопакеты на порядок улучшают шумоизоляционные свойства, помогают существенно снизить теплопотери.</p>

            <p><img src="<?=$pathToImg?>/steklopaket-1-2.jpg" alt="Виды стеклопакетов"></p>
            <h2><a name="pricshiny"></a>Топ-4 причин обращений по стеклопакетам:</h2>

            <ol>
                <li>деформация, нарушение герметичности, появление конденсата внутри;</li>
                <li>трещины на стёклах, глубокие царапины, сколы, окалины, выгорание на солнце (пожелтение);</li>
                <li>желание улучшить функциональные свойства окон, повысить шумоизоляцию, тонировка;</li>
                <li>повысить эстетический вид (шпросы, фальш-накладки, витражное стекло).</li>
            </ol>

            <p class="comment">Также наши специалисты оценят правильность монтажа рам в оконных проемах. Возможно, именно из-за неправильной установки, стёкла потрескались.</p>



            <h2>Замена разбитого стеклопакета - фотоотчет</h2>
            <img src="<?=$pathToImg?>/66d962c817a5c7405e0728b2b1a39f5e.jpg" alt=""/>
            <img src="<?=$pathToImg?>/dfeac3def814719443f922b44182c164.jpg" alt=""/>
            <img src="<?=$pathToImg?>/ae70db760e62cfc47422bca1bbb8d5da.jpg" alt=""/>
            <img src="<?=$pathToImg?>/b1b4b7f27ffb95509884c0e96e327340.jpg" alt=""/>
            <img src="<?=$pathToImg?>/f27ff00206a7667643c559d596b8bfe1.jpg" alt=""/>
            <img src="<?=$pathToImg?>/7aabb1ef959e2e1e7b57cbe01c150a93.jpg" alt=""/>
            <img src="<?=$pathToImg?>/a3a5d81e9da67aa784268791fe4b012c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/cf60574d624fcfe4c56090b01e5b8aeb.jpg" alt=""/>
            <img src="<?=$pathToImg?>/3a7f7dca48d82eea9c6ad7c78a07176b.jpg" alt=""/>
            <img src="<?=$pathToImg?>/3f912e7d4b142ae3624c6652ff1d9f0d.jpg" alt=""/>
            <img src="<?=$pathToImg?>/7f3d6b3b0e96e0e3405104bd8ecdc27e.jpg" alt=""/>



            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloakb8bb822d7d2e543e74034c5cc78fb60b"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloakb8bb822d7d2e543e74034c5cc78fb60b').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addyb8bb822d7d2e543e74034c5cc78fb60b = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addyb8bb822d7d2e543e74034c5cc78fb60b = addyb8bb822d7d2e543e74034c5cc78fb60b + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_textb8bb822d7d2e543e74034c5cc78fb60b = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakb8bb822d7d2e543e74034c5cc78fb60b').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyb8bb822d7d2e543e74034c5cc78fb60b + '\'>'+addy_textb8bb822d7d2e543e74034c5cc78fb60b+'<\/a>';
                    </script></p>
            </div>

        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>