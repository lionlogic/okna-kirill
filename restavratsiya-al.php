
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Регулировка алюминиевых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>







	
	<div class="container">
        <div itemprop="articleBody">
            <p>Регулировка алюминиевых окон, необходимое условие в эксплуатации современных светопрозрачных конструкций.</p>




            <p>Компания предоставляет качественное сервисное обслуживание «теплых» профиль которых оснащен термомостом, офисных "холодных", оконных и дверных алюминиевых систем, также раздвижных и фасадных.</p>

            <p>Наши высококвалифицированные сотрудники осуществляют регулировку фурнитур любых ведущих Российских и мировых брендов, алюминиевый. Мы регулируем окна ШУКО (SCHUCO), NEW TEC, Realit и другие.</p>

            <ul class="ul-inline ul-utepalum">
                <li><img src="<?=$pathToImg?>/a1.png"></li>
                <li><img src="<?=$pathToImg?>/a2.png"></li>
                <li><img src="<?=$pathToImg?>/a3.png"></li>
            </ul>


            <h2>Перечень работ при регулирование окон</h2>
            <p>Наличие современного высокотехнологичного оборудования позволяет производить работы в том числе раздвижных окон. Мастера компании «Помощь окнам» на профессиональном уровне осуществляют широкий перечень услуг по ремонту алюминиевых оконных блоков:</p>
            <ul>
                <li>замену стеклопакетов на изделия специального назначения – энергосберегающие, шумопоглощающие, витражные, триплекс, армированные;</li>
                <li>регулировку и выравнивание геометрии створок;</li>
                <img src="/files/imges/god.png" border="0" alt="Гарантия 1 год" class="img_right">
                <li>смену, смазку, регулировку изношенной фурнитуры;</li>
                <li>замену уплотнительного материала;</li>
                <li>покраску рам;</li>
                <li>монтаж отливов, козырьков, откосов;</li>
                <li>установку москитных сеток.</li>
            </ul>
            <p class="comment">Качество нашей работы гарантируется в течении 1 года по договору!</p>




            <h2>Регулировка алюминиевых окон - фотоотчет</h2>
            <img src="<?=$pathToImg?>/5d320032339c1e1acd4bb80d902fa1ee.jpg" alt=""/>
            <img src="<?=$pathToImg?>/b7d7129cd62196da6479224823edaa4c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/b3aee6039ce5a4b62ced64d5ded164be.jpg" alt=""/>




            <h2>Телефоны для связи:</h2>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloakff9ab84dec32cdad87da0544fe477026"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloakff9ab84dec32cdad87da0544fe477026').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addyff9ab84dec32cdad87da0544fe477026 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addyff9ab84dec32cdad87da0544fe477026 = addyff9ab84dec32cdad87da0544fe477026 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_textff9ab84dec32cdad87da0544fe477026 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakff9ab84dec32cdad87da0544fe477026').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyff9ab84dec32cdad87da0544fe477026 + '\'>'+addy_textff9ab84dec32cdad87da0544fe477026+'<\/a>';
                    </script></p>
            </div>

            <p>Специалистами нашей сервисной службы производится реставрация поверхности алюминиевого профиля. Устранение дефектов требует обладания профессиональными навыками и специальными материалами. Царапины закрашиваются особой порошковой краской, выбранной по шкале RAL. Более глубокие трещины покрываются пленкой для ламинации алюминиевого профиля.</p>







        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>