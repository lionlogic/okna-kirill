
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Ремонт алюминиевых дверей				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <div class="img_right"><img src="<?=$pathToImg?>/80d0c965532e658a3b0100ffffa3dd4c.jpg" alt=""/></div>
            <p>У нас вы можете заказать ремонт алюминиевых дверей любого производителя  с различной фурнитурой, также, модернизировать дверь из алюминия, а именно:</p>
            <ul>
                <li>устранить проблемы замка или поменять его на новый;</li>
                <li>отрегулировать или заменить фурнитуру двери;</li>
                <li>устранить дефекты (сколы, царапины и т.п.) профиля;</li>
                <li>решить проблему с продуванием и сквозняками (замена уплотнителей);</li>
                <li>добавить в конструкцию двери дополнительные точки прижимов, элементы (например, дополнительную фрамугу).</li>
            </ul>

            <p class="comment">Вы можете получить бесплатную консультацию или задать любой интересующий вопрос по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a> и наши специалисты решат любую вашу проблему.</p>
            <ul class="ul-inline ul-utepalum">
                <li><img src="<?=$pathToImg?>/a1.png"></li>
                <li><img src="<?=$pathToImg?>/a2.png"></li>
                <li><img src="<?=$pathToImg?>/a3.png"></li>
            </ul>

            <p>Все наши мастера имеют не только соответствующие сертификаты, но и большой опыт работы с алюминиевыми конструкциями различных производителей:&nbsp;NEWTEC, ВСМПО,&nbsp;AGS (АгриСовГаз), SCHUCO, ТАТПРОФ, РЕАЛИТ, PROVEDAL, ALUTECH (Алютех), HUECK/HARTMANN&nbsp;и т. д.</p>









            <h2 id="price">Ремонт алюминиевых дверей - цены</h2>

            <p>Указанные в прайсе расценки не являются публичной офертой, окончательную стоимость работ определит мастер сервиса после <a href="#" class="zakaz">выезда и проведения осмотра</a>. </p>

            <div class="pricea">
                <div>
                    <table class="table table-5col table-alum" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr class="head">
                            <td class="col1">№</td>
                            <td class="col2">Вид работ</td>
                            <td class="col3">Примечание</td>
                            <td class="col4">Ед. изм.</td>
                            <td class="col5">Цена, руб.</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Регулировка петель, ответных планок</td>
                            <td>Гарантирует плавное открывание, уменьшает проникновение шума и пыли с улицы, сохраняет тепло в помещении</td>
                            <td rowspan="5">шт.</td>
                            <td>1200</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Вытягивание геометрии полотна створки стеклопакетом</td>
                            <td>Выравнивает геометрии (восстановление прямых углов), что позволяет двери плотно закрываться</td>
                            <td>2000</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Т/о (чистка, смазка дверных механизмов и уплотнителя)</td>
                            <td>Проводится 1-2 раз в год, обеспечивает долговечность работы конструкции&nbsp;</td>
                            <td>1450</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td><a href="/foto/278-remont-alyuminievoj-dveri.html" title="Замена петель алюминиевых дверей">Замена петель входной группы</a></td>
                            <td>
                                <p>&nbsp;Полная замена старых петель (установка новых двухлепистковых, трёхлепистковых производитель&nbsp;Savio)</p>
                            </td>
                            <td>&nbsp;от&nbsp;4000 до&nbsp;4500</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Замена комплекта поворотной фурнитуры (лёгкая балконная дверь)</td>
                            <td>Монтаж и регулировка нового комплекта&nbsp;SAVIO S.p.A.</td>
                            <td>от 11000</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Установка нового уплотнителя</td>
                            <td>Вернет вашим дверям утраченные полезные свойства</td>
                            <td>м.п.</td>
                            <td>от 200</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Установка дверного доводчика</td>
                            <td>Также замена старых доводчиков (рычажные, маятниковые)</td>
                            <td rowspan="5">шт.</td>
                            <td>от 4700</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Замена, установка ответных планок (дополнительная точка запирания)</td>
                            <td>Монтаж дополнительных промежуточных регулируемых точек запирания, для увеличения прижима</td>
                            <td>1000</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td><a href="/foto/314-ustanovka-shpingaleta.html" title="Установка дверного шпингалета">Установка накладного рычажного шпингалета</a></td>
                            <td>Замена не работающих дверных шпингалетов,&nbsp;<span>SAVIO</span>&nbsp;</td>
                            <td>1900</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Установка ответных планок для замков</td>
                            <td>Монтаж новой регулируемой ответной планки для замков с &nbsp;обычными и падающими ригелями&nbsp;</td>
                            <td>1000</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Дверные ручки</td>
                            <td>Замена дверных ручек (нажимные гарнитуры, ручка с ключом, стационарные ручки (скоба), антипаниковые ручки</td>
                            <td>от 2500</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p class="comment">Заказать бесплатный выезд специалиста вы можете по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a> или заполнив форму обратной связи.</p>
            </div>

            <h2>Причины поломки алюминиевых дверей</h2>
            <ul>
                <li>некачественный монтаж;</li>
                <li>неправильная эксплуатация конструкции;</li>
                <li>простой износ частей фурнитуры;</li>
                <li>заедание или поломка механизма замка (относительно недорогой ремонт);</li>
                <li>старение уплотнителя.</li>
            </ul>

            <h2>Особенности ремонта дверей из алюминия&nbsp;</h2>
            <ul>
                <li>двери, после их поломки, требуют наиболее оперативного вмешательства специалиста, т. к. они связанны с наибольшей проходимостью и любая поломка способна быстро масштабироваться;</li>
                <li>в виду сложности конструкции, осуществлять самостоятельный ремонт в кустарных условиях не рекомендуется;</li>
                <li>при замене частей, лучше использовать только оригинальные комплектующие и фурнитуру.</li>
            </ul>
            <p class="comment">Доверьте нам свои двери из алюминия, и мы сможем обеспечить Вас отличным комфортом и сервисом по привлекательной цене.</p>



            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong>{{SET PHONE}}</p>
                <p><strong>WhatsApp: </strong>{{SET PHONE}}</p>
                <p><strong>E-mail: </strong><span id="cloaka264ea90e7553832e1e411562b509f53"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloaka264ea90e7553832e1e411562b509f53').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addya264ea90e7553832e1e411562b509f53 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addya264ea90e7553832e1e411562b509f53 = addya264ea90e7553832e1e411562b509f53 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_texta264ea90e7553832e1e411562b509f53 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloaka264ea90e7553832e1e411562b509f53').innerHTML += '<a ' + path + '\'' + prefix + ':' + addya264ea90e7553832e1e411562b509f53 + '\'>'+addy_texta264ea90e7553832e1e411562b509f53+'<\/a>';
                    </script></p>
            </div>



</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>