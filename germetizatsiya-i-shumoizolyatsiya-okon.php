
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Герметизация и шумоизоляция ваших окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>






	<div class="container">
        <div itemprop="articleBody">
            <img class="img_right" title="Герметизация крыши балкона" src="<?=$pathToImg?>/Germetizacija-balkona.JPG" alt="Герметизация балкона">
            <p>Наша компания выполняет работы по устранению протечек крыш, герметизации стеклопакетов, балконов и лоджий, наружных примыканий пластиковых, деревянных, алюминиевых окон и дверей, устранение шума дождя (шумоизоляция выносных козырьков, отливов). Работаем в Москве Новой Москве и всей области.</p>
            <p></p>Мы поможем, если: <p></p>
            <ul>
                <li>на внутренних откосах оконных конструкций появилась плесень, а ещё хуже, на балконе или лоджии капает вода с потолка;</li>
                <li>в дождливую погоду не возможно заснуть от барабанных&nbsp;звуков водоотлива (ударов капель об металлический козырёк). </li>
            </ul>
            <div class="clean"></div>
            <br>


            <h3>Герметизация пластиковых, алюминиевых, деревянных окон и дверей</h3>
            <p>Часто при монтаже пластиковых окон, мастера не закрывают монтажный шов от прямых солнечных лучей, как известно ультрафиолет разрушает пену, наверняка все наблюдали изменения цвета (пена становится тёмно рыжего цвета), а если дотронуться до неё она сыпется как песок.</p>
            <img class="img_right" title="Герметизация окон" src="<?=$pathToImg?>/Ustranenie-shuma-dozhdja.jpg" alt="Герметизация пластиковых окон">
            <p>2-3 года активного солнцепёка и под внутренние откосы начинает задувать ветер, итог, холод в помещение, образование благоприятной атмосферы для плесневых грибков.</p>
            <p>Меры по устранению:</p>
            <ul>
                <li>обрезаются излишки старой пены;</li>
                <li>производится повторная пропенка если требуется;</li>
                <li>гидроизоляция материалом Стиз;</li>
                <li>установка наличников, нащельников из ПВХ, металлических оцинкованных.</li>
            </ul>
            <p class="comment">Всем клиентам предоставляется 365 дней гарантии на проводимые работы.</p>



            <h3>Устранение протечек крыш, шумоизоляция козырьков балконов, лоджий</h3>
            <img class="img_right" title="Устранение протечек крыш" src="<?=$pathToImg?>/Ustranenie-protechek-krysh.JPG" alt="Устранение протечек крыш балконов">
            <p>Мы устраняем протекание крыш любых конструкций из дерева, пластика ПВХ, алюминия, а также наши специалисты производят шумоизоляционные работы, устраняя шум дождя козырьков, отливов выносных конструкций, капели кондиционеров.</p>
            <p>Работы производятся с уличной стороны при помощи специальной ленты герлен, изолон (вспененный полиэтилен).</p>
            <p>Эти материалы имеют высочайшие показатели, и обладают отличным шумопонижением.</p>




            <h2><a name="noise"></a>Стоимость герметизации и шумоизоляции окон</h2>
            <p>Указанные в прайсе расценки не являются публичной офертой, окончательную стоимость работ определит мастер сервиса после проведения осмотра. Стоимость работ с учётом материалов, за ширину ленты не более 400 мм.</p>

            <table class="table table-text-center" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>Наименование работ</td>
                    <td>Цена</td>
                </tr>
                <tr>
                    <td>Устранение протечек крыши&nbsp;с применением "Герлена"</td>
                    <td><span class="no-wrap">от 2500 руб.</span></td>
                </tr>
                <tr>
                    <td>Устранение протечек с применением "Силикона"</td>
                    <td>от 1900 руб.</td>
                </tr>
                <tr>
                    <td>Шумоизоляция&nbsp; окон&nbsp;(устранение шума дождя,&nbsp;шумоизоляция козырька балкона, лоджии)</td>
                    <td>от 2000<span class="no-wrap">руб/м.п.</span></td>
                </tr>
                <tr>
                    <td>Герметизация окон, гидроизоляция окон&nbsp;(наружных примыканий, снаружи) с применением "Стиз"</td>
                    <td>400 <span class="no-wrap">руб/м.п.</span></td>
                </tr>
                <tr>
                    <td>Просиликонить внутренние швы откосов окон (с демонтажом старого силикона)</td>
                    <td>250-300 <span class="no-wrap">руб/м.п.</span></td>
                </tr>
                <tr>
                    <td>Пропенка&nbsp;утепление монтажной пеной</td>
                    <td>480 <span class="no-wrap">руб/м.п.</span></td>
                </tr>
                <tr>
                    <td>Установка нащельников самоклеящихся пластиковых, металлических</td>
                    <td>от 350 руб/м.п.</td>
                </tr>
                <tr>
                    <td>Установка наличников оконных, дверных из ПВХ</td>
                    <td>от 500 руб/м.п.</td>
                </tr>
                </tbody>
            </table>
            <h2>Телефоны для связи:</h2>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloak350e9830f1a4aab817372e6f25a4b687"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloak350e9830f1a4aab817372e6f25a4b687').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy350e9830f1a4aab817372e6f25a4b687 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy350e9830f1a4aab817372e6f25a4b687 = addy350e9830f1a4aab817372e6f25a4b687 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text350e9830f1a4aab817372e6f25a4b687 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak350e9830f1a4aab817372e6f25a4b687').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy350e9830f1a4aab817372e6f25a4b687 + '\'>'+addy_text350e9830f1a4aab817372e6f25a4b687+'<\/a>';
                    </script></p>
            </div>


            <p class="comment">После проведения работ ваш балкон будет надёжно защищён от неблагоприятных воздействий внешней среды: дождя, снега, сквозняков, сырости и т. п.</p>



            <h3>Как сделать окно герметичным</h3>
        </div>
</div>

</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>