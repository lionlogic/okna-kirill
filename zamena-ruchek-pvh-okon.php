
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена ручки на пластиковом окне				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">

            <h2>Оконные ручки - цены</h2>
            <p>Ниже указан прайс-лист на наши услуги по ремонту и замене оконных пвх ручек. Обращаем Ваше внимание – цены указаны примерные. Точная стоимость работ рассчитывается индивидуально после осмотра объекта мастером.</p>
            <p class="comment">Уточнить детали и записаться на замер можно по телефону <a href="tel:{{SET PHONE_S}}">{{SET PHONE}}</a></p>

            <table class="table" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr class="head">
                    <td>№</td>
                    <td>Вид работ/услуг</td>
                    <td>Единица</td>
                    <td>Цена, руб.</td>
                </tr>
                <tr>
                    <td>3.0.</td>
                    <td><a href="/katalog-uslug/ruchki-balkonnye/belaya-metal-.html">Ручка оконная белая</a></td>
                    <td>шт.</td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>3.1.</td>
                    <td><strong><a href="/katalog-uslug/ruchki-balkonnye/4-8-pozicionnye-.html">Оконная ручка с ключом</a></strong></td>
                    <td>шт.</td>
                    <td>от 3500</td>
                </tr>
                <tr>
                    <td>3.2.</td>
                    <td>Ручка обратная с защёлкой на дверь</td>
                    <td>шт.</td>
                    <td>1300</td>
                </tr>
                <tr>
                    <td>3.3.</td>
                    <td>Гребёнка (ограничитель открывания)</td>
                    <td>шт.</td>
                    <td>500</td>
                </tr>
                </tbody>
            </table>


            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloak1f8e8a2e61bc4e6648f769f267184151"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloak1f8e8a2e61bc4e6648f769f267184151').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy1f8e8a2e61bc4e6648f769f267184151 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy1f8e8a2e61bc4e6648f769f267184151 = addy1f8e8a2e61bc4e6648f769f267184151 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text1f8e8a2e61bc4e6648f769f267184151 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak1f8e8a2e61bc4e6648f769f267184151').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy1f8e8a2e61bc4e6648f769f267184151 + '\'>'+addy_text1f8e8a2e61bc4e6648f769f267184151+'<\/a>';
                    </script></p>
            </div>






            <h2>Когда нужно заменить оконную ручку</h2>
            <p>Ручка – часть окна, которая особенно сильно подвержена износу. При несоблюдении правил эксплуатации или в случае неправильной регулировки ручки могут выходить из строя (прокручиваться, давать трещину), и в этом случае необходимо заменить элемент новым.</p>
            <p>Также услуга замены оконной ручки может быть нужна в случае, если:</p>
            <ul>
                <li>нужно усилить безопасность окна – установить ручку с детским замком или гребенкой;</li>
                <li>изменился интерьер комнаты и Вы хотите установить ручку другого цвета.</li>
            </ul>
            <p class="comment">Вы всегда можете получить бесплатную консультацию мастера по телефону <a href="tel:{{SET PHONE_S}}">{{SET PHONE}}</a></p>








            <h2>Как мы работаем</h2>
            <p>Наш мастер приедет к Вам, изучит сломанный механизм, покажет каталог продукции и поможет определиться с выбором. После этого проведет замену ручки по следующему алгоритму:</p>
            <ol>
                <li>Снимаем декоративную панель.</li>
                <li>Демонтируем сломанную деталь.</li>
                <li>Смазываем механизмы для плавного хода ручки.</li>
                <li>Устанавливаем новую деталь.</li>
            </ol>
            <p>По завершению работ мастер проверяет работоспособность установленной ручки во всех положениях.</p>
            <p>Мы принимаем к ремонту ручки: односторонние, двухсторонние, лепестки и ракушки, антивандальные. И на все работы — и замену, и ремонт — предоставляем гарантию качества до одного года, вне зависимости от стоимости оказанных услуг и вашей фурнитуры.</p>

            <p class="comment">Если не знаете, что именно сломалось — вызовите мастера на дом. Выезд по Москве, диагностика и расчет стоимости ремонта — бесплатно! Связаться с нами по номеру: <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>









        </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>