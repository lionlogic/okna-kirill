
<div class="section order" id="masterCall">
    <div class="container">

        <div class="order__content">
            <div class="order__info">
                <div class="title-wrap title-margin">

                    <h2 class="section-title">
                        ПОРЯДОК РАБОТ								</h2>
                </div>
                <div class="order__step">
                    <div class="order__step-item order__step-item-1">
                        <div class="order__step-img order__step-img-1">
                            <img src="wp-content/themes/src/assets/img/home/order/item-1.png" alt="">
                        </div>

                        <h4 class="order__step-text">
                            <span>1. </span>Мастер приедет на указанный адрес и оценит стоимость работ						</h4>
                    </div>

                    <div class="order__step-item order__step-item-2">
                        <div class="order__step-img order__step-img-2">
                            <img src="wp-content/themes/src/assets/img/home/order/item-2.png" alt="">
                        </div>

                        <h4 class="order__step-text">
                            <span>2. </span>Заключаем договор и приступаем к ремонте БЕЗ ПРЕДОПЛАТЫ						</h4>
                    </div>

                    <div class="order__step-item order__step-item-3">
                        <div class="order__step-img order__step-img-3">
                            <img src="wp-content/themes/src/assets/img/home/order/item-3.png" alt="">
                        </div>

                        <h4 class="order__step-text">
                            <span>3. </span>Выполняем ремонт и Вы принимаете работу						</h4>
                    </div>

                    <div class="order__step-item order__step-item-4">
                        <div class="order__step-img order__step-img-4">
                            <img src="wp-content/themes/src/assets/img/home/order/item-4.png" alt="">
                        </div>

                        <h4 class="order__step-text">
                            <span>4. </span>Подписываем Договор (гарантия). Оплачиваете. Вручаем Вам подарок!						</h4>
                    </div>
                </div>
            </div>

            <div class="order__form-wrap">
                <div class="big-form">
                    <div class="title-form">Вызвать мастера: <br> бесплатно заполните форму</div>


                    <form action="/obrob.php" method="POST">
                        <input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
                        <input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: <?=$_SERVER['HTTP_HOST']?> - ВЫЗОВ МАСТЕРА (в конце)">
                        <input type="hidden" name="formname" value="order-master">
                        <div class="form-item">
                            <!--  -->
                            <div class="form-item-block form-item-block-title">
                                <div class="form-item-block-num"></div>
                                <div class="form-item-block-inp">
                                    <div class="form-item-block-text">
                                        Выберите вид ремонта:
                                    </div>
                                </div>
                            </div>
                            <div class="form-item-block">
                                <div class="form-item-block-num n1">1</div>
                                <div class="form-item-block-inp">
                                    <!-- <input type="text" name="typeR" class="input input-sm select-input" required placeholder="Поменять прокладку"> -->

                                    <select name="DATA[UF_CRM_1572883588931]" class="input input-sm select-input">
                                        <option value="Утепление окон">Утепление окон</option>
                                        <option value="Ремонт окон (фурнитуры)">Ремонт окон (фурнитуры)</option>
                                        <option value="Шумоизоляция окон">Шумоизоляция окон</option>
                                        <option value="Ремонт фурнитуры">Ремонт фурнитуры</option>
                                        <option value="Установка разбитого стеклопакета">Установка разбитого стеклопакета</option>
                                        <option value="Москитные сетки">Москитные сетки</option>
                                        <option value="Жалюзи, рулонные шторы и т.д">Жалюзи, рулонные шторы и т.д</option>
                                        <option value="Установка аксессуаров ручка, гребенка, детский замок, и т.д">Установка аксессуаров ручка, гребенка, детский замок, и т.д</option>
                                        <option value="Другое">Другое</option>
                                    </select>
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-item-block form-item-block-title">
                                <div class="form-item-block-num"></div>
                                <div class="form-item-block-inp">
                                    <div class="form-item-block-text">
                                        В каком районе Москвы Вы находитесь:
                                    </div>
                                </div>
                            </div>
                            <div class="form-item-block">
                                <div class="form-item-block-num n2">2</div>
                                <div class="form-item-block-inp">
                                    <!-- <input type="text" name='raion' class="input input-sm select-input" required placeholder="Центр"> -->
                                    <select name="DATA[UF_CRM_1572885939971]" class="input input-sm select-input">
                                        <option value="Москва">Москва</option>
                                        <option value="Московская область ">Московская область </option>
                                    </select>
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-item-block form-item-block-title">
                                <div class="form-item-block-num"></div>
                                <div class="form-item-block-inp">
                                    <div class="form-item-block-text">
                                        Когда к Вам приехать
                                    </div>
                                </div>
                            </div>
                            <div class="form-item-block">
                                <div class="form-item-block-num n3">3</div>
                                <div class="form-item-block-inp">
                                    <input type="text" name='DATA[UF_CRM_1572885999831]' class="input input-sm" required placeholder="Дата ">
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-item-block form-item-block-title">
                            </div>
                            <div class="form-item-block">
                                <div class="form-item-block-num n4">4</div>
                                <div class="form-item-block-inp">
                                    <input type="tel" name='DATA[UF_CRM_1572883145518]' class="input input-phone input-sm" required placeholder="Ваш телефон ">
                                </div>
                            </div>
                            <!--  -->

                            <!--  -->
                            <div class="form-item-block form-item-block-title">
                            </div>
                            <div class="form-item-block">
                                <div class="form-item-block-num"></div>
                                <div class="form-item-block-inp">
                                    <button class="btn">Вызвать мастера</button>
                                    <div class="form-small">
                                        Среднее время ответа 2 минуты
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- контакты -->

<div class="contacts" id="contacts">
    <div class="container">
        <div class="title-wrap title-margin">
            <h2 class="section-title">
                СВЯЖИТЕСЬ С НАМИ И МЫ МАКСИМАЛЬНО ОПЕРАТИВНО РЕШИМ ВАШУ ЗАДАЧУ			</h2>
        </div>

        <div class="contacts__content">

            <div class="contacts__info">
                <div class="contacts__info-item">
                    <div class="small-title item-block-small">
                        Контакты
                    </div>

                    <div class="contacts__info-item-block">
                        <div class="text-small item-block-small item-block-small-marg">Есть вопрос? Звоните:</div>
                        <div class="elem__block">

                            <div class="img__block">
                                <img src="wp-content/themes/src/assets/img/main/phone.png" alt="">
                            </div>

                            <div class="elem__wrap">
                                <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                                <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            </div>
                        </div>
                    </div>

                    <div class="contacts__info-item-block">
                        <div class="text-small item-block-small">Физический адрес:</div>
                        <div class="elem__block">

                            <div class="img__block">
                                <img src="wp-content/themes/src/assets/img/contacts/pin.png" alt="">
                            </div>

                            <div class="elem__wrap">
                                <p class="contact-text">
                                    г. Москва, ул. Смольная 24а 								</p>
                            </div>
                        </div>
                    </div>

                    <div class="contacts__info-item-block">
                        <div class="text-small item-block-small">Почта:</div>
                        <div class="elem__block">

                            <div class="img__block">
                                <img src="wp-content/themes/src/assets/img/contacts/mail.png" alt="">
                            </div>

                            <div class="elem__wrap">
                                <a href="mailto:{{EMAIL}}" class="link-classic">info@info.ru</a>
                            </div>
                        </div>
                    </div>

                    <div class="contacts__info-item-block">
                        <div class="text-small item-block-small">Прием заявок диспетчером:</div>
                        <div class="elem__block">

                            <div class="img__block">
                                <img src="wp-content/themes/src/assets/img/contacts/time.png" alt="">
                            </div>

                            <div class="elem__wrap">
                                <p class="contact-text">
                                    с 7:00 - 23:00 без выходных<br />
                                    Работа офиса с 8:00 - 19:00 по будням<br />
                                    Работа склада (самовывоз) с 10.00 - 18.00 пн-сб, возможно другое время по согласованию с менеджером								</p>
                            </div>
                        </div>
                    </div>


                </div>
                <!--<div class="contacts__info-item">
                    <div class="small-title ">
                        Реквизиты
                    </div>
                    <div class="contacts__info-item-block">
                        <p>
                            {{FOOTER_TEXT}}
                        </p>
                    </div>
                </div>-->

                <div class="contacts__info-header">
                    <div class="logo">
                        <a href="#" class="logo__img">
                            <img src="wp-content/uploads/2019/05/logo.png" alt="">
                        </a>

                        <p class="logo__text">
                            mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 						</p>
                    </div>
                </div>
            </div>



            <div class="map-wrap">
                <div class="small-title">
                    Мы на карте
                </div>
                <div class="map-link"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A691d4360fb7f7e226f1d1388499a54866d4932dfdd5f068a3ff17358e58cafa5&amp;width=576&amp;height=361&amp;lang=ru_RU&amp;scroll=true"></script></div>
                <div class="map" id="map">

                </div>
            </div>
        </div>
    </div>