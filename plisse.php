
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Москитные сетки плиссе</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Москитные сетки плиссе			</h1>
            </div>





            <div itemprop="articleBody">
                <img class="img_right" title="Установка москитных сеток Плиссе" src="<?=$pathToImg?>/plisse2.jpg" alt="Установка москитных сеток Плиссе" border="0">

                <p>Наша компания в Москве изготавливает на заказ и продает москитные сетки Плиссе (гармошка) для установки на пластиковых, деревянных, алюминиевых окнах и дверях любой конструкции, в том числе и мансардных окнах.</p>
                <p>В наличие имеются алюминиевый профиль для рамки и плиссированное сеточное полотно различных оттенков и функций (антипыль, светоотражение), а также возможность изготовить сетку большого размера (макс. 3х6м).</p>
                <p>Мы предлагаем москитные сетки нескольких вариантов открывания:</p>

                <ul class="ul-inline ul-utep">
                    <li><img src="<?=$pathToImg?>/pli_1.jpg" alt="Вертикальное открывание москитных сеток плиссе">  <p>Вертикальное<br>открывание</p></li>
                    <li><img src="<?=$pathToImg?>/pli_2.jpg" alt="Горизонтальное открывание москитных сеток плиссе">  <p>Горизонтальное<br>открывание</p></li>
                    <li><img src="<?=$pathToImg?>/pli_3.jpg" alt="Штульповое открывание москитных сеток плиссе">  <p>Штульповое<br>открывание</p></li>
                    <li><img src="<?=$pathToImg?>/pli_4.jpg" alt="Реверсное открывание москитных сеток плиссе">  <p>Реверсивное<br>открывание</p></li>
                </ul>

                <p class="comment">У нас можно купить москитную сетку плиссе, изготовим за 3 дня, установим за 2 часа! Выезд мастера на замер <b>Бесплатно*, но с учётом заключения договора</b>.</p>

                <h2>Преимущества москитных сеток Плиссе</h2>
                <ul>
                    <li>разнообразные способы открытия (вертикальное, горизонтальное, штульповое, реверс),</li>
                    <li>плотно прилегают к проемам, не мешают открытию окон, дверей, раздвижных крыш, беседок, террас, окон на верандах, балконах и лоджиях,</li>
                    <li>простые при уходе и эксплуатации (необязательно снимать на зимний период, стойкость к воздействиям),</li>
                    <li>продолжительный срок службы всех механизмов (не имеют трения, не используются пружины) и полиамидного волокна сетки;</li>
                    <li>пластиковые уголки обеспечивают точное прилегание, даже к неидеально-установленным окнам, дверям,</li>
                    <li>разнообразие решения красивых идей (палитра оттенков рамок, полотна, возможность индивидуального декорирования).</li>
                </ul>
                <img src="<?=$pathToImg?>/plisse.jpg" alt="Сетки плиссе">



                <h2>Особенности изготовления</h2>
                <p>Все москитные сетки плиссе мы изготавливаем в собственной мастерской, оснащенной пневматическим оборудованием.</p>
                <img class="img_right" src="<?=$pathToImg?>/god.png" alt="Гарантия от 1 года">
                <p>Согласно выполненным на месте замерам из алюминиевого профиля собирается конструкция (фронтальная и подвижная рамки, кассета для хранения москитки) при помощи стандартных пластиковых и металлических деталей. На нее впоследствии натягивается сеточное полотно (у нас итальянского и российского производства). Сеточное полотно поставляется нам уже в сложенном «плиссированном» виде от производителей.</p>
                <p>При выезде на заказ наш специалист имеет при себе каталог нашей продукции. Вы сможете ознакомиться с образцами сеточного полотна, выбрать его оттенок, а также обсудить конструкцию желаемой сетки.</p>
                <p class="comment">Мы обязательно посоветуем вам наилучший вариант для Вашего дома или офиса. При установке мы соблюдаем европейские стандарты, обеспечиваем гарантии качества.</p>


                <h2>Цены в зависимости от количества покупаемых изделий</h2>

                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup> сетки при самовывозе</td>
                        <td>Цена м<sup>2</sup> с замером, доставкой и установкойв пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.</td>
                        <td>8000</td>
                        <td>9500 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>16 000</td>
                        <td>19 000 руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>24 000</td>
                        <td>28 500 руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">* Цена договорная</span></td>
                    </tr>
                    </tbody>
                </table>


                <h2>Стоимость работ по замеру и установке москитных сеток</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Вид работ</td><td>Цена</td></tr>
                    <tr>
                        <td>Замер в Москва</td>
                        <td>бесплатно (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км. от МКАД</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>1000 руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500&nbsp;руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>

                <h3>Наценка на москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Цвет профиля</td><td>Наценка</td></tr>
                    <tr>
                        <td>Белый/коричневый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 3500 к цене</td>
                    </tr>
                    </tbody>
                </table>




                <div class="otz_in" id="otz">
                    <h2>Отзывы клиентов</h2>

                    <div class="otz3">
                        <div class="otz">
                            <p class="desc">Изготовили для нашего дома отличную сетку плиссе большого размера (задняя дверь в дом увеличена по высоте). Качеством услуг довольны, сетка отличная, светоотражающая (реально работает!). Спасибо.</p>
                            <p class="date"><span id="today1">4 февраля</span>, Андрей</p>

                        </div>

                        <div class="otz">
                            <p class="desc">Замерщик приехал с образцами сеток и цветов, очень удобно, сразу смогли наглядно подобрать сетку в тон дерева. Подошло даже лучше, чем ожидали, сама сетка хорошая, добротная. Рекомендую.</p>
                            <p class="date"><span id="today2">24 января</span>, Ольга</p>
                        </div>

                        <div class="otz">
                            <p class="desc">Подлатали мне сетку плиссе на двери и починили окна, где ручка разболталась, где створка скрипит. Все быстро и за скромные деньги, отличный сервис.</p>
                            <p class="date"><span id="today3">16 января</span>, Роман</p>
                        </div>
                    </div>
                    <br>

                </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>