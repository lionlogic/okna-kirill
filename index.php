<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToCatImg = '/files/imges/CatImges' ?>
<div class="main" style="background: url(/images/synego.jpg) no-repeat center; background-size: cover;">
    <style>
        .header {
            background: #2d3439 !important;
        }
        .header a{
            color: #f4cb67;
        }
    </style>
    <style>
    .container {
        max-width: 85vw !important;
        margin: 0 auto !important;
    }
</style>
    <div class="header" style="">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="index.php" class="logo__img">
                        <img src="wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>

                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                СЕГОДНЯ ДЕЛАЕМ СКИДКУ -20%				</div>

            <h1 class="title title-main title-first">
                Ремонт окон в Москве и М.О.				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
            <form action="obrob.php" method="POST" class="main__form" onsubmit="yaCounter56025331.reachGoal('zayavka')">
                <input type="hidden" class="main-label">
                <input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
                <input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - ВЫЗОВ МАСТЕРА (гл.экран)">
                <h3 class="form__title">Срочный вызов мастера:</h3>
                <div class="main__form-wrap">
                    <div class="input-block-sm">
                        <input type="tel" name="DATA[UF_CRM_1572883145518]" class="input input-tel" required placeholder="+7 (999) 99 99 99">
                    </div>
                    <div class="btn-block-sm">
                        <button class="btn">
                            Вызвать мастера (бесплатно)
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<ul class="list-none right-bar">
    <!-- <li>
        <a href="kalkuljator-stoimosti/index.php"><span class="right-text">
				Расчёт стоимости
			</span>

            <span class="right-img">
				<img src="wp-content/themes/src/assets/img/main/calc.svg" alt="">
			</span></a>
    </li> -->
    <li>
        <a href="/#otzivi" class="master-open"><span class="right-text">
				Отзывы
			</span>

            <span class="right-img">
				<img src="wp-content/themes/src/assets/img/main/master.svg" alt="">
			</span></a>
    </li>
    <li>
        <a href="akcii-i-skidki.php"><span class="right-text">
				Вызвать мастера
			</span>

            <span class="right-img">
				<img src="/images/phone.png" alt="">
			</span></a>
    </li>
</ul>

<!-- 45 слов руководителя -->

<!-- 	<div class="director-wrap">
		<div class="director section active">
	<div class="container director__continer">
		<div class="title-wrap">
			<h2 class="section-title">
				Более 10 лет являемся экспертами в области оконных систем 			</h2>

			<p class="section-desc">
				С 2009 года мы предлагаем клиентам принципиально новое качество обслуживания в сфере ремонта металлопластиковых и ПВХ, алюминиевых, деревянных окон и дверей, а также светопрозрачных конструкций в Москве и области!			</p>
		</div>

		<div class="director__wrap">
			<div class="director__block">
				<h4 class="director__title">
					21 слово руководителя				</h4>

				<p class="text-sm">
					Наша фирма дорожит своей репутацией, поэтому у нас работает только
квалифицированный персонал, прошедший подготовку на специальных
курсах производителей оконных профильных систем и фурнитуры!				</p>
			</div>

			<div class="director__info">
				<div class="director__img">
					<img src="https://kislorod-ok.ru/wp-content/uploads/2019/09/dir.png" alt="">

					<div class="director__data">
						<div class="director__data-title">Артём Гринцевич</div>
						<div class="director__data-descr">Основатель компании</div>
					</div>
				</div>

				<div class="director__video">
					<div class="director__play" data-play="1221312"></div>

					<div class="director__video-title">
						ВИДЕО-ОБРАЩЕНИЕ ОСНОВАТЕЛЯ КОМПАНИИ					</div>
				</div>
</div>
</div>
</div>
</div>  -->	<!-- следующие услуги -->

	<div class="amenities section" style="padding-bottom: 0px">
	<div class="container">

		<div class="title-wrap title-margin">
			<h2 class="section-title fade_in">
				Мы уже более 15 лет оказываем следующие услуги:			</h2>
		</div>

        <style>
            .category-wrap {
                border-radius: 24px;
                border: 7px solid #f4cb67;
                margin-top: 100px;
            }
        </style>

        <div class="category-wrap">
            <div class="title-wrap title">
                <h3 class="title fade_in">
                    <font-size="7">	Ремонт пластиковых окон:		</font-size>	</h3>
            </div>
            <div class="amenities__content">
                <div class="amenities__block">
                    <!-- 1 -->
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/fit_930_519_false_crop_1000_562_0_52_q90_1120531_be1a973f31.webp'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Регулировка</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 350 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="regulirovka-plastikovykh-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена уплотнителя</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 150 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-uplotnitelya.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/maxresdefault.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Устранение запотевания</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 500 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="ustranenie-zapotevaniya-okna.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/src_d88a0411-3e20-406f-861d-b94e0fa5c584.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Устранение продувания</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 1.700 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="ustranenie-produvaniya.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed2.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Ремонт ПВХ дверей</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="remont-plastik-dverej.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/kak-uteplit-plastikovie-okna-uplotnitel.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Утепление</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 150 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="uteplenie-plastikovykh-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed3.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена створки</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 7.500 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-stvorok.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/zamena.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена стеклопакета</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-stvorok.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/shumoizoljacija-plastikovyh-okon.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Герметизация и шумоизоляция</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 250 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="germetizatsiya-i-shumoizolyatsiya-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed4.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена фурнитуры</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.500 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="smena-furnitury.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/6fca51198ca0bd9c4f41e5f17bc9d3d6.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена ручки</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 500 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-ruchek-pvh-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>



                    <span class="fix"></span>

                </div>
            </div>
        </div>

        <div class="category-wrap">
		    <div class="title-wrap title">
			<h3 class="title fade_in">
				<font-size="7">	Ремонт деревянных окон:		</font-size>	</h3>
		</div>


		    <div class="amenities__content">
			<div class="amenities__block">
				<!-- 1 -->
				<div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/ut80.jpg'); background-size: cover;">
					
					<div class="amenities__item-hover">
						<h3 class="amenities__item-title">
							<span class="aminites-title-block">Замена уплотнителя</span>
							<span class="amenities__item-subtitle">
															</span>
						</h3>
						

						<div class="amenities__item-price">
							<div class="amenities__item-price-old"></div>
							<div class="amenities__item-price-new">от 220 руб</div>
						</div>

						<div class="amenities__item-btn">
							<a href="#" class="btn btn-sm master-open open-master-js">
								<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
								Вызвать мастера
							</a>

							<a href="zamena-uplotnitelya-wood.php" class="btn btn-sm btn-border">Подробнее</a>
						</div>
					</div>
				</div>
				<div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/936ecef89a03225db4dc018385ca7ed6.jpg'); background-size: cover;">
					
					<div class="amenities__item-hover">
						<h3 class="amenities__item-title">
							<span class="aminites-title-block">Покраска и восстановление</span>
							<span class="amenities__item-subtitle">
															</span>
						</h3>
						

						<div class="amenities__item-price">
							<div class="amenities__item-price-old"></div>
							<div class="amenities__item-price-new">от 200 руб</div>
						</div>

						<div class="amenities__item-btn">
							<a href="#" class="btn btn-sm master-open open-master-js">
								<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
								Вызвать мастера
							</a>

							<a href="restavratsiya.php" class="btn btn-sm btn-border">Подробнее</a>
						</div>
					</div>
				</div>
				<div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed5.jpg'); background-size: cover;">
					
					<div class="amenities__item-hover">
						<h3 class="amenities__item-title">
							<span class="aminites-title-block">Утепление</span>
							<span class="amenities__item-subtitle">
															</span>
						</h3>
						

						<div class="amenities__item-price">
							<div class="amenities__item-price-old"></div>
							<div class="amenities__item-price-new">от 200 руб</div>
						</div>

						<div class="amenities__item-btn">
							<a href="#" class="btn btn-sm master-open open-master-js">
								<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
								Вызвать мастера
							</a>

							<a href="uteplenie.php" class="btn btn-sm btn-border">Подробнее</a>
						</div>
					</div>
				</div>
				<div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/zamena.jpg'); background-size: cover;">
					
					<div class="amenities__item-hover">
						<h3 class="amenities__item-title">
							<span class="aminites-title-block">Замена стеклопакета</span>
							<span class="amenities__item-subtitle">
															</span>
						</h3>
						

						<div class="amenities__item-price">
							<div class="amenities__item-price-old"></div>
							<div class="amenities__item-price-new">от 2.000 руб</div>
						</div>

						<div class="amenities__item-btn">
							<a href="#" class="btn btn-sm master-open open-master-js">
								<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
								Вызвать мастера
							</a>

							<a href="zamena-steklopaketa.php" class="btn btn-sm btn-border">Подробнее</a>
						</div>
					</div>
				</div>
				<span class="fix"></span>
				
			</div>
		</div>
        </div>


        <div class="category-wrap">
            <div class="title-wrap title">
                <h3 class="title fade_in">
                    <font-size="7">	Ремонт алюминиевых окон:		</font-size>	</h3>
            </div>
            <div class="amenities__content">
                <div class="amenities__block">
                    <!-- 1 -->
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/zamena-uplotnitelja-aljuminievye-okna-1.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена уплотнителя</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 250 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-uplotnitelej.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/aloknareg-800x800.png'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Регулировка окон</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 500 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="restavratsiya-al.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/ut40-m.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Утепление</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 150 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="uteplenie-al.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/zamena.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Поменять стеклопакет</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zamena-steklopaketa.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/okna_dvery1_0.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Алюминиевые двери</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 200 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="dveri.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed6.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Ремонт зимнего сада</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 150 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="sad-zimoy.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/giesse.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Giesse</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="giesse.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/geze_logo_vsezamki-520x630.png'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">GEZE</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="geze.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/savio-logo.gif'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">SAVIO</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="savio.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/shucko-logo-300x292.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Schuco</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"/></svg>
                                    Вызвать мастера
                                </a>

                                <a href="remont-okon-schuko.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <span class="fix"></span>

                </div>
            </div>
        </div>


        <div class="category-wrap">
            <div class="title-wrap title">
                <h3 class="title fade_in">
                    <font size="7">Аксессуары:</font></h3>
            </div>
            <div class="amenities__content">
                <div class="amenities__block">
                    <!-- 1 -->
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/389570177_w640_h640_zhalyuzi-alyuminevye-80140.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Жалюзи</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 220 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="zhalyuzi.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/1252890368_w640_h640_moskitnye-setki-v.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Москитные сетки</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 200 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="moskitnye-setki.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed7.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Откосы для окон</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 200 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="otkosy-dlya-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/pr_3149.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Стеклопакет с отверстием</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="steklopaket-s-otverstiem.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/ustroystvo-nozhnichnogo-mehanizma.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Электропривод для окон</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="elektroprivod.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/2896345.jpeg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Фрамужные приборы</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="framuzhnye-pribory.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/image.webp'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Тонировка окон</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="tonirovka-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed8.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Мебель для окон</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="mebel.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <span class="fix"></span>

                </div>
            </div>
        </div>


        <div class="category-wrap">
            <div class="title-wrap title">
                <h3 class="title fade_in">Москитные сетки:</h3>
            </div>
            <div class="amenities__content">
                <div class="amenities__block">
                    <!-- 1 -->
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/389570177_w640_h640_zhalyuzi-alyuminevye-80140.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Антикошка</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 220 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/antikoshka.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/1252890368_w640_h640_moskitnye-setki-v.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Антипыльца</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 200 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/antipyltsa.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed7.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Антипыль</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 200 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/antipyltsa.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/pr_3149.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Дверные</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/dvernye.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/ustroystvo-nozhnichnogo-mehanizma.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Рулонные</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/rulonnye.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/2896345.jpeg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Плиссе</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/plisse.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/image.webp'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Раздвижные</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/razdvizhnye.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed8.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Рамочные</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2.000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/ramochnye.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <span class="fix"></span>

                </div>
            </div>
        </div>


        <div class="category-wrap">
            <div class="title-wrap title">
                <h3 class="title fade_in">
                    Популярные услуги с ценами:</h3>
            </div>
            <div class="amenities__content">
                <div class="amenities__block">
                    <!-- 1 -->
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/389570177_w640_h640_zhalyuzi-alyuminevye-80140.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена уплотнителя</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 150 руб/м.п.</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/zamena-uplotnitelya.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/1252890368_w640_h640_moskitnye-setki-v.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Регулировка фурнитуры</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 300 руб/створка</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/regulirovka-plastikovykh-okon.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/unnamed7.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Устранение продувания</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 2000 руб</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/ustranenie-produvaniya.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/pr_3149.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Замена стеклопакета</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 3500 руб/м2</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/zamena-steklopaketa.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="amenities__item" style="background-image: url('<?=$pathToCatImg?>/ustroystvo-nozhnichnogo-mehanizma.jpg'); background-size: cover;">

                        <div class="amenities__item-hover">
                            <h3 class="amenities__item-title">
                                <span class="aminites-title-block">Монтаж подоконника</span>
                                <span class="amenities__item-subtitle">
                                                                </span>
                            </h3>


                            <div class="amenities__item-price">
                                <div class="amenities__item-price-old"></div>
                                <div class="amenities__item-price-new">от 1500 руб/м.п</div>
                            </div>

                            <div class="amenities__item-btn">
                                <a href="#" class="btn btn-sm master-open open-master-js">
                                    <svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#man-btn"></use></svg>
                                    Вызвать мастера
                                </a>

                                <a href="/ustanovka-podokonnikov.php" class="btn btn-sm btn-border">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <span class="fix"></span>

                </div>
            </div>
        </div>
	</div>
</div>

	<!-- Доверие -->
	<div class="trust section">
	<div class="container">
		<div class="title-wrap title-margin">
			<h2 class="section-title">
				НАМ ДОВЕРЯЮТ ЗА КАЧЕСТВО И ЧЕСТНУЮ СТОИМОСТЬ А ТАКЖЕ			</h2>
			<p class="section-desc">
				мы делаем так, чтобы больше не ломалось			</p>
		</div>

		<div class="trust__content">
			<div class="trust__advas">
				<div class="small-title trust__advas-title">
					Наш подход, благодаря которому к нам обращаются
				</div>

				<div class="trust__advas-block">
										<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-1.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Сочетание цены и качества. Льготные условия для пенсионеров и многодетных cемей						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-2.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Опыт работы говорит сам за себя - >10 лет						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-3.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Предоставляем расширенную гарантию						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-4.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Работаем честно,<br />
по договору						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-5.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Круглосуточно принимаем заявки						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-6.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Оперативный выезд мастера - в течение 30 мин						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-7.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							Выполняем нестандартные и сложные заказы						</p>
					</div>
					 					<!-- 4 -->
					<div class="trust__advas-item">
						<img src="wp-content/uploads/2019/05/trust-8.svg" alt="" class="trust__advas-img">
						<p class="small-text">
							101% качественные материалы и комплектующие						</p>
					</div>
					 									</div>
			</div>

			


			</div>
		</div>

		<div class="trust__pay">
			<div class="small-title trust__advas-title">
				Работаем с удобным для Вас способом оплаты
			</div>

			<div class="trust__pay-block">
								<div class="trust__pay-item">
					<img src="wp-content/uploads/2019/05/pay-1.svg" alt="" class="trust__pay-img">
					<span class="small-text">Наличный расчет</span>
				</div>

				 				<div class="trust__pay-item">
					<img src="wp-content/uploads/2019/05/pay-2.svg" alt="" class="trust__pay-img">
					<span class="small-text">Возможна рассрочка</span>
				</div>

				 				<div class="trust__pay-item">
					<img src="wp-content/uploads/2019/05/pay-3.svg" alt="" class="trust__pay-img">
					<span class="small-text">Оплата картой</span>
				</div>

				 				<div class="trust__pay-item">
					<img src="wp-content/uploads/2019/05/pay-4.svg" alt="" class="trust__pay-img">
					<span class="small-text">Безналичный расчет</span>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- Посмотрите на нас в процессе работы -->
	<div class="process section">
	<div class="container">
		<div class="title-wrap title-margin">
			<h2 class="section-title">
				ПОСМОТРИТЕ НА НАС В ПРОЦЕССЕ РАБОТЫ			</h2>
		</div>

		<div class="process__content">
			<div class="process__slider-block">
				<!-- <div class="link-wrap">
					<a href="nashi-raboty/index.php" class="link-trigger">Смотерь наши работы</a>
				</div> -->

				<div class="process__slider">
					<div class="frame" id="cycleitems">

						<div class="slider-ul" style="position: relative">
							<!-- 1 -->
														<a href="wp-content/uploads/2019/09/q5.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/09/34754854.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/09/q1.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/09/q111.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/09/q2.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/09/23634632.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/09/q4.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/09/32663334.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/10/1foto-v-processe-raboty-.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/10/foto-v-processe-raboty-.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/10/2.foto-v-processe-raboty-.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/10/2foto-v-processe-raboty-.jpg) no-repeat center; background-size: cover;">
							</a>
							 							<a href="wp-content/uploads/2019/10/3.foto-v-processe-raboty-.jpg" class="slider-ul-item fancy-class" data-fancybox="frame-gallery" style="background: url(wp-content/uploads/2019/10/3foto-v-processe-raboty-.jpg) no-repeat center; background-size: cover;">
							</a>
							 														<!-- 2 -->				
						</div>
					</div>
					<button class="backward arrows slidePrev"></button>
					<button class="forward arrows slideNext "></button>
				</div>
				<div class="scrollbar">
					<div class="handle">
						<div class="mousearea"></div>
					</div>
				</div>
			</div>	
			</div>
	</div>
	</div>
</div>



<link href="/bootstrap.min.css" rel="stylesheet">
<div class="process section" id="masters">
    <div class="container">
        <div class="title-wrap title-margin">
            <h2 class="section-title">НАШИ МАСТЕРА</h2>
        </div>

        <div class="row">
            <div class="col-sm-4 master">
                <div class="card">
                    <div class="img">
                        <img src="https://mosremokna.ru/wp-content/uploads/2019/05/master.jpg" alt="">
                    </div>
                    <br>
                    <br>
                    <h3>Алексей Волков</h3>
                    <span>Стаж: 6 лет</span>
                    <span>Специалист сервисной службы</span>
                </div>
            </div>
            <div class="col-sm-4 master">
                <div class="card">
                    <div class="img">
                        <img src="https://mosremokna.ru/wp-content/uploads/2019/05/master.jpg" alt="">
                    </div>
                    <br>
                    <br>
                    <h3>Алексей Волков</h3>
                    <span>Стаж: 6 лет</span>
                    <span>Специалист сервисной службы</span>
                </div>
            </div>
            <div class="col-sm-4 master">
                <div class="card">
                    <div class="img">
                        <img src="https://mosremokna.ru/wp-content/uploads/2019/05/master.jpg" alt="">
                    </div>
                    <br>
                    <br>
                    <h3>Алексей Волков</h3>
                    <span>Стаж: 6 лет</span>
                    <span>Специалист сервисной службы</span>
                </div>
            </div>
            <style>
                .master {
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    text-align: center;
                }
                .master .card {
                    padding: 20px;
                    margin-bottom: 30px;
                    border: 7px #f4cb67 solid;
                    border-radius: 24px;
                }
                .master .img {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    width: 100%;
                }
                .master .img img {
                    width: 50%;
                    border-radius: 500px;
                    border: 1px solid black;
                }
            </style>
        </div>
    </div>
</div>
</div>



    <div class="process section" id="otzivi">
        <div class="container">
            <div class="title-wrap title-margin">
                <h2 class="section-title">Отзывы довольных клиентов</h2>
            </div>

            <div class="row">
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Татьяна З.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Была проблема продувания окон. В течении 2х часов мастер приехал , осмотрел и устранил проблему за полчаса. Цена доступная, сервис на высоте.</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Юрий С.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>У меня случилась беда- расшаталась окoнная ствoрка. В квартире холoдно и неуютно...Друг пoсоветoвал обратиться к специалисту Смирнову Александру. Изначальнo, мне сложно было поверить, что за такой прайс можно быстрo и качественно сделать. Моя благoдарность.</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Денис П.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Недавно обращался к Александру по поводу того, что моя балкoнная дверь стала некoрректно работать. Я думаю, этo случилась из-за тогo, что ей уже много лет и боялся, что инструментов и материалoв на нее нигде не найти. Но у этогo мастера есть навыки и реквизит на любую ситуацию. Респект.</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Катя С.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Очень довольна работой компании. Из окон продувало, много щелей было. Мастер пришел опрятный, вежливый. В течении часа отремонтировал окно, теперь ни откуда не дует. Спасибо за работу.</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Игорь Г.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Окна от застройщика были поворотные, Александр переделал на поворотно - откидные. Устранил продувание и поставил детские замки. Сделал всё быстро. Большое спасибо!</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Ольга Л.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Александр быстро откликнулся и приехал. Плохо закрывалось окно и сильно хрустело. Александр поменял запчасть и отрегулировал окно. Выражаю огромную благодарность за качественную и быструю работу!</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Никита Иванов</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Окна свистели, проблему устранил быстро и качественно. Спасибо. Спим в тишине.</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Надежда С.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Не закрывалось окно, Александр быстро приехал и исправил. Ещё отрегулировал и смазал дверь. Большое спасибо!</span>
                </div>
                <div class="col-sm-4 kommenti">
                    <div class="s-title">Татьяна И.</div>
                    <div class="stars"><img src="/images/stars.png"></div>
                    <span>Сильно продувала балконная дверь. Окна и дверь от застройщика. Обратилась к Александру. Мне повезло, мастер был в моем районе, приехал в течении часа. Быстро и качественно выполнил ремонт дверей, подтянул окно которое продувало. Заплатила как и договаривались. Спасибо!</span>
                </div>

                <style>
                    .fade_in {
                        padding-left: 10px;
                    }
                    .kommenti {
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        text-align: center;
                        min-height: 300px;
                    }
                    .kommenti .card {

                        padding: 0;
                    }
                    .kommenti .stars {
                        display: flex;
                        justify-content: center;
                        width: 100%;
                    }
                    .kommenti .stars img {
                        width: 30%;
                    }
                    .s-title {
                        font-size: 25px;
                    }
                    .kommenti span {
                        color: black;
                        font-weight: bold;
                        text-decoration: none;
                    }
                </style>
            </div>
        </div>
    </div>
    </div>


    <div class="process section" style="padding-top: 0">
        <div class="container" style="padding-top: 0">
            <div class="title-wrap title-margin">
                <h2 class="section-title">ОТЗЫВЫ НА ДРУГИХ ПЛАТФОРМАХ</h2>
            </div>

            <div class="row">
                <div class="col-sm-3 otzivi">
                    <a href="#">
                        <div class="card">
                            <img src="https://mosremokna.ru/wp-content/themes/mosremokna.ru/assets/img/otzovik.png">
                            <span>Отзывы на Отзовик</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 otzivi">
                    <a href="#">
                        <div class="card">
                            <img src="https://mosremokna.ru/wp-content/themes/mosremokna.ru/assets/img/yandex.png">
                            <span>Отзывы на Яндексе</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 otzivi">
                    <a href="#">
                        <div class="card">
                            <img src="https://mosremokna.ru/wp-content/themes/mosremokna.ru/assets/img/google.png">
                            <span>Отзывы на Google</span>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 otzivi">
                    <a href="#">
                        <div class="card">
                            <img src="https://mosremokna.ru/wp-content/themes/mosremokna.ru/assets/img/vkontakte.png">
                            <span>Отзывы в ВКонтакте</span>
                        </div>
                    </a>
                </div>

                <style>
                    .otzivi {
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        text-align: center;
                    }
                    .otzivi .card {
                        padding: 0;
                        margin-bottom: 30px;
                    }
                    @media (max-width: 768px) {
                        .otzivi  img {
                            height: unset !important;
                        }
                    }
                    .card {
                        border: 1px #bbb solid;
                        border-radius: 5px;
                    }
                    .otzivi span {
                        color: black;
                        font-weight: bold;
                        text-decoration: none;
                    }
                    .otzivi  img {
                        width: 100%;
                        height: 169px;
                    }
                    .otzivi  img::before {
                        width: 100%;
                        height: 100%;
                        content: " ";
                    }
                </style>
            </div>
        </div>
    </div>
    </div>
	<!-- Клиенты -->

	

<div class="clients section blue">
	<div class="container">
		<div class="title-wrap title-margin">
			<h2 class="section-title">
				РАБОТАЕМ И С КОРПОРАТИВНЫМИ КЛИЕНТАМИ			</h2>
		</div>

		<div class="clients__content">
			<div class="clients__block">
								<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/gas-logo.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/lkogo-burger.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-333.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-353.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-4244.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-14444.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-alfa.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-ntv.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo-t.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo11.png" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/logo122.jpg" alt="">
				</a>
				 				<a href="#" class="clients__item">
					<img src="wp-content/uploads/2019/09/loho-33.jpg" alt="">
				</a>
				 				
				<!-- <a href="korporativnym-klientam.php" class="clients__item clients-corp">
					<span>
						Корпоративным
						клиентам
					</span>
				</a> -->

			</div>
		</div>
	</div>
</div>


	<!-- Акции -->

<!-- 	<div class="gift section">
	<div class="container">
		<div class="title-wrap title-margin">
			<h2 class="section-title">
				ПРИМИТЕ УЧАСТИЕ В НАШИХ АКЦИЯХ <br />
И ПОЛУЧИТЕ ПОДАРКИ			</h2>
		</div>

		<div class="gift__content">
			<div class="gift__item"style="background: url(wp-content/themes/src/assets/img/home/gift/item-1-bg.jpg) no-repeat center; background-size: cover;">
				<div class="gift__item-info">
					<div class="gift__item-title">Набор для ухода за пластиковыми окнами в подарок!</div>
					<div class="gift__item-subtitle">Получите набор для ухода за пластиковыми окнами абсолютно бесплатно</div>
					<div class="gift__item-btn-wrap">
						<a href="#" class="btn btn-sm gifts-open">
							<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#hand-svg"/></svg>
							<span>ПОЛУЧИТЬ БЕСПЛАТНО</span>
						</a>
					</div>
				</div>

				<div class="gift__item-img gift__item-img-1" id="prx-gift-1">
					<img src="wp-content/themes/src/assets/img/home/gift/nabor.png" data-depth='1' alt="">
				</div>
			</div>

			<div class="gift__item"style="background: url(wp-content/themes/src/assets/img/home/gift/item-2-bg.jpg) no-repeat center; background-size: cover;">
				<div class="gift__item-info">
					<div class="gift__item-title">Сумка пляжная с личным логотипом компании в подарок!</div>
					<div class="gift__item-subtitle">Получите  сумку пляжную с внешним карманом для бутылки, с личным логотипом компании , абсолютно бесплатно</div>
					<div class="gift__item-btn-wrap">
						<a href="#" class="btn btn-sm gifts-open">
							<svg class="img-btn" viewBox="0 0 26 26"><use xlink:href="#hand-svg"/></svg>
							<span>ПОЛУЧИТЬ БЕСПЛАТНО</span>
						</a>
					</div>
				</div>

				<div class="gift__item-img" id="prx-gift-2">
					<img src="wp-content/themes/src/assets/img/home/gift/paket.png" data-depth='1.3' alt="">
				</div>
			</div>
		</div>

		<div class="gift__btn">
			<a href="akcii-i-skidki/index.php" class="btn btn-border btn-sm">Смотреть все акции</a>
		</div>
	</div>
</div> -->
	<!-- порядок работ -->
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>