
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?><div class="main" style="background: url(https://kislorod-ok.ru/wp-content/uploads/2019/09/bg-teplo-okon.jpg) no-repeat center; background-size: cover;">
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                        <div class="icon-wrapper">
                            <a href="https://api.whatsapp.com/send?phone=79253512022">
                                <img src="/images/what-icon.png" class="icon">
                            </a>

                            <a href="viber://add?number=79253512022">
                                <img src="/images/viber-icon.png" class="icon">
                            </a>
                        </div>
                        <style>
                            .icon-wrapper {
                                display: flex;
                                justify-content: center;
                                align-items: center;
                            }
                            .icon-wrapper img {
                                margin: 5px 5px;
                                width: 32px !important;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Установка подоконников, подоконной доски</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div id="right">
            <div id="component">
                <div class="jshop" id="comjshop">
                    <h1>Установка подоконников, подоконной доски</h1>
                    <div class="category_description">
                        <p>Сервисная служба «Помощь окнам» предоставляет своим клиентам профессиональные услуги по установке и замене пластиковых подоконников (подоконных досок из ПВХ, дерева). <br> В нашей работе мы используем только качественные, проверенные временем, изделия от известных зарубежных и отечественных производителей.<br> У нас можно заказать установку подоконников известных брендов, самые востребованные:</p>
                        <ul>
                            <li><strong>Vitrage Plast </strong>– изделие российского производства с декоративно-защитим покрытием;</li>
                            <li><strong>Werzalit</strong> – немецкие ламинированные подоконники из влагостойкого материала;</li>
                            <li><strong>Moeller</strong>– изделие, изготовленное по современной немецкой технологии на основе смеси древо-пластика.</li>
                        </ul>  </div>

                    <div class="jshop_list_category">
                    </div>


                    <div class="jshop_list_product"><div class="jshop list_product" id="comjshop_list_product">
                            <div class="row-fluid">

                                <div class="sblock4">
                                    <div class="block_product">
                                        <div class="product productitem_50">


                                            <div class="image">
                                                <div class="image_block">

                                                        <img class="jshop_img" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/components/com_jshopping/files/img_products/thumb_vitrage-plast.jpg" alt="Пластиковые подоконники Vitrage Plast" title="Пластиковые подоконники Vitrage Plast">

                                                </div>


                                            </div>


                                            <div class="name">

                                                    Пластиковые подоконники Vitrage Plast
                                            </div>



                                            <div class="oiproduct">





                                                <div class="jshop_price">
                                                    <span>1500.00 RUB</span>
                                                </div>


                                                <div class="price_extra_info">

                                                </div>






                                                <div class="extra_fields">
                                                    <div>
                                                        <span class="label-name">Эксплуатация:</span>
                                                        <span class="data">до 50 лет</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Декор:</span>
                                                        <span class="data">DisCovery, VPL, Palitra, Crystallit, ПВХ и ПВХ с 3D ламинацией</span>
                                                    </div>
                                                </div>



                                                <div class="description">
                                                    Материал не содержит свинца (технология BioConcept), имеет высокую стойкость к УФ-лучам и моющим средствам        </div>


                                                <div class="buttons">

                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="sblock4">
                                    <div class="block_product">
                                        <div class="product productitem_51">


                                            <div class="image">
                                                <div class="image_block">

                                                        <img class="jshop_img" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/components/com_jshopping/files/img_products/thumb_moeller.jpg" alt="Подоконники пластиковые Moeller" title="Подоконники пластиковые Moeller">

                                                </div>


                                            </div>


                                            <div class="name">

                                                    Подоконники пластиковые Moeller
                                            </div>



                                            <div class="oiproduct">





                                                <div class="jshop_price">
                                                    <span>3400.00 RUB</span>
                                                </div>


                                                <div class="price_extra_info">

                                                </div>






                                                <div class="extra_fields">
                                                    <div>
                                                        <span class="label-name">Ширина, мм:</span>
                                                        <span class="data">150 - 800</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Эксплуатация:</span>
                                                        <span class="data">до 40 лет</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Декор:</span>
                                                        <span class="data">однотонные, под дерево, под камень</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Покрытие:</span>
                                                        <span class="data">Elesgo-Plus on top (ламинат) матовый/глянцевый</span>
                                                    </div>
                                                </div>



                                                <div class="description">
                                                    Стоимость указана за метр погонный, подоконник шириной 800 мм.        </div>


                                                <div class="buttons">


                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="sblock4">
                                    <div class="block_product">
                                        <div class="product productitem_52">


                                            <div class="image">
                                                <div class="image_block">
                                                        <img class="jshop_img" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/components/com_jshopping/files/img_products/thumb_werzalit-1.jpg" alt="Подоконник Werzalit" title="Подоконник Werzalit">

                                                </div>


                                            </div>


                                            <div class="name">
                                                    Подоконник Werzalit
                                            </div>



                                            <div class="oiproduct">





                                                <div class="jshop_price">
                                                    <span>3800.00 RUB</span>
                                                </div>


                                                <div class="price_extra_info">

                                                </div>






                                                <div class="extra_fields">
                                                    <div>
                                                        <span class="label-name">Материал:</span>
                                                        <span class="data">из отходов древесины, экологически чистый</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Ширина, мм:</span>
                                                        <span class="data">200, 250, 300, 350, 400, 450, 500, 550, 600 мм.</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Эксплуатация:</span>
                                                        <span class="data">25 лет</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Декор:</span>
                                                        <span class="data">под дерево, под камень, однотонные</span>
                                                    </div>
                                                    <div>
                                                        <span class="label-name">Покрытие:</span>
                                                        <span class="data">влагостойкое, многослойное до 6-ти слоев</span>
                                                    </div>
                                                </div>



                                                <div class="description">
                                                    Стоимость указана за метр погонный, подоконник шириной 600 мм.        </div>


                                                <div class="buttons">

                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </div></div>

                </div><span id="mxcpr">Copyright MAXXmarketing GmbH<br><a rel="nofollow" target="_blank" href="https://www.joomshopping.com/">JoomShopping Download &amp; Support</a></span>
            </div>
            <div class="right_bottom">
                <div class="moduletable">


                    <div class="custom">
                        <h2>Как происходит монтаж подоконников</h2>
                        <p>Высококвалифицированные сотрудники компании «Помощь окнам» производят оперативную и качественную установку ПВХ подоконников. Монтаж производится тремя основными способами:</p>
                        <ul>
                            <li>на клей;</li>
                            <li>на кронштейны;</li>
                            <li>на монтажную пену.</li>
                        </ul>
                        <p>При выборе способа установки изделия наши специалисты учитывают размеры подоконника, материал изготовления и условия его эксплуатации. Для каждого вида крепления подбираются подходящие строительные материалы, эксплуатация которых осуществляется в регламентированных производителем условия.</p>
                        <p><img src="<?=$pathToImg?>/777-9.jpg" border="0"></p>
                        <p><img src="<?=$pathToImg?>/777-8.jpg" border="0"></p></div>
                </div>
                <div class="moduletable">


                    <div class="custom">
                        <div class="min_price"><strong>Внимание!</strong> В случае не заключения договора на объекте, <b>выезд специалиста 800 рублей</b> по Москве. Цена минимального заказа компании "Помощь окнам" <b>составляет 2000 рублей</b>. Например, при заказе регулировки 3 створок стоимостью 1500 р., будет необходимо дозаказать услуги на сумму не менее 500 руб.</div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>