
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена фурнитуры на пластиковых окнах и ремонт				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>







	
	<div class="container">
        <div itemprop="articleBody">
            <p>Замена фурнитуры на пластиковых окнах, одна из частых задач наших инженеров.</p>
            <p>Заменяем всю комплектацию, когда отремонтировать нет возможности. Специализированный оконный сервис, чёткий, программный подбор деталей именно к вашему окну. </p>
            <p>Замер, доставка, установка. Работаем в Москве, Новой Москве и всей области. Гарантия 365 дней, на материал 10 лет.</p>
            <p class="comment">Профессиональный ремонт подвижных частей и механизмов стеклопакетов – <b>Россия, Германия, Турция, Китай</b>, в наличие и на заказ от 2х дней. Замена оконной фурнитуры любых брендов. Также возможна регулировка и частичное замещение поломанных деталей.</p>


            <h2>Телефоны для связи:</h2>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloakdc45ec092cde45d793a77361c67da5a0"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloakdc45ec092cde45d793a77361c67da5a0').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addydc45ec092cde45d793a77361c67da5a0 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addydc45ec092cde45d793a77361c67da5a0 = addydc45ec092cde45d793a77361c67da5a0 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_textdc45ec092cde45d793a77361c67da5a0 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakdc45ec092cde45d793a77361c67da5a0').innerHTML += '<a ' + path + '\'' + prefix + ':' + addydc45ec092cde45d793a77361c67da5a0 + '\'>'+addy_textdc45ec092cde45d793a77361c67da5a0+'<\/a>';
                    </script></p>
            </div>



            <p><a name="napr"></a></p>
            <h2>Замена фурнитуры на пластиковых окнах цена:</h2>
            <p>Ниже представлен примерный прайс-лист на наши услуги. Обратите внимание - указана ориентировочная стоимость без учета особенностей объекта.</p>
            <p>Точную стоимость определит специалист замерщик, состоит она из выбранных вами функций, названия бренда и расходами на приобретение составных частей. Не является публичной офертой.</p>
            <table class="table" border="0" cellspacing="0" cellpadding="0">

                <tbody>
                <tr class="head">
                    <td colspan="3">Замена фурнитуры пластиковых окон </td>
                </tr>
                <tr class="grei">
                    <td style="width: 25px;">№</td>
                    <td style="width: 706px;">Работы и материалы</td>
                    <td style="width: 76px;">Цена, компл.</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Установка фурнитуры на поворотную створку</td>
                    <td>от 5000</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Поворотно откидная фурнитура для пластикового окна </td>
                    <td>от 6000</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Противовзломная фурнитура для пластиковых окон </td>
                    <td>от 7000</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Замена петель на пластиковых окнах</td>
                    <td>от 2500</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Установка петли оконной, регулируемой</td>
                    <td>от 3000</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Монтаж ответной планки </td>
                    <td>500</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Замена замка на пластиковом окне (основного запора)</td>
                    <td>от 4000</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Микропроветривание (угловая передача)</td>
                    <td> от 2500</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Замена ножниц на пластиковом окне</td>
                    <td> от 3500</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Декоративные накладки на окна пластиковые</td>
                    <td> от 350</td>
                </tr>
                </tbody>
            </table>


            <p>Подробней о замене фурнитуры и ремонте окон можно узнать <a href="/remont-plastikovykh-okon.html" title="Ремонт пластиковых окон" target="blank"><b>тут</b>.</a></p>
            <p>Для восстановления работоспособности оконных механизмов и подвижных деталей могут потребоваться следующие дополнительные услуги:</p>
            <ul>
                <li>Полная переустановка петель (замена верхней петли, нижней) балконных и оконных ручек, раздвижных кареток Патио и откатных механизмов, ответных частей, систем микропроветривания, защёлок, приточных клапанов, гребёнок, шпингалетов и декоративных петельных накладок.</li>
                <li>Заменяем вышедшие из строя детали: эксцентриков, элементов запорных устройств, цапф, пружин и рычажных тяг, дверных доводчиков и прочего.</li>
                <img class="img_right desctop" title="1 год гарантии на регулировку окон" src="/files/imges/god.png" alt="1 год гарантии на регулировку окон" border="0">
                <li>Регулировка, смазка, профилактика, обслуживание и настройка оборудования.</li>
            </ul>
            <p>В независимости от того, какая заказана (полная, или частичная) замена фурнитуры, цена услуги будет включать годовую гарантию на все работы и использованные материалы.</p>
            <p class="comment">Вы можете получить бесплатную консультацию и задать любой интересующий вопрос по телефону <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a> Наши специалисты решат какую угодно задачу.</p>


            <p>Часто производя диагностику поломки, мастера решают вопрос простым ремонтом фурнитуры, это даёт возможность не менять механизм полностью, такие работы выполняются при первом выезде, это экономит ваши финансы и время.</p>


            <h2>Ремонт фурнитуры пластиковых окон цены:</h2>
            <table class="table table-5col" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="col1">Фото</td>
                    <td class="col2">Вид работ</td>
                    <td class="col3">Примечание</td>
                    <td class="col5">Цена, руб.</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/686e712f2e85de88f8e4035893884c10.jpg" alt=""/></td>
                    <td>Ремонт основного запора</td>
                    <td>Чистка, смазка основного запора, проверка подшипника</td>
                    <td>2500/3500</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/fcfb8f357adde8bfaf2b6005be4bd495.jpg" alt=""/></td>
                    <td>Устранение дефектов откидных ножниц</td>
                    <td>Снятие створки, выравнивание ножницы, чистка смазка</td>
                    <td>от 2500</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/d9552ddb883ad1d4c0a57b7f82ec606c.jpg" alt=""/></td>
                    <td>Капремонт оконной петли</td>
                    <td>Капремонт, укрепление, заменим саморезы, смена штифта</td>

                    <td>от 2500</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/78fb9a325e63c421b85572ab1e757dcb.jpg" alt=""/></td>
                    <td>Устраним повреждения частей дистанционного открывания </td>
                    <td>Регулируем скользящую штангу, фиксация винтов крепления, смазка угловых передач </td>
                    <td>от 3000</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/70ac395f8afe08f3ca768c78fde711b8.jpg" alt=""/></td>
                    <td>Ответная планка</td>
                    <td>Поменяем старые, зафиксируем по шаблону на раме, относительно цапф </td>
                    <td>от 500</td>
                </tr>
                </tbody>
            </table>




            <h2>Изменение фурнитуры окон</h2>
            <p>Замена фурнитуры на откидывающуюся, глухого окна на открывающуюся створку, поворотную и т. п.  Ведущие производители оконного оборудования предлагают готовые решения, позволяющие вносить в установленный стеклопакет необходимые конструктивные изменения. Наша компания предлагает клиентам фурнитурную продукцию следующих брендов:</p>
            <ul>
                <li>Австрийское предприятие «Мако» - надёжные поворотно-откидные и сдвижные системы, блокировка ошибочного открывания, противовзломные опции, скрытые петли и детские замки.</li>
                <li>Немецкая фирма «Рото» - параллельно складывающиеся и раздвижные системы, механизмы, рассчитанные на повышенную нагрузку, высокое качество декоративных элементов.</li>
                <li>Турецкая компания «Ворне» - простая и точная оконная система.</li>
                <li>Предприятие «Кале» из Турции – долговечность и износостойкость металлических деталей замков и поворотных блоков благодаря трёхслойному цинковому покрытию.</li>
            </ul>



        </div>
</div>

</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>