
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Антикошка				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Москитная сетка Антикошка			</h1>
            </div>





            <div itemprop="articleBody">
                <p>Предлагаем купить москитные сетки Антикошка с установкой на окна пластиковые, деревянные, алюминиевые окна в Москве, Новой Москве и всей области.</p>
                <p><img src="<?=$pathToImg?>/antikoshka.jpg" alt="Москитная сетка Антикошка" style="border:none; padding:0;"></p>
                <p>Прочное полиэстеровое полотно, <b>проклеенное в цеху, на производстве супер клеем, </b>черного или серого цвета и металлические кронштейны, надежно убережет ваших домашних питомцев от выпадения из окна.</p>
                <p><a class="naprice" href="#napr">Узнать цены</a></p>
                <p>Мы предлагаем нашим клиентам услуги:</p>
                <ul>
                    <li>изготовление, сборка москитной сетки с полотном Антикошка (Pet Screen), установка;</li>
                    <li>замене сеточного полотна (порванного, стандартного «фибергасс») на «антикошка»;</li>
                    <li>ремонт москитной сетки.</li>
                </ul>
                <ul class="ul-inline ul-utepalum">
                    <li><div id="sigplus_1001" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/366c923b6d548e86106cb10f8d056007.jpg" title="Черное полотно Антикошка (1/1)" data-summary="Черное полотно Антикошка (1/1)" data-fancybox="sigplus_1001"><img class="sigplus-preview" src="<?=$pathToImg?>/366c923b6d548e86106cb10f8d056007.jpg" width="204" height="190" alt="Черное полотно Антикошка" sizes="204px"></a></li></ul></div><br>Полотно Антикошка черное</li>
                    <li><div id="sigplus_1002" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/91a2bcfd6386935b014b1ae00ebf45a5.jpg" title="Серое полотно Антикошка (1/1)" data-summary="Серое полотно Антикошка (1/1)" data-fancybox="sigplus_1002"><img class="sigplus-preview" src="<?=$pathToImg?>/91a2bcfd6386935b014b1ae00ebf45a5.jpg" width="204" height="190" alt="Серое полотно Антикошка" sizes="204px"></a></li></ul></div><br>Полотно Антикошка серое</li>
                    <li><div id="sigplus_1003" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="<?=$pathToImg?>/c00f40f68d2b7a2d8e291f4a75d3ce0c.jpg" title="Белое полотно Антикошка (1/1)" data-summary="Белое полотно Антикошка (1/1)" data-fancybox="sigplus_1003"><img class="sigplus-preview" src="<?=$pathToImg?>/c00f40f68d2b7a2d8e291f4a75d3ce0c.jpg" width="204" height="190" alt="Белое полотно Антикошка" sizes="204px"></a></li></ul></div><br>Полотно Антикошка белое</li>
                </ul>
                <p class="comment">Время изготовления 2-3 и монтажа – 1 рабочий день! Мастер на замер выезжает <strong>Бесплатно*, но с учётом заключения договора</strong>. График приёма заявок <b>с 8 до 22<sup>00</sup>без выходных.</b></p>
                <h2>Особенности полотна</h2>
                <p><img class="img_left" title="Антикошка - москитная сетка" src="<?=$pathToImg?>/anticat.jpg" alt="Антикошка - москитная сетка"></p>
                <p>Сеточное полотно "антикошка" состоит из прочно переплетенных полиэфирных нитей сечением 0,60 мм. в ПВХ оболочке. Это предохраняет сетку от повреждений когтями, клювами, зубами и прочими принадлежностями домашних обитателей. Ткань не провиснет и не деформируется при попытке животного вскарабкаться по нему, или прыжка через окно.</p>
                <p>Москитные сеточные полотна антикошка устанавливаются только в рамные конструкции и раздвижные. Другие виды, роллетные-рулонные сетки, не способны выдержать нагрузки повадок питомцев. Рамочные москитки с полотном антикошка обязательно устанавливаются на плунжерные крепления, или «барашки», часто с импостом, что придает дополнительную надежность конструкции.</p>
                <p><a class="blue-button bolt" title="Разновидности креплений сеток" href="/moskitnye-setki/krepezh.html#plynjer" target="blank">Плунжерные крепления сеток Антикот</a></p>
                <p><a href="https://vk.com/remont_okn" target="blank"><img src="<?=$pathToImg?>/01.jpg" alt=""></a></p>
                <p class="comment">Получите скидку до 10% на москитные сетки с установкой, став участником нашей <a class="vk_in_text" href="https://vk.com/remont_okn" target="blank">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 5 шт </b>.</p>
                <p><a name="napr"></a></p>
                <h2>Цены на москитные сетки с полотном Антикошка</h2>
                <p>Купить рамочные сетки Антикошка можно по таким ценам:</p>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup> сетки при самовывозе</td>
                        <td>Цена м<sup>2</sup> с замером и установкой в пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.<a name="akciya"></a></td>
                        <td>2000&nbsp;руб.</td>
                        <td>3100 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.*</td>
                        <td>4000&nbsp;руб.*</td>
                        <td>5700&nbsp;руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.*</td>
                        <td>6000&nbsp;руб.*</td>
                        <td>8300&nbsp;руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6*</td>
                        <td colspan="2"><span class="color-red">Цена договорная</span></td>
                    </tr>
                    </tbody>
                </table>
                <p>*Акционная стоимость изделий, распространяется на количество более 1 шт.</p>
                <h3>Стоимость работ по замеру, установке и доставке</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Вид работ</td>
                        <td>Цена</td>
                    </tr>
                    <tr>
                        <td>Замер в Москве</td>
                        <td><span class="color-red">бесплатно</span> (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км. от МКАД
                            до 10 км. считаем Москвой</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500&nbsp;руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Установка на плунжерное крепление + фетр/щётка на клеевой основе, скрывает зазоры и неровности проёмов</td>
                        <td>+700 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>
                <h3>Наценка на москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Цвет профиля</td>
                        <td>Наценка</td>
                    </tr>
                    <tr>
                        <td>Белый/коричневый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>"Антикошка" (Pet Screen) (Phifer, США) цвет светло - серый</td>
                        <td>+100 руб./м.кв.</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 40% к цене</td>
                    </tr>
                    <tr>
                        <td>Сетка на дверь Антикошка</td>
                        <td>7000 руб.м<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td>Сетка антикошка на раздвижные окна балкона</td>
                        <td>4600 руб.м<sup>2</sup></td>
                    </tr>
                    </tbody>
                </table>

                <div id="otz" class="otz_in">
                    <h2>Отзывы клиентов</h2>
                    <div class="otz2">
                        <div class="otz">
                            <p class="desc">Кот уже пару раз убегал, когда оставляли открытым окно (этаж первый, не разбивается, но сбегает в соседний двор). Установили серое полотно антикошка — и все, прогулки закончились. Он конечно пару раз схлопотал по ушам при попытке приложить когти, но когда его оставили дома и забыли закрыть окно.. ничего не случилось. Он не смог порвать полотно! Отлично, я в восторге! Советую всем кошатникам, это действительно работает.</p>
                            <p class="date"><span id="today1">4 февраля</span>, Марина</p>
                        </div>
                        <div class="otz">
                            <p class="desc">Как только стало тепло, в моей кухне ночью начал по ночам кто-то шарить. Оказалось соседский кот по балконам добирается до моего окна и залезает в дом. Напугал меня, клубок пушистый, поронял посуду, а хомяк дочери по-моему до сих пор в шоке от такого "соседа". Обратилась в "Помощь", мне посоветовали просто сменить полотно на "антикошку". И чудо, пушистый бандит теперь только на балконе тусуется, внутрь никак. Спасибо!</p>
                            <p class="date"><span id="today2">22 января</span>, Алина</p>
                        </div>
                    </div>
                    <br> </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>