
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_wood.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Утепление деревянных окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <p><img src="<?=$pathToImg?>/4.jpg" alt="Утепление окон из дерева" class="radius_img"></p>

            <p>Утепление деревянных окон евростандарта – один из видов широкого перечня услуг сервисной службы «Помощь окнам». Команда наших опытных мастеров на профессиональном уровне осуществляет ряд мероприятий по увеличению теплоизоляционных свойств евроокон финской, немецкой и норвежской технологии производства.</p>

            <p class="comment">Вы можете получить бесплатную консультацию или задать любой интересующий вопрос по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a>. Наши специалисты решат любую вашу проблему.</p>


            <h2>Комплексное утепление деревянных евроокон</h2>
            <img src="<?=$pathToImg?>/3w2.jpg" alt="Утепление деревянных окон" class="img_right">
            <p>Перечень работ включает в себя следующие услуги:</p>
            <ul>
                <li>выстраивание геометрии створок;</li>
                <li>герметизация откосов;</li>
                <li>установка высокоэластичного трубчатого уплотнителя;</li>
                <li>устройство дополнительного контура уплотнения;</li>
                <li>регулирование степени прижатия створки к оконной коробке;</li>
                <li>отладка и замена фурнитуры;</li>
                <li>герметизация швов герметиками или монтажной пеной;</li>
                <li>замена однокамерного стеклопакета на двух- и трехкамерный.</li>
            </ul>

            <h2>Порядок проведения теплоизоляционных работ</h2>

            <p>Процесс утепления евроокон состоит из нескольких основных этапов:</p>
            <ol>
                <li>Демонтаж створок.</li>
                <li>Снятие изношенного уплотнителя и подгонка геометрии створок.</li>
                <li>Монтаж нового контурного и межрамного резинового уплотнителя.</li>
                <li>Замена и регулировка оконной фурнитуры – петель, замков, ручек.</li>
                <li>Установка створок, проверка плотности их прижима к коробке.</li>
            </ol>
            <p><img src="<?=$pathToImg?>/5.jpg" alt="Утепление деревянных евро окон" class="radius_img"></p>
            <p class="comment">На все производимые работы распространяется бесплатная гарантия — 1 год.</p>




        </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>