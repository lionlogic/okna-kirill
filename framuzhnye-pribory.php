
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Дистанционное открывание окон, ручное 				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">

        <img class="img_right" src="<?=$pathToImg?>/framuga.jpg" alt="Фрамужные механизмы для открывания окон">
        <p>Фирма устанавливает устройства дистанционного открывания окон, фрамуг, как в служебных и сервисных зданиях, так и в единицах жилого фонда Москвы и всего Подмосковья.</p>
        <p>Выполняем все виды установки, ремонта, регулировки замены частей, любых брендов, механизмов фрамужного открывания, на пластиковые окна (ПВХ), также деревянные и алюминиевые:</p>
        <ul>
            <li>Geze;</li>
            <li> Hautau;</li>
            <li>Elementis; </li>
            <li>G-U ventus; </li>
            <li>Stublina.</li>
        </ul>

        <img src="<?=$pathToImg?>/9faa09b94377d85866d904b41bf6c8e0.jpg" alt=""/>
        <img src="<?=$pathToImg?>/ad8e88142ed5d7950caf39479e9d1db5.jpg" alt=""/>
        <img src="<?=$pathToImg?>/d82cc37163c5ea14ee2cdb4658eb39c0.jpg" alt=""/>

        <p>Мечта любого занятого человека – не отрываться от работы, чтобы открыть или закрыть окно. Во время заседания отказал кондиционер, а влезать на стул, чтобы распахнуть створки, как-то неудобно. Решаема ли эта проблема?</p>
        <p>Однозначно утверждаем: да, решаема. Дистанционное открывание окон, или приводы для фрамуг перестали быть чем-то экзотическим.</p>
        <p class="comment">Получить бесплатную консультацию, узнать цену, или вызвать специалиста на дом вы можете по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a> или с помощью <a href="#" class="zakaz">формы обратной связи</a>.</p>
        <p>Следует отметить, что помимо удобства пользования, плавное, лишенное рывков открывание и закрывание окон, фрамуг, створок сводит к минимуму возможность поломки, обеспечивает примерно одинаковую нагрузку на все зоны данной единицы – и потому позволяет продлить срок пользования ею без ремонта.</p>



        <h2>Фрамужные приборы - цена</h2>
        <p>Стоимость включает в себя выезд нашего мастера, комплект прибора и его установку на ваше окно.</p>
        <table class="table table-text-center" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td>Услуга</td>
                <td>Примечание</td>
                <td>Цена за комплект</td>
            </tr>
            <tr>
                <td>Комплект на жесткой&nbsp;тяге</td>
                <td>Скидка при заказе более 3 штук.</td>
                <td>9900 руб.</td>
            </tr>
            <tr>
                <td>Комплект на гибкой тяге</td>
                <td>Цена зависит от конфигурации окна.</td>
                <td>от 13500&nbsp;руб.</td>
            </tr>
            <tr>
                <td>Ремонт и смазка</td>
                <td>Замена управляющей ручки, углового переходника, регулировка открывания</td>
                <td>3900 руб.</td>
            </tr>
            </tbody>
        </table>




        <h2>Виды устройств фрамужных открывателей</h2>
        <p>В зависимости от конкретных условий и возможностей, фрамужные приборы могут быть механическими (ручным) и с электронным управлением:</p>
        <img class="img_right" src="<?=$pathToImg?>>/framuga_r.jpg" alt="Автоматическое открывание окон">
        <h3>Механические (ручные) устройства</h3>
        <p>Ручное дистанционное открывание окон, называемое также рычажным, - самый простой вариант, не требующий больших затрат, быстро монтируемый.</p>
        <p>Поворот-вращение створки вокруг вертикальной оси (верхний подвес, открывание внутрь) или вокруг горизонтальной оси (нижний подвес, открывание наружу) позволяют либо открывать-закрывать створку, либо частично или полностью открывать фрамугу. Эти конструкции не громоздки, выглядят эстетично, компактны и не меняют внешнего вида окон.</p>
        <img src="<?=$pathToImg?>/6817f9de1e4f667e2394d5abcf4e4057.jpg" alt=""/>
        <img src="<?=$pathToImg?>/fd7641b1145c7116f91d4831fce9e0ef.jpg" alt=""/>
        <img src="<?=$pathToImg?>/756ef77c1f6ba0123fc2961f4e3c0c54.jpg" alt=""/>
        <img src="<?=$pathToImg?>/1c6cb8fdac072a22ea143b24d428e408.jpg" alt=""/>
        <h3>Электроприводы для окон</h3>
        <p>Автоматическое открывание может быть цепное, штоковое или реечное. Управление возможно с кнопки на стене, или переносного пульта, есть возможность синхронно открывать сразу несколько створок окон. Пульт достаточно компактен и прост в управлении.</p>
        <p class="comment">Штоковые и реечные приводы устанавливаются на окна типа "люк", расположенных на крышах зданий и открывающихся на улицу.</p>
        <img src="<?=$pathToImg?>/framuga4.jpg"

        <p class="comment">Чтобы выбрать подходящий механизм дистанционного открывания окон, обратитесь в фирму «Помощь окнам» по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a>. К вам приедет наш консультант и даст конкретные рекомендации. Замеры и расчеты выполняются бесплатно, а при установке нескольких систем предоставляется скидка.</p>

        <div class="bcont bcont-email">
            <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
            <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
            <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
            <p><strong>E-mail: </strong><span id="cloakec53f2c96be2e9cb6ac4b5bca879da40"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                    document.getElementById('cloakec53f2c96be2e9cb6ac4b5bca879da40').innerHTML = '';
                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                    var path = 'hr' + 'ef' + '=';
                    var addyec53f2c96be2e9cb6ac4b5bca879da40 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                    addyec53f2c96be2e9cb6ac4b5bca879da40 = addyec53f2c96be2e9cb6ac4b5bca879da40 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                    var addy_textec53f2c96be2e9cb6ac4b5bca879da40 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakec53f2c96be2e9cb6ac4b5bca879da40').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyec53f2c96be2e9cb6ac4b5bca879da40 + '\'>'+addy_textec53f2c96be2e9cb6ac4b5bca879da40+'<\/a>';
                </script></p>
        </div>
    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
