
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Рулонные москитные сетки</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Рулонные москитные сетки			</h1>
            </div>





            <div itemprop="articleBody">
                <p><img src="<?=$pathToImg?>/rol2.jpg" alt="Рулонная москитная сетка"></p>

                <p>Мы предлагаем заказать, или отремонтировать рулонные (роллетные) москитные сетки для пластиковых, деревянных, алюминиевых окон, дверей квартир, домов, гаражей, террас и балконов.</p>

                <p>Рулонные сетки идеальны для мансардных окон, т.к. именно мансардные окна часто находятся под углом к полу помещения.</p>

                <p>Для изготовления наших рулонных сеток мы используем комплектующие от известного голландского производителя «Hunter Douglas».</p>

                <p></p><center><img src="<?=$pathToImg?>/hd.png" alt="Hunter Douglas"></center><p></p>

                <p class="comment">Изготовление рулонных сеток в Москве и МО всего 4 рабочих дня, а установка 2 часа! Мастер для замера выезжает <b>Бесплатно*, но с учётом заключения договора</b>. График приёма заявок  <b>с 8 до 22<sup>00</sup>без выходных</b>.</p>


                <h2>Особенность противомоскитного полотна</h2>
                <img src="<?=$pathToImg?>/rulon1.jpg" border="0" alt="Рулонные москитные сетки на окна" title="Рулонные москитные сетки" class="img_left">
                <p>Сетчатое противомоскитное полотно, производится из полимерного волокна серого цвета. Полотна различаются по назначению, благодаря размеру ячеек. Современные технологии позволяют изготовить москитные сетки с размером ячеек, не пропускающих мелкие фракции уличной пыли. Несмотря на тонкость плетения, противомоскитное полотно довольно прочное и износостойкое.</p>
                <p>Монтаж рулонной москитной сетки ограничивается соблюдением определенных параметров высоты и ширины:</p>

                <table class="table table-text-center width73" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Размеры готового изделия</td><td>Ширина</td><td>Высота</td></tr>
                    <tr>
                        <td>Максимальная</td>
                        <td>2000 мм</td>
                        <td>2500 мм</td>
                    </tr>
                    <tr>
                        <td>Минимальная</td>
                        <td>322 мм</td>
                        <td>500 мм</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Плюсы рулонного механизма</h2>
                <ul>
                    <li>Необязательно снимать в зимнее время года;</li>
                    <li>В корпусе короба установлены щётки. Сворачиваясь, полотно, очищается от пыли и пуха;</li>
                    <li>Под надежной защитой короба сетка лучше сохраняется, дольше служит;</li>
                    <li>Надёжность и простота в использовании.</li>
                </ul>


                <ul class="icon102 ul-inline">
                    <li class="garant">Гарантия на все виды работ и материалы 1 год.</li>
                    <li class="money101">Доступные цены и гибкая система скидок.</li>
                    <li class="good">100% выполнение гарантийных и постгарантийных обязательств.</li>
                </ul>



                <h2>Как работают рулонные сетки?</h2>
                <div class="img_right"><div id="sigplus_1001" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/images/rol_shema.gif" title=" (1/1)" data-summary=" (1/1)" data-fancybox="sigplus_1001"><img class="sigplus-preview" src="/cache/preview/15e1dac21f3de244c781e3e10691bd3a.gif" width="250" height="320" alt="" srcset="/cache/preview/80f47eaefc2967fd29d3a0834bc407ed.gif 500w, /cache/preview/15e1dac21f3de244c781e3e10691bd3a.gif 250w, /cache/thumbs/c3de768d07a39adc9e69c2234061f4c6.gif 60w" sizes="250px"></a></li></ul></div></div>
                <p>Рулонная (кассетная) москитная сетка – это специально разработанная конструкция свертывания москитного сеточного полотна в рулон, помещаемого в алюминиевый короб. Короб возможно закрепить в верхней или боковой частях оконного пространства, а разматываемое полотно двигается по боковым направляющим. Тугая пружина удерживает полотно от чрезмерного разматывания.</p>
                <p>Для того чтобы воспользоваться рулонной москитной сеткой необходимо потянуть вниз за специальную ручку. Когда край сетки достигает нижней части оконной (дверной) рамы, его фиксируют при помощи защелки. Обратное сворачивание полотна в рулон происходит легким нажатием автоматической кнопки.</p>
                <p>Движение рулонной сетки происходит по специальным направляющим, смонтированными на оконной (дверной) раме. Внутри направляющих установлены уплотнители, обеспечивающие плотное прижатие к раме и очистку. Направляющие обеспечивают поступательное движение сеточного полотна, защищают конструкцию от перегибов, продлевая срок ее службы. Алюминиевый короб также защищает сетку от воздействия осадков, особенно в зимнее время, а также декоративно скрывает скрученный рабочий рулон.</p>


                <h2>Очистка роллетной сетки</h2>
                <p>Проводить постоянную очистку рабочего полотна нет необходимости. Эту функцию выполняют щеточные уплотнители, предусмотренные конструкцией.</p>
                <p class="comment">Сетка снимается и моется в случае серьезного загрязнения или по окончании сезона активности насекомых.</p>
                <p>Последнее не всегда требуется. Загрязненные ячейки без труда очистятся обычной щеткой пылесоса, или влажной тряпкой.</p>



                <h2>Цены в зависимости от количества покупаемых изделий</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup>&nbsp;сетки при самовывозе</td>
                        <td>Цена м<sup>2</sup>&nbsp;с замером, доставкой и установкой&nbsp;в пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.</td>
                        <td>5000 руб.</td>
                        <td>6000 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>10 000 руб.</td>
                        <td>11 500 руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>15 000 руб.</td>
                        <td>17 000 руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">Цена договорная</span></td>
                    </tr>
                    </tbody>
                </table>

                <p><a href="https://vk.com/remont_okn" target="blank" rel="nofollow"><img src="/images/moskitki/01.jpg" alt=""></a></p>
                <p class="comment">* Получите скидку до 10% на москитные сетки с установкой, став участником нашей   <a href="https://vk.com/remont_okn" target="blank" class="vk_in_text">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 3 шт </b>.</p>


                <h2>Стоимость работ по замеру и установке москитных сеток</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Вид работ</td>
                        <td>Цена</td>
                    </tr>
                    <tr>
                        <td>Замер в Москва</td>
                        <td>бесплатно (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км от МКАД</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500 руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>

                <h2>Наценка на москитные сетки</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Цвет профиля</td>
                        <td>Наценка</td>
                    </tr>
                    <tr>
                        <td>Белый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 3500 к цене</td>
                    </tr>
                    </tbody>
                </table>


                <p>Мы не ответим на вопрос: "Как самому установить москитную сетку?", но мы можем изготовить ее по срочному заказу, произвести горизонтальный или вертикальный монтаж.</p>




                <div class="otz_in" id="otz">
                    <h2>Отзывы клиентов</h2>

                    <div class="otz3">
                        <div class="otz">
                            <p class="desc">На мансарде (частный дом) стоят окна под нестандартным углом к полу. Долго думали рулонную сетку ставить или рамочную. Остановились на рулонных — удобнее и эстетичнее. Установили быстро, после себя оставили чистоту. Отличные мастера, рекомендую.</p>
                            <p class="date"><span id="today1">5 февраля</span>, Максим</p>

                        </div>

                        <div class="otz">
                            <p class="desc">Еще в начале зимы рулонная сетка в спальне начала заедать, плохо открывалась и закрывалась со скрипом. Вызвала мастера, заменил какие-то детали, все смазал, теперь отлично работает.</p>
                            <p class="date"><span id="today2">21 января</span>, Мария и Антон</p>
                        </div>

                        <div class="otz">
                            <p class="desc">Обратилась в компанию для изготовления москитки, предложили реально большой выбор. Очень понравились рулонные, можно даже красить в любой цвет. Все сделали и поставили довольно быстро, спасибо.</p>
                            <p class="date"><span id="today3">17 января</span>, Дарья</p>
                        </div>
                    </div>
                    <br>
                    <a href="/otziv.html" class="blue-button">Все отзывы</a>
                </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>