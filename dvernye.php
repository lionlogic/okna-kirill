
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Москитная сетка на дверь (распашные)</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Москитная сетка на дверь (распашные)			</h1>
            </div>





            <div itemprop="articleBody">
                <img src="<?=$pathToImg?>/moskitnaj_setka_na_dveri.jpg" border="0" alt="Москитные сетки на двери" title="Дверные москитные сетки" class="img_right">
                <p>Компания «Помощь окнам» изготавливает москитные сетки на двери, пластиковые (ПВХ), деревянные, алюминиевые.</p>
                <p> Чаще для установки на даче, а также в балконных блоках, работаем в Москве, Новой Москве и всей области.</p>
                <p>Распашные сетки изготавливаются из алюминиевого каркаса окрашенного порошковой краской, сеточную ткань-фильтр, можно выбрать в зависимости от вашего предпочтения.</p>
                <p>При сборке дверной москитки, используется усиленный профиль (32 мм., 53 мм.), стыковка пластиковыми сухарями по углам и фиксация тяговыми заклепками. Рама сетки обязательно должна содержать в середине поперечную вставку (лучше две вставки) – импост, усиливающий прочность конструкции.</p>

                <p class="comment">Время изготовления – 3 монтажа 1 рабочий день! <b>Мастер на замер выезжает Бесплатно*, но с учётом заключения договора</b>. Возможны серьёзные скидки за объём. График приёма заявок  <b>с 8 до 22<sup>00</sup>без выходных.</b></p>

                <p>Москитная сетка укомплектовывается облегченными навесными петлями, пружинными доводчиками, а также магнитами, обеспечивающими надёжное закрытие. По всему периметру каркаса установлен ворс – это не оставляет щели для проникновения насекомых или мелкой пыли.</p>


                <p>Наряду с другими противомоскитными сетками, защищающими от насекомых и обеспечивающих постоянный приток свежего воздуха, имеют ряд неоспоримых преимуществ.</p>

                <p><a href="https://vk.com/remont_okn" target="blank" rel="nofollow"><img src="/images/moskitki/01.jpg" alt=""></a></p>

                <p class="comment">* Получите скидку до 10% на москитные сетки с установкой, став участником нашей   <a href="https://vk.com/remont_okn" target="blank" class="vk_in_text">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 3 шт </b>.</p>


                <h2 class="western">Особенности и преимущества дверных сеток:</h2>
                <ul>
                    <li>может комплектоваться сеточным полотном любого назначения (обычное «Fiberglass», антикошка «Pet-Screen», антипыльца «Poll-tex», антипыль, светоотражающее);</li>
                    <li>для простоты пользования – снабжаются оригинальными алюминиевыми ручками;</li>
                    <li>возможность установки в любых помещениях (общепита, больниц, детских садов) и полуоткрытых площадках (террасы, беседки);</li>
                    <li>безопасная, надёжная конструкция петель (на ваш выбор петли доводчики и обычные поворотные) <a href="/moskitnye-setki/krepezh.html#petli">подробней о крепеже москитных дверей</a>;</li>
                    <li>простота демонтажа, ухода и хранения в зимний период;</li>
                    <li>низкая цена.</li>
                </ul>

                <ul class="icon102 ul-inline">
                    <li class="garant">Гарантия на все виды работ и материалы 1 год.</li>
                    <li class="money101">Доступные цены и гибкая система скидок.</li>
                    <li class="good">100% выполнение гарантийных и постгарантийных обязательств.</li>
                </ul>


                <h2>Цены сеток на двери:</h2>

                <h3>Расценка в зависимости от количества покупаемых изделий</h3>

                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток</td>
                        <td>Цена м<sup>2</sup>&nbsp;изделия при самовывозе</td>
                        <td>Цена м<sup>2</sup>&nbsp;с замером, доставкой и установкой&nbsp;в пределах МКАД</td>
                    </tr>
                    <tr>
                        <td>1 шт.&nbsp;</td>
                        <td>4500 руб.</td>
                        <td>6000 руб.</td>
                    </tr>
                    <tr>
                        <td>2 шт.</td>
                        <td>9000 руб.</td>
                        <td>11 500 руб.*</td>
                    </tr>
                    <tr>
                        <td>3 шт.</td>
                        <td>13 500 руб.</td>
                        <td>17 000 руб.*</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td colspan="2"><span class="color-red">Цена договорная</span></td>
                    </tr>
                    </tbody>
                </table>


                <h3>Работа по замеру и установке</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Вид работ</td><td>Цена</td></tr>
                    <tr>
                        <td>Замер Москва</td>
                        <td>бесплатно (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км. от МКАД</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500 руб./шт.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500 руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500 руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>



                <h3>Наценка на специальные москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Вид полотна</td><td>Наценка за сетку до 1 кв.м.</td></tr>
                    <tr>
                        <td>Фигурные москитные сетки: арочные,треугольные</td>
                        <td>+50% к стоимости</td>
                    </tr>
                    <tr>
                        <td>"Антикошка"(Pet Screen), "Антипыль" (Saint Gobain, Канада) цвет черный</td>
                        <td>+ 1000 руб./м.кв.</td>
                    </tr>
                    <tr>
                        <td>"Антикошка" (Pet Screen) (Phifer, США) цвет светло - серый</td>
                        <td>+ 1100 руб./м.кв.</td>
                    </tr>
                    </tbody>
                </table>

                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Цвет профиля</td><td>Наценка&nbsp;</td></tr>
                    <tr>
                        <td>Белый/коричневый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 40% к цене</td>
                    </tr>
                    </tbody>
                </table>




                <div class="otz_in" id="otz">
                    <h2>Отзывы клиентов</h2>

                    <div class="otz2">
                        <div class="otz">
                            <p class="desc">Несмотря на то, что все окна у нас доме с москитными сетками, комары прошлым летом достали. Сейчас перед началом сезона заказали распашную москитку на дверь с полотном анти-кошка. Кот действительно уже пытался делать ноги, но сетка пока побеждает. Качество отличное, надеюсь это лето пройдет лучше. Спасибо компании "Помощь окнам".</p>
                            <p class="date"><span id="today1">3 февраля</span>, Людмила</p>

                        </div>

                        <div class="otz">
                            <p class="desc">Перетягивали полотно на антипыльцу на старой москитное сетке дверной. Пока все идеально, смотрится отлично, установили без проблем, цена средняя.</p>
                            <p class="date"><span id="today2">27 января</span>, Артем</p>
                        </div>

                    </div>
                    <br>

                </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>