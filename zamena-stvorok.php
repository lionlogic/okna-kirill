
<!DOCTYPE html>
<div lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
    <? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
        <style>
            .main__block li{
                background: rgba(255, 255, 255, 0.7);
                border-radius: 5px;
                padding: 10px;
            }
            .header {
                background-color: #2d3439;
            }
        </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена глухой створки на поворотно-откидную				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>


	
	<div class="container">
        <div itemprop="articleBody">
            <h2>Популярные конфигурации окон</h2>
            <ul>
                <li>Глухое остекление – это окно без открывающихся створок, (на нём нет ручек). Такое остекление обладает лучшими свойствами звуко- и теплоизоляции, но не даёт возможность вентилировать помещение, также нет возможности помыть стеклопакеты.</li>
                <li>Поворотная, поворотно-откидная, фрамужная створка – это открывающаяся часть окна, необходима для проветривания обслуживания и поддержания микроклимата помещения.</li>
            </ul>
            <p class="comment">Наши специалисты способны установить и отремонтировать створки окон любых, даже редких видов.</p>
            <img src="<?=$pathToImg?>/vidy-stvorki.jpg" alt="Виды оконных створок">
            </div>

            <h2>Плюсы модернизации глухих оконных проёмов</h2>
            <ul>
                <li>появляется возможность освежить воздух, <a class="image" title="Москитные сетки " href="/moskitnye-setki.html">установки москитных сеток</a>, повышается экологическая безопасность;</li>
                <li>вы сможете помыть окна с внешней стороны, не вызывая альпинистов;</li>
                <li>удовлетворение требованиям пожарной безопасности.</li>
            </ul>
            <p class="comment">Выезд мастера на замер  <b>Бесплатно*, но с учётом заключения договора</b>. Позвоните по телефону <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a> или <a href="#" class="zakaz">заполните форму обратной связи</a> мы вам перезвоним.</p>

            <p><a name="napr"></a></p>
            <h2>Стоимость замены створки</h2>
            <p>Цена врезки поворотной створки в глухое окно зависит от используемой профильной системы:</p>
            <ul>
                <li>"металлическая" лёгкая профильная система Provedal (Проведал), изготовление раздвижных створок - <b>7500-8000 руб. шт.</b>;</li>
                <li>замена створки пластикового окна (ПВХ)  - <b>от 12000 рублей</b> за створку (тариф зависит от цвета конструкции, формы, размера и комплектации фурнитуры;</li>
                <li>алюминиевые окна и конструкции - <b>от 35000-40000 рублей за створку</b> (стоимость определяется после точного замера).</li>
            </ul>



            <h2>Фотоотчет замены оконной створки</h2>
            <img src="<?=$pathToImg?>/4fba31ef7487691c781f6ff38b33c264.jpg" alt=""/>
            <img src="<?=$pathToImg?>/2959d98c43f27be3b9d06fb6d7b2fb1c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/bcc68f005ad6cb1bc2498c51d160adf7.jpg" alt=""/>
            <img src="<?=$pathToImg?>/815ab946dfa7d3d33d2fa5a804766391.jpg" alt=""/>
            <img src="<?=$pathToImg?>/b3c932ea199af2bad42c95c39b0f6122.jpg" alt=""/>
            <img src="<?=$pathToImg?>/04df979eb0e345a9e47bdc0d2b6ca3fd.jpg" alt=""/>
            <img src="<?=$pathToImg?>/5c875f518173c223a3b3d9ca5f0be253.jpg" alt=""/>
            <img src="<?=$pathToImg?>/9fbe07172f3f41a77f85ab4a3dafe15e.jpg" alt=""/>
            <img src="<?=$pathToImg?>/8f63c076913042877b54e7445b4a6f38.jpg" alt=""/>
            <img src="<?=$pathToImg?>/8a60a613b6546f4da4f6134ef4c2d529.jpg" alt=""/>
            <img src="<?=$pathToImg?>/72096952d3015fad762e0d88e0f75cf6.jpg" alt=""/>
            <img src="<?=$pathToImg?>/9e76dcc7cc2130a94ee064a54d94ddcf.jpg" alt=""/>



            <h2>Варианты модернизации глухих остеклений</h2>
            <p>Замена глухой створки на поворотно-откидную, поворотную, также производим добавление в оконный проем импостов (разделителей), что позволит увеличить количество открывающихся частей.</p>
            <p><img style="border: none;" src="/images/ustanovka-stvorki.jpg" alt="Замена глухой створки на поворотную"></p>
            <p>Замена конфигурации окна часто связана с перестановкой в помещении или с особенностями использования окна. В любом случае, услуги по замене глухих створок на открывающиеся очень востребованы среди жителей Москвы.</p>
            <p class="comment">Заказать замену створок вы можете по телефону <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a>. Менеджер согласует с вами время приезда инженера замера. Специалисты нашей компании произведут демонтаж и замену быстро, аккуратно и без мусора. Вся процедура не займет более 2 часов.</p>
            <h2>Контакты для связи:</h2>
            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>WhatsApp: </strong><a href="tel:{{SET PHONE}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>E-mail: </strong><span id="cloak7c7e8a504ea54e8f75da3182221aa53c"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloak7c7e8a504ea54e8f75da3182221aa53c').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy7c7e8a504ea54e8f75da3182221aa53c = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy7c7e8a504ea54e8f75da3182221aa53c = addy7c7e8a504ea54e8f75da3182221aa53c + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text7c7e8a504ea54e8f75da3182221aa53c = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak7c7e8a504ea54e8f75da3182221aa53c').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy7c7e8a504ea54e8f75da3182221aa53c + '\'>'+addy_text7c7e8a504ea54e8f75da3182221aa53c+'<\/a>';
                    </script></p>
            </div>






    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>