
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Стеклопакет с отверстием				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">


            <p>У нас вы можете заказать изготовление стеклопакета с отверстием под мобильный кондиционер или вентилятор. Качественная сборка ГОСТ, гарантия 365 дней. Работаем в Москве , Новой Москве и всей области.</p>
            <img src="<?=$pathToImg?>/16e324468f652c742cbbcf7850044bc2.jpg" alt=""/>
            <img src="<?=$pathToImg?>/4486e73163fbff87f351ae5cdf3ec550.jpg" alt=""/>
            <img src="<?=$pathToImg?>/08d5008a5e81a48b1fd7d165997e3004.jpg" alt=""/>
            <img src="<?=$pathToImg?>/16c0f7c104f086fa6943e33162f4f2bb.jpg" alt=""/>
            <img src="<?=$pathToImg?>/ef273cacbe4f1ebc79cae706386cb455.jpg" alt=""/>

            <p class="comment">Также замер, производство, доставка и установка любых других видов стеклопакетов для ваших окон и дверей.</p>
            <h2>Плюсы мобильного кондиционера со специальным стеклопакетом в комплекте:</h2>
            <ul>
                <li>нет пыли и грязи при монтаже;</li>
                <li>средняя стоимость стеклопакета с отверстием, в комлекте с мобильным кондиционером в 2-3 раза дешевле стационарной сплит-системы;</li>
                <li>не нарушается архитектурный вид здания.</li>
            </ul>
            <h2>Цена стеклопакета с отверстием под кондиционер</h2>
            <p>Стоимость стеклопакета зависит от размера, формулы, например: 24 мм,(4/16/4), 32 мм,(4/10/4/10/4), 40 мм,(4/12/4/16/4) формы и диаметра отверстия. Средняя цена за изделие начинается <strong>от 9000 рублей.</strong></p>

            <p>Еще предлагаем установку более бюджетного варианта, замена  стекла на сэндвич панель с отверстием. Установка в более сжатые сроки, 1 максимум 2 дня <strong>цена от 6500 руб.</strong> если панель белого цвета, если цветные варианты то дольше <strong>от 8500 руб.</strong>(ламинация, покраска).</p>


            <p class="comment">Звоните нашим консультантам или вызывайте замерщика бесплатно  по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a> или с помощью <a href="#" class="zakaz">формы обратной связи</a>.</p>


            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
                <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
                <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
                <p><strong>E-mail: </strong><span id="cloak0218feccf21fc7af206d038eb7fd469b"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                        document.getElementById('cloak0218feccf21fc7af206d038eb7fd469b').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy0218feccf21fc7af206d038eb7fd469b = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy0218feccf21fc7af206d038eb7fd469b = addy0218feccf21fc7af206d038eb7fd469b + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text0218feccf21fc7af206d038eb7fd469b = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak0218feccf21fc7af206d038eb7fd469b').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy0218feccf21fc7af206d038eb7fd469b + '\'>'+addy_text0218feccf21fc7af206d038eb7fd469b+'<\/a>';
                    </script></p>
            </div>


    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>

    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
