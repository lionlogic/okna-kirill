
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/akcii-i-skidki/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:05:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?><div class="main" style="background: url(https://kislorod-ok.ru/wp-content/themes/src/assets/img/actions/bg.jpg) no-repeat center; background-size: cover;">
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		<?include 'nav.php'?>		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Акции и скидки				</div>

				<h1 class="title title-main title-first">
					ПОЛУЧИТЕ ПОДАРКИ И БОНУСЫ ПРИ ЗАКАЗЕ РЕМОНТА ОКОН				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
				<form action="obrob.php" method="POST" class="main__form" onsubmit="yaCounter56025331.reachGoal('zayavka')">
				    	<input type="hidden" name="DATA[SOURCE_ID]" value="WEB">
					<input type="hidden" name="DATA[TITLE]" value="Заявка с сайта: kislorod-ok.ru - ВЫЗОВ МАСТЕРА (гл.экран)">
					<input type="hidden" name="formname" value="main-master">
					<h3 class="form__title">Срочный вызов мастера</h3>
					<div class="main__form-wrap">
						<div class="input-block-sm">
							<input type="tel" name="DATA[UF_CRM_1572883145518]" class="input input-tel" required placeholder="+7 (999) 99 99 99">
						</div>
						<div class="btn-block-sm">
							<button class="btn">
								Вызвать мастера (бесплатно)
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>


	<ul class="list-none right-bar">
	 
	<li>
		<a href="/#masterCall" class="master-open"><span class="right-text">
				вызвать мастера
			</span>
			
			<span class="right-img">
				<img src="./wp-content/themes/src/assets/img/main/master.svg" alt="">
			</span></a>
	</li>
	<li>
		<a href="index.php"><span class="right-text">
				Акции
			</span>
			
			<span class="right-img">
				<img src="./wp-content/themes/src/assets/img/main/gift.svg" alt="">
			</span></a>
	</li>
</ul>
	<!-- Акции -->
	<div class="actions section">
		
		<div class="container">
			<div class="projects__content">
				
				<div class="projects__card">
	<div class="projects__info actions__info">
		<h3 class="title projects__title">
			АКЦИЯ ПРИ ЗАКАЗЕ УСЛУГ НА СУММУ БОЛЕЕ 10000 РУБ.		</h3>
		<p>При заказе любых услуг по ремонту окон или дверей в компании «mosremokna.ru» на сумму более 10000 руб. Вы получите сумку пляжную с внешним карманом для бутылки  с нашим логотипом в подарок!</p>

		<div class="projects__info-block">
			<div class="small-title actions-mini-title">
				Условия акции:
			</div>
			<ul class="ul-list">
								<li>Количество подарков ограничено.<br />
</li>
				 				<li>Для получения подарка Вам необходимо сообщить менеджеру, что Вы хотите воспользоваться данной акцией.</li>
				 				<li>Подарок можно получить у мастера в день реализации работ.</li>
				 				<li>Акции не суммируются. При наличии нескольких акций Вы можете воспользоваться только одной.</li>
				 								
			</ul>
		</div>

		<div class="title-sm actions__title-sm">
			Спешите заказать по акции! Звоните или оставьте заявку и мы Вам перезвоним!
		</div>

		<div class="btn-actions-wrap">
			<a href="#" class="btn btn-actions gifts-open">ПРИНЯТЬ УЧАСТИЕ В АКЦИИ</a>
		</div>
	</div>

	<div class="actions__picture" style="background: url(https://kislorod-ok.ru/wp-content/uploads/2019/05/22222222222222.jpg) no-repeat center; background-size: cover;">
		<div class="btn-actions actions__trigger">
			акция!
		</div>
		<div class="gift__item-title actions__item-title">
			При заказе на сумму более 10000 руб. Вы получите сумку пляжную с внешним карманом для бутылки в подарок!		</div>

		<div class="actions__img">
			<img src="https://kislorod-ok.ru/wp-content/uploads/2019/05/700800.png" alt="">
		</div>
	</div>
</div>

				
	          <script>
	            var ajaxurl = 'https://kislorod-ok.ru/wp-admin/admin-ajax.php';
	            var true_posts = 'a:63:{s:3:"cat";i:4;s:9:"post_type";s:4:"post";s:14:"posts_per_page";i:1;s:5:"order";s:3:"ASC";s:5:"error";s:0:"";s:1:"m";s:0:"";s:1:"p";i:0;s:11:"post_parent";s:0:"";s:7:"subpost";s:0:"";s:10:"subpost_id";s:0:"";s:10:"attachment";s:0:"";s:13:"attachment_id";i:0;s:4:"name";s:0:"";s:8:"pagename";s:0:"";s:7:"page_id";i:0;s:6:"second";s:0:"";s:6:"minute";s:0:"";s:4:"hour";s:0:"";s:3:"day";i:0;s:8:"monthnum";i:0;s:4:"year";i:0;s:1:"w";i:0;s:13:"category_name";s:7:"actions";s:3:"tag";s:0:"";s:6:"tag_id";s:0:"";s:6:"author";s:0:"";s:11:"author_name";s:0:"";s:4:"feed";s:0:"";s:2:"tb";s:0:"";s:5:"paged";i:0;s:8:"meta_key";s:0:"";s:10:"meta_value";s:0:"";s:7:"preview";s:0:"";s:1:"s";s:0:"";s:8:"sentence";s:0:"";s:5:"title";s:0:"";s:6:"fields";s:0:"";s:10:"menu_order";s:0:"";s:5:"embed";s:0:"";s:12:"category__in";a:0:{}s:16:"category__not_in";a:0:{}s:13:"category__and";a:0:{}s:8:"post__in";a:0:{}s:12:"post__not_in";a:0:{}s:13:"post_name__in";a:0:{}s:7:"tag__in";a:0:{}s:11:"tag__not_in";a:0:{}s:8:"tag__and";a:0:{}s:12:"tag_slug__in";a:0:{}s:13:"tag_slug__and";a:0:{}s:15:"post_parent__in";a:0:{}s:19:"post_parent__not_in";a:0:{}s:10:"author__in";a:0:{}s:14:"author__not_in";a:0:{}s:19:"ignore_sticky_posts";b:0;s:16:"suppress_filters";b:0;s:13:"cache_results";b:1;s:22:"update_post_term_cache";b:1;s:19:"lazy_load_term_meta";b:1;s:22:"update_post_meta_cache";b:1;s:8:"nopaging";b:0;s:17:"comments_per_page";s:2:"50";s:13:"no_found_rows";b:0;}';
	            var current_page = 1;
	            var max_pages = '3';
	            </script>

			
			</div>
		</div>
	</div>
	<!-- контакты -->
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>