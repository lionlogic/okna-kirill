<!DOCTYPE html>
<html lang="ru">

<meta http-equiv="content-type" content="text.php;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>

    <meta charset="UTF-8">
    <title>РЕМОНТ ОКОН! Ремонт пластиковых окон в Москве и МО</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="wp-content/themes/src/assets/favicon/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="wp-content/themes/src/assets/favicon/favicon.php" type="image/png">

    <link rel="stylesheet" href="/css/content.css">
    <link rel="stylesheet" href="/css/front.css">
    <link rel="stylesheet" href="/css/m320.css">
    <link rel="stylesheet" href="/css/m510.css">
    <link rel="stylesheet" href="/css/m980.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/template.css">

    <!-- <link rel="stylesheet" href="assets/js/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/js/slick/slick.css">
    <link rel="stylesheet" href="assets/js/fancy/jquery.fancybox.min.css">

    <link rel="stylesheet" href="assets/css/style.css"> -->

    <!-- This site is optimized with the Yoast SEO plugin v12.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <meta name="description" content="Ремонт окон в москве и МО. Металлопластиковые, алюминиевые, деревянные окна и двери, а также светопрозрачных конструкции. Европейскиое качество."/>
    <link rel="canonical" href="index.php" />
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ремонт окон в москве и МО" />
    <meta property="og:description" content="Ремонт окон в москве и МО. Металлопластиковые, алюминиевые, деревянные окна и двери, а также светопрозрачных конструкции. Европейскиое качество." />
    <meta property="og:url" content="index.php" />
    <meta property="og:site_name" content="Ремонт и утепление окон в Москве и области" />
    <meta property="og:image" content="wp-content/uploads/2019/09/1444-1.png" />
    <meta property="og:image:secure_url" content="wp-content/uploads/2019/09/1444-1.png" />
    <meta property="og:image:width" content="357" />
    <meta property="og:image:height" content="480" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Ремонт окон в москве и МО. Металлопластиковые, алюминиевые, деревянные окна и двери, а также светопрозрачных конструкции. Европейскиое качество." />
    <meta name="twitter:title" content="Ремонт окон в москве и МО" />
    <meta name="twitter:image" content="wp-content/uploads/2019/09/1444.png" />
    <script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://kislorod-ok.ru/#website","url":"https://kislorod-ok.ru/","name":"\u0420\u0435\u043c\u043e\u043d\u0442 \u0438 \u0443\u0442\u0435\u043f\u043b\u0435\u043d\u0438\u0435 \u043e\u043a\u043e\u043d \u0432 \u041c\u043e\u0441\u043a\u0432\u0435 \u0438 \u043e\u0431\u043b\u0430\u0441\u0442\u0438","potentialAction":{"@type":"SearchAction","target":"https://kislorod-ok.ru/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://kislorod-ok.ru/#webpage","url":"https://kislorod-ok.ru/","inLanguage":"ru-RU","name":"\u0420\u0435\u043c\u043e\u043d\u0442 \u0438 \u0443\u0442\u0435\u043f\u043b\u0435\u043d\u0438\u0435 \u043e\u043a\u043e\u043d \u0432 \u041c\u043e\u0441\u043a\u0432\u0435 \u0438 \u043e\u0431\u043b\u0430\u0441\u0442\u0438! \u0417\u0432\u043e\u043d\u0438\u0442\u0435! 24/7","isPartOf":{"@id":"https://kislorod-ok.ru/#website"},"datePublished":"2019-05-21T16:16:09+00:00","dateModified":"2020-01-24T06:33:48+00:00","description":"\u0420\u0435\u043c\u043e\u043d\u0442 \u043e\u043a\u043e\u043d \u0432 \u043c\u043e\u0441\u043a\u0432\u0435 \u0438 \u041c\u041e. \u041c\u0435\u0442\u0430\u043b\u043b\u043e\u043f\u043b\u0430\u0441\u0442\u0438\u043a\u043e\u0432\u044b\u0435, \u0430\u043b\u044e\u043c\u0438\u043d\u0438\u0435\u0432\u044b\u0435, \u0434\u0435\u0440\u0435\u0432\u044f\u043d\u043d\u044b\u0435 \u043e\u043a\u043d\u0430 \u0438 \u0434\u0432\u0435\u0440\u0438, \u0430 \u0442\u0430\u043a\u0436\u0435 \u0441\u0432\u0435\u0442\u043e\u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u044b\u0445 \u043a\u043e\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u0438. \u0415\u0432\u0440\u043e\u043f\u0435\u0439\u0441\u043a\u0438\u043e\u0435 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u043e."}]}</script>
    <!-- / Yoast SEO plugin. -->

    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Ремонт и утепление окон в Москве и области &raquo; Лента комментариев к &laquo;Главная&raquo;" href="remont-okon-v-moskve/feed/index.php" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/kislorod-ok.ru\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'  href='wp-includes/css/dist/block-library/style.min6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='main-style-css'  href='wp-content/themes/src/style6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='slick-css'  href='wp-content/themes/src/assets/js/slick/slick6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='slick-theme-css'  href='wp-content/themes/src/assets/js/slick/slick-theme6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='fancySelect-css'  href='wp-content/themes/src/assets/css/fancySelect6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox-css'  href='wp-content/themes/src/assets/js/fancy/jquery.fancybox.min6619.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='wp-content/themes/src/assets/css/style4acf.css?v1_4&amp;ver=5.2.5' type='text/css' media='all' />
    <script type='text/javascript' src='wp-content/themes/src/assets/js/libs/jquery-3.3.1.min6619.js?ver=5.2.5'></script>
    <link rel='https://api.w.org/' href='wp-json/index.php' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.2.5" />
    <link rel='shortlink' href='index.php' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embed251e.json?url=https%3A%2F%2Fkislorod-ok.ru%2F" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed846b?url=https%3A%2F%2Fkislorod-ok.ru%2F&amp;format=xml" />
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "./mc.yandex.ru/metrika/tag.js", "ym");

        ym(56025331, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56025331" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <script>
        (function(w,d,u){
            var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
            var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'./cdn.bitrix24.ru/b12156794/crm/site_button/loader_1_cmid8g.js');
    </script>
    <script type="text/javascript">
        (function ct_load_script() {
            var ct = document.createElement('script'); ct.type = 'text/javascript';
            ct.src = document.location.protocol+'//cc.calltracking.ru/phone.fcfe6.9400.async.js?nc='+Math.floor(new Date().getTime()/300000);
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
        })();
    </script>
    <meta name="yandex-verification" content="4c217849a8c4337c" />
    <meta name="google-site-verification" content="s58WSFvZcNgLsgKWsn_vq-7_Ylty4PmGg8D9ErL1vlQ" />
</head>
<body>


<script>
    $(document).ready(function () {
        $(document).on('click', '.mobile__content', function () {
            $('.nav').css('display', 'unset');
        });
        $(document).on('click', '.master-open', function () {
            $('.nav').css('display', 'unset');
        });

        $(document).on('click', '.close-nav', function () {
            $('.nav').css('display', 'none');
        });

        $('.sub-menu').css('display', 'none');

        var usl = 0;
        $(document).on('click', '.openUsl', function () {

            if (usl == 0) {
                $('.sub-menu').css('display', 'block');
                usl = 1;
            } else {
                $('.sub-menu').css('display', 'none');
                usl = 0;
            }


        });

        $(document).on('click', '.main', function () {
            $('.sub-menu').css('display', 'none');
        });



        $(document).on('click', '.slidePrev', function () {
            var lef = $('.slider-ul').css('left');
            var lefta = parseInt(lef)+200;

            if (lefta > 0) {
                lefta = 0;
            }

            $('.slider-ul').css('left', lefta+'px');
        });


        $(document).on('click', '.slideNext', function () {
            var lef = $('.slider-ul').css('left');
            var lefta = parseInt(lef)-200;

            if (lefta <= -600) {
                lefta = -600;
            }

            $('.slider-ul').css('left', lefta+'px');
        });




    })
</script>
<div class="mobile-container">
    <div class="container mobile__container">
        <div class="mobile__content" style="display: flex; justify-content: space-between">
            <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 60px">
            <div class="burger__wrap">
                <div class="burger">
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nav">
    <div class="close-nav"></div>
    <div class="container-nav">
        <div class="nav__content">
            <ul id="menu-osnovnoe-menju" class="list-none nav-list">
                <li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-23"><a href="index.php" aria-current="page">Главная</a></li>
                <li id="menu-item-398" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-398"><a class="openUsl">Услуги</a>
                    <ul class="sub-menu" style="z-index: 99999999;
    opacity: 0.9;left: 40vw;border: 2px solid #f4cb67;">
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/regulirovka-plastikovykh-okon.php">Пластиковые окна - Регулировка</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-uplotnitelya.php">Пластиковые окна - Замена уплотнителя</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/ustranenie-zapotevaniya-okna.php">Пластиковые окна - Устранение запотевания</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/ustranenie-produvaniya.php">Пластиковые окна - Устранение продувания</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/remont-plastik-dverej.php">Пластиковые окна - Ремонт ПВХ дверей</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/uteplenie-plastikovykh-okon.php">Пластиковые окна - Утепление</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-stvorok.php">Пластиковые окна - Замена створки</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-stvorok.php">Пластиковые окна - Замена стеклопакета</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/germetizatsiya-i-shumoizolyatsiya-okon.php">Пластиковые окна - Герметизация и шумоизоляция</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/smena-furnitury.php">Пластиковые окна - Замена фурнитуры</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-ruchek-pvh-okon.php">Пластиковые окна - Замена ручки</a></li>

                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-uplotnitelya-wood.php">Деревянные окна - Замена уплотнителя</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/restavratsiya.php">Деревянные окна - Покраска и восстановление</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/uteplenie.php">Деревянные окна - Утепление</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-steklopaketa.php">Деревянные окна - Замена стеклопакета</a></li>

                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-uplotnitelej.php">Алюминиевые окна - Замена уплотнителя</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/restavratsiya-al.php">Алюминиевые окна - Регулировка окон</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/uteplenie-al.php">Алюминиевые окна - Утепление</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zamena-steklopaketa.php">Алюминиевые окна - Поменять стеклопакет</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/dveri.php">Алюминиевые окна - Алюминиевые двери</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/sad-zimoy.php">Алюминиевые окна - Ремонт зимнего сада</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/giesse.php">Алюминиевые окна - Giesse</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/geze.php">Алюминиевые окна - GEZE</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/savio.php">Алюминиевые окна - SAVIO</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/remont-okon-schuko.php">Алюминиевые окна - Schuco</a></li>

                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/zhalyuzi.php">Аксессуары - Жалюзи</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/moskitnye-setki.php">Аксессуары - Москитные сетки</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/otkosy-dlya-okon.php">Аксессуары - Откосы для окон</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/steklopaket-s-otverstiem.php">Аксессуары - Стеклопакет с отверстием</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/elektroprivod.php">Аксессуары - Электропривод для окон</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/framuzhnye-pribory.php">Аксессуары - Фрамужные приборы</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/tonirovka-okon.php">Аксессуары - Тонировка окон</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/mebel.php">Аксессуары - Мебель для окон</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/antikoshka.php">Москитные сетки - Антикошка</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/antipyltsa.php">Москитные сетки - Антипыльца</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/antipyltsa.php">Москитные сетки - Антипыль</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/dvernye.php">Москитные сетки - Дверные</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/rulonnye.php">Москитные сетки - Рулонные</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/plisse.php">Москитные сетки - Плиссе</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/razdvizhnye.php">Москитные сетки - Раздвижные</a></li>
                        <li id="menu-item-831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-831">  <a href="/ramochnye.php">Москитные сетки - Рамочные</a></li>
                    </ul>
                </li>
<!--                <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="o-kompanii.php">О компании</a></li>-->
<!--                <li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="korporativnym-klientam.php">Корпоративным клиентам</a></li>-->
                <li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="akcii-i-skidki.php">Акции и скидки</a></li>
                <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="kalkuljator-stoimosti.php">Калькулятор стоимости</a></li>
                <li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="/#otzivi">Отзывы</a></li>
                <li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="/#masters">Мастера</a></li>
                <!-- <li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="nashi-raboty.php">Наши работы</a></li> -->
                <!-- <li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="video-blog.php">Видео-блог</a></li> -->
                <li id="menu-item-32" class="cont-class menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-32"><a href="/#masterCall">Контакты</a>
                    <!-- <ul class="sub-menu">
                        <li id="menu-item-637" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-637"><a href="uslugi/geografy-rabot/index.php">География работ</a></li>
                    </ul> -->
                </li>
            </ul>
        </div>
    </div>
</div>
