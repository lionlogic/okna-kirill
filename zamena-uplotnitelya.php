
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена уплотнителя пластиковых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>






	
	<!-- Работаем в Москве и Подмосковье. -->
	<div class="service section" style="padding-bottom: 0;">
		<div class="container">
            <div id="component">
                <div class="item-page">
                    <div id="component">
                        <div class="item-page">
                            <div>
                                <h2 id="priznak">4 главных признака необходимости в замене уплотнителя:</h2>
                                <p>Нужно понимать, что причиной продувания, не всегда является уплотнитель, а например,&nbsp;<a title="Замена фурнитуры ПВХ окон" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/smena-furnitury.html" target="blank">поломка механизмов</a>, но всё же можно выделить 4 основных признака:</p>
                                <ul>
                                    <li>истёк срок годности &mdash; окнам примерно 5-6 лет;</li>
                                    <li>при закрытых окнах в комнату, на подоконник попадает пыль, шум с улицы, от окна дует;</li>
                                    <li>визуально определяются следы износа, мелкие трещины, геометрические деформации, потеря эластичности;</li>
                                    <li>появление наледи, конденсата или плесени на подоконниках или откосах.</li>
                                </ul>
                                <div class="img50 margall">
                                    <div id="sigplus_1002" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3">
                                        <ul>
                                            <li><img src="/files/imges/plastic_windows/9a12da84da510f15dd0446ae92bbd9e0.jpg" alt=""></li>
                                            <li><img src="/files/imges/plastic_windows/23419246edae8a2fed7e653e33fec66a.jpg" alt=""></li>
                                            <li><img src="/files/imges/plastic_windows/ad854a570c5d7baddd8ab2836aa62018.jpg" alt=""></li>
                                            <li><img src="/files/imges/plastic_windows/c4765bd65c69bc8dc6a06ebe833f5f80.jpg" alt=""></li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="comment">У нас также можно заказать&nbsp;<a title="Замена уплотнения деревянных окон" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/remont-derevyannykh-okon/zamena-uplotnitelya.html" target="blank">уплотнения для деревянных окон</a>&nbsp;или&nbsp;<a title="Замена уплотнителей алюминиевых окон" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/remont-alyuminievykh-okon/zamena-uplotnitelej.html" target="blank">установка уплотнителей на алюминиевых окнах</a>. Заводской стандарт качества&nbsp;<strong>ГОСТ 30778-2001</strong></p>
                                <a name="uslugi"></a><a name="garant"></a><a name="price"></a><a name="polomki"></a>
                                <h2>Уплотнитель для пластиковых окон цена:</h2>
                                <p class="comment"><strong>Акция!</strong>&nbsp;Европейский уплотнитель по цене российского. Спешите, количество ограниченно. Стоимость указана за 1 метр с учетом работы (демонтаж старого+монтаж нового). Всегда производим замену 2х контуров (рамный и створочный), только в таком случае, гарантирован чёткий результат.</p>
                                <table class="table table-uplot1 table-text-center" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td>Параметры</td>
                                        <td class="desctop">Марка и материал</td>
                                        <td class="desctop">Оконная система</td>
                                        <td>Цвет</td>
                                        <td class="desctop">Цена, руб/п.м.</td>
                                        <td class="desctop">Назначение</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластикового окна КВЕ-ТПЕ" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/01-227.jpg" alt="Уплотнитель ОП-01-227, ТПЕ" border="0" /></td>
                                        <td class="desctop">ОП-01-227, ТПЕ</td>
                                        <td class="desctop"><a title="Уплотнитель КВЕ" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/zamena-uplotnitelya/kbe.html" target="blank">КВЕ</a></td>
                                        <td rowspan="3">Белый, слоновая кость, светлый дуб, серый, красное дерево, коричневый</td>
                                        <td class="desctop">200&nbsp;150&nbsp;Акция!</td>
                                        <td class="desctop">внутренний на наплав</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластикового окна РЕХАУ-ТПЕ" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/04-.jpg" alt="Уплотнитель ОП-04-Р, ТПЕ REXAU" border="0" /></td>
                                        <td class="desctop">ОП-04-Р, ТПЕ</td>
                                        <td class="desctop"><a title="Уплотнитель REXAU" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/zamena-uplotnitelya/rehau.html" target="blank">REXAU</a></td>
                                        <td class="desctop">200&nbsp;150&nbsp;Акция!</td>
                                        <td class="desctop">притворный</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластикового окна ВЕКА-ТПЕ" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/05-07.jpg" alt="Уплотнитель VEKA" border="0" /></td>
                                        <td class="desctop">ОП-05-07, ТПЕ</td>
                                        <td class="desctop"><a title="Уплотнитель VEKA" href="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/zamena-uplotnitelya/veka.html" target="blank">VEKA</a></td>
                                        <td class="desctop">170&nbsp;150&nbsp;Акция!</td>
                                        <td class="desctop">универсальный</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластикового окна КВЕ-ТПЕ-ремонтный" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/06-228.jpg" alt="Утеплитель ОП-06-228, ТПЕ KBE" border="0" /></td>
                                        <td class="desctop">ОП-06-228, ТПЕ</td>
                                        <td class="desctop">КБЕ</td>
                                        <td>Серый, белый</td>
                                        <td class="desctop">200&nbsp;150&nbsp;Акция!</td>
                                        <td class="desctop">универсальный, камерный</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель для пластиковых окон REXAU-ЭПДМ" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/04-.jpg" alt=" REXAU" border="0" /></td>
                                        <td class="desctop">упл., EPDM</td>
                                        <td class="desctop">РЕХАУ</td>
                                        <td class="desctop" rowspan="6">Черный</td>
                                        <td class="desctop">150</td>
                                        <td class="desctop">активный (створочный, рамный)</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластиковых окон КВЕ-ЭПДМ-Россия" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/06-228.jpg" alt="Уплотнитель KBE" border="0" /></td>
                                        <td class="desctop">арт. 228, EPDM</td>
                                        <td class="desctop">KBE (Россия)</td>
                                        <td class="desctop">150</td>
                                        <td class="desctop">на наплав (створка, рама)</td>
                                    </tr>
                                    <tr>
                                        <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/ARTEK.jpg" alt="Упллотнитель EPDM-ARTEC" border="0" /></td>
                                        <td class="desctop">упл., EPDM</td>
                                        <td class="desctop">ARTEC</td>
                                        <td class="desctop">150</td>
                                        <td class="desctop">притвор (створка, рама)</td>
                                    </tr>
                                    <tr>
                                        <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/LG.jpg" alt="Уплотнители EPDM-LG" border="0" /></td>
                                        <td class="desctop">уплотнители EPDM</td>
                                        <td class="desctop">LG</td>
                                        <td class="desctop">150</td>
                                        <td class="desctop">подвижный (створка, рама)</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластиковых окон VEKA-ЭПДМ-Германия" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/05-07.jpg" alt=" VEKA (Германия)" border="0" /></td>
                                        <td class="desctop">упл., EPDM</td>
                                        <td class="desctop">VEKA (Германия)</td>
                                        <td class="desctop">250&nbsp;200&nbsp;Акция!</td>
                                        <td class="desctop">притвор (створка, рама)</td>
                                    </tr>
                                    <tr>
                                        <td><img title="Уплотнитель пластиковых окон КВЕ-ЭПДМ-Германия" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/foto/uplotniteli/01-227.jpg" alt=" Уплотнитель KBE (Германия)" border="0" /></td>
                                        <td class="desctop">арт. 227, EPDM</td>
                                        <td class="desctop">KBE Semperit (Германия)</td>
                                        <td class="desctop">250&nbsp;200&nbsp;Акция!</td>
                                        <td class="desctop">притворный</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a name="prop"></a><img class="img_right desctop" title="Замена уплотнителя в пластиковых окнах" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/stories/Zamena-uplotnitelj-plastikovyh-okon.jpg" alt="Замена уплотнителя в любых пластиковых окнах" border="0" />
                                <h2>Сравнительная оценка разницы материалов EPDM и TPE</h2>
                                <p>Какой уплотнитель для ПВХ окон лучше:</p>
                                <p>Утеплители для пластиковых окон изготавливаются в направлении двух основных линеек сырьевых материалов:</p>
                                <ul>
                                    <li>EPDM &ndash; резиновый уплотнитель в основе синтетический каучук (произведен методом экструзии из этилен-пропилен-диенового каучука).</li>
                                    <li>TPE &ndash; в основе модифицированный пластик (произведен методом экструзии из термопластичного эластомера). Такой уплотнительный профиль впаян/вклеен в конструкции на заводе производителе. Тариф на смену такого вида, будет дороже от 230 р/м.п</li>
                                </ul>
                                <p>Для большей наглядности мы указали их основные параметры в таблице ниже:</p>
                                <table class="table table-uplot2 table-text-center" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td>Показатели</td>
                                        <td>EPDM</td>
                                        <td>TPE</td>
                                    </tr>
                                    <tr>
                                        <td>Сохранение эластичности в диапазоне температур</td>
                                        <td>от -45&deg;С до +80&deg;С</td>
                                        <td>от -40&deg;С до +70&deg;С</td>
                                    </tr>
                                    <tr>
                                        <td>Остаточная деформация</td>
                                        <td>низкая</td>
                                        <td>средняя</td>
                                    </tr>
                                    <tr>
                                        <td>Срок службы</td>
                                        <td>свыше 10 лет</td>
                                        <td>4-5 лет</td>
                                    </tr>
                                    <tr class="desctop">
                                        <td>Устойчивость к воздействию</td>
                                        <td>озона, УФ-лучей, масел, слабых щелочей и кислот, бактерий и грибков, влаге, износу и мех. повреждениям</td>
                                        <td>УФ-лучей</td>
                                    </tr>
                                    <tr>
                                        <td>Удобство в работе</td>
                                        <td>легкий монтаж/демонтаж</td>
                                        <td>легкий монтаж</td>
                                    </tr>
                                    <tr>
                                        <td>Тепло- и звукоизоляция</td>
                                        <td>высокая</td>
                                        <td>высокая</td>
                                    </tr>
                                    <tr>
                                        <td>Эластичность</td>
                                        <td>высокая</td>
                                        <td>низкая</td>
                                    </tr>
                                    <tr>
                                        <td>Безопасность</td>
                                        <td>экологич. чистая, не горюч</td>
                                        <td>не горюч</td>
                                    </tr>
                                    <tr>
                                        <td>Оттеночная гамма</td>
                                        <td>только черный</td>
                                        <td>в ассортименте</td>
                                    </tr>
                                    <tr>
                                        <td>Цена</td>
                                        <td>от 150 рублей</td>
                                        <td>от 200 рублей</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>Так какой, уплотнитель для пластиковых окон лучше? Нашими инженерами был одобрен и выбран усиленный, камерный уплотнитель ЭПДМ ( в отличие от штатного, лепесткового, камерная резина имеет внутри полость с воздухом, которая удерживает его от слипания).</p>
                                <p class="comment">Уплотнения из (ЭПДМ) используется для герметизации стыков между рамой и створкой. Профили устанавливается вручную. Поставляется в бухтах по 400 м/п. с силиконовым покрытием, нанесенным распылителем, благодаря этому они легко монтируются в пазы. В местах склеивания уплотнения, мастер очищает поверхность от избыточного силикона. Все нарезанные уплотнения стыкуются без зазора и склеиваются цианакрилатным клеем (моментальным).</p>
                                <h3>Профессиональный подход к ремонту пластиковых окон</h3>
                                <p>Для быстрого и качественного ремонта уплотнителей, наши специалисты оснащены необходимым набором инструмента и профессиональными средствами по уходу за окнами.</p>
                                <center></center>
                                <p>Опытный мастер определит, нуждается ли окно или балконная дверь в дополнительном сервисе. Возможно, вместо замены "старых резинок", необходима смазка оконных уплотнителей, также понадобится отремонтировать замки окон, отрегулировать и т. д.</p>
                                <h2>Фотоотчёт работы наших мастеров</h2>
                                            <img src="/files/imges/plastic_windows/6bc50b5817ac6283183e9153d0fe0ea8.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/b4a8d85799d017071d01ff8fa04df65d.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/8e884c2942cbf3f82acb0e34cca32605.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/e57d32cb00c2bce0a561dd57a32f5304.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/7782bcbb5e96269697b70db2400a36e3.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/a6869c783bda033f72c656e4d34f5594.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/134bfe382c06f68b390cdd953a519eeb.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/dd74871cdb92d74b9b7b1e470e4c94a3.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/123bda573eb3642492844c14e6cfc265.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/63f2f6d477d63df20495138f366f0f10.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/6388a80513996e8028f623c0033984ea.jpg" alt=""/>
                                            <img src="/files/imges/plastic_windows/ddae3ada85d1522789745f5798e480ce.jpg" alt=""/>
                        </div>
                    </div>
                    <div class="right_bottom">
                        <div class="moduletable">
                            <div class="custom">
                                <div class="min_price"><strong>Внимание!</strong>&nbsp;В случае не заключения договора на объекте,&nbsp;<strong>выезд специалиста 800 рублей</strong>&nbsp;по Москве. Цена минимального заказа компании "Помощь окнам"&nbsp;<strong>составляет 2000 рублей</strong>. Например, при заказе регулировки 3 створок стоимостью 1500 р., будет необходимо дозаказать услуги на сумму не менее 500 руб.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
    </div>
	<!-- порядок работ ЗАГОЛОВОК СМЕНИТЬ -->
    </div>
 <? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>