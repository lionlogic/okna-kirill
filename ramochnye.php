
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_moscit.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">Рамочные москитные сетки</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>









<div class="container">
    <div itemprop="articleBody">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Рамочные москитные сетки			</h1>
            </div>





            <div itemprop="articleBody">
                <div class="raion raion-inline"><center><a href="#krepl">Виды крепления</a> <a href="#price">Цены</a> <a href="#raboty">Фото работ</a></center></div>
                <p><img class="img_right" src="/images/aksessuary/moskitka.jpg" alt="" border="0">Мы производим, продаем и устанавливаем рамочные москитные сетки на окна:</p>
                <ul>
                    <li>пластиковые,</li>
                    <li>алюминиевые,</li>
                    <li>деревянные евроокна,</li>
                    <li>алюминиево-деревянные,</li>
                </ul>
                <p>в Москве Новой Москве и всей области.&nbsp;Это самый распространенный, экономичный и экологически-чистый вариант защиты от насекомых.</p>
                <p><strong> У нас такой вид москитных сеток стоит 1000 руб/м<sup>2</sup>.</strong></p>
                <div class="img_right"><div id="sigplus_1001" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/images/aksessuary/ral.png" title=" (1/1)" data-summary=" (1/1)" data-fancybox="sigplus_1001"><img class="sigplus-preview" src="/cache/preview/4dceca2cfb9ec270935e76a660f877b7.png" width="200" height="160" alt="" srcset="/cache/preview/f362940f9df6aa155b81166f3d53b323.png 400w, /cache/preview/4dceca2cfb9ec270935e76a660f877b7.png 200w, /cache/thumbs/dd08ba7e6271096f513d79bd022ff35a.png 60w" sizes="200px"></a></li></ul></div></div>
                <p>Рамочная москитная сетка состоит из алюминиевой рамки (каркаса) и натянутого на неё сеточного полотна.</p>
                <p>Цвет рамки обычно белый, но есть возможность окрасить ее в оттенок по шкале RAL.</p>
                <p>Размер ячейки и плотность сетки может разниться в зависимости от свойств полотна (антикошка, антипыль, светоотражение).</p>
                <p><a href="https://vk.com/remont_okn" target="blank"><img src="/images/moskitki/01.jpg" alt=""></a></p>
                <p class="comment">* Получите скидку до 10% на москитные сетки с установкой, став участником нашей <a class="vk_in_text" href="https://vk.com/remont_okn" target="blank">группы Вконтакте</a>. <b>Акция распространяется на заявки с количеством антимоскиток более 6 шт </b>.</p>
                <p>Самое обычное (и недорогое) полотно FIBER GLASS вполне справляется с насекомыми, залетающими и заползающими в наш дом с улицы. Простые рамочные москитки легко устанавливаются на старые советские деревянные окна.</p>
                <p>Преимущество рамочной москитной сетки – простота ухода и снятия с окна на хранение в зимний период.</p>
                <p class="comment">Время изготовления 2-3 и монтажа – 1 рабочий день! Мастер на замер выезжает <strong>Бесплатно*, но с учётом заключения договора</strong>. График приёма заявок <b>с 8 до 22<sup>00</sup>без выходных.</b></p>
                <p>Для размера сетки более 1 м<sup>2</sup> в рамку вставляется импост (поперечная перемычка), который придает конструкции дополнительную жесткость. Мы предлагаем купить рамочные сетки для стандартных окон ПВХ или изготовить конструкцию по индивидуальным размерам.</p>
                <ul class="icon102 ul-inline">
                    <li class="garant">Гарантия на все виды работ и материалы 1 год.</li>
                    <li class="money101">Доступные цены и гибкая система скидок.</li>
                    <li class="good">100% выполнение гарантийных и постгарантийных обязательств.</li>
                </ul>
                <p><a name="price"></a></p>
                <h2>Цены на рамочные сетки</h2>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Кол-во сеток, шт.</td>
                        <td>Цена при самовывозе, руб.</td>
                        <td>Цена с доставкой и установкой на Z-крепёж, руб.</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>1000</td>
                        <td>2500 *</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>2000</td>
                        <td>3500 *</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>3000</td>
                        <td>5000 *</td>
                    </tr>
                    <tr>
                        <td>более 6</td>
                        <td>850</td>
                        <td colspan="2"><span class="color-red">Цена договорная со скидкой</span></td>
                    </tr>
                    <tr>
                        <td class="table-comment" colspan="3">Стоимость указана в рублях за 1 м<sup>2</sup> москитной сетки. Изделия меньше 0.5м<sup>2</sup> оцениваются по факту снятия размеров.</td>
                    </tr>
                    </tbody>
                </table>
                <h3>Стоимость работ по замеру и установке</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Вид работ</td>
                        <td>Цена</td>
                    </tr>
                    <tr>
                        <td>Замер в Москва</td>
                        <td><span class="color-red">бесплатно</span> (при заключения договора)</td>
                    </tr>
                    <tr>
                        <td>Замер в Подмосковье</td>
                        <td>50 руб./км. от МКАД</td>
                    </tr>
                    <tr>
                        <td>Установка сетки</td>
                        <td>500&nbsp;руб./шт.&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Установка сетки на&nbsp;<a title="Крепление сеток на металлические уголки" href="/moskitnye-setki/krepezh.html#metall" rel="alternate">металлические Z&nbsp;уголки</a></td>
                        <td>+100&nbsp;руб./за комплект&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Установка&nbsp;на&nbsp;<a title="Установка москитных сеток на плунжера" href="/moskitnye-setki/krepezh.html#plunjer" rel="alternate">плунжерное крепление</a>&nbsp;+ фетр самоклеющийся (щётка скрывающая зазоры)</td>
                        <td>+700&nbsp;руб. к стоимости 1 сетки</td>
                    </tr>
                    <tr>
                        <td>Фигурные москитные сетки, нестандартные: арочные, треугольные</td>
                        <td>+50% к стоимости</td>
                    </tr>
                    <tr>
                        <td>Доставка по Москве</td>
                        <td>500&nbsp;руб.</td>
                    </tr>
                    <tr>
                        <td>Доставка по Подмосковью</td>
                        <td>500&nbsp;руб. + 50 руб./км.</td>
                    </tr>
                    </tbody>
                </table>
                <h3>Наценка на москитные сетки</h3>
                <table class="table table-text-center" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>Цвет профиля</td>
                        <td>Наценка</td>
                    </tr>
                    <tr>
                        <td>Белый/коричневый</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Покраска в цвета RAL</td>
                        <td>+ 3500 к цене</td>
                    </tr>
                    </tbody>
                </table>
                <h2 id="krepl">Крепления для москитных сеток</h2>
                <p>Мы предлагаем множество вариантов и приспособлений для крепления москитных сеток, каждое из которых имеет свои достоинства и недостатки.</p>
                <table class="table table-text-center desctop" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Z-образные крепления</td>
                        <td>Металлические крючки</td>
                        <td>Плунжеры</td>
                    </tr>
                    <tr>
                        <td>Монтаж</td>
                        <td>простой</td>
                        <td>простой</td>
                        <td>высокая точность</td>
                    </tr>
                    <tr>
                        <td>Возможность использования полотна антикошка, антипыль</td>
                        <td>не рекомендуется</td>
                        <td>не рекомендуется</td>
                        <td>рекомендуется</td>
                    </tr>
                    <tr>
                        <td>Цена (см. прайс ниже)</td>
                        <td>низкая</td>
                        <td>низкая</td>
                        <td>высокая</td>
                    </tr>
                    <tr>
                        <td>Взаимодействие с окруж. средой</td>
                        <td>не устойчивы</td>
                        <td>не устойчивы</td>
                        <td>очень устойчивы</td>
                    </tr>
                    <tr>
                        <td>Внешний вид окна</td>
                        <td>заметны с уличной стороны</td>
                        <td>не заметны</td>
                        <td>не заметны</td>
                    </tr>
                    <tr>
                        <td>Для каких типов окон</td>
                        <td>для пластиковых и деревянных стеклопакетов с «замкнутым» внешним контуром</td>
                        <td>для пластиковых</td>
                        <td>для любых</td>
                    </tr>
                    </tbody>
                </table>
                <p>Вид крепления зависит от материала, из которого изготовлены окна и от других факторов (например, насколько глубоко рама утоплена в проеме).</p>
                <p class="comment">Внимание! По нашему опыту, большинство рамочных сеток изготовленных по самостоятельным замерам заказчиков не подходят для установки на их окна. Такие сетки нуждаются либо в переделке, либо подлежат утилизации.</p>
                <h2 id="raboty">Установка рамочных москитных сеток</h2>

                <div class="otz_in" id="otz">
                    <h2>Отзывы клиентов</h2>
                    <div class="otz3">
                        <div class="otz">
                            <p class="desc">Мастера из "Помощь окнам" устанавливали на нестандартные окна рамочные москитки. Все точно замерили и рассчитали, посоветовали усилить одну из сеток поперечной планкой. При установке все встало идеально! Очень довольна, спасибо.</p>
                            <p class="date"><span id="today1">5 февраля</span>, Екатерина</p>
                        </div>
                        <div class="otz">
                            <p class="desc">Сделали скидку за повторное обращение, когда заказывали москитное полотно "антикошка". Очень приятно и неожиданно было, отличный сервис.</p>
                            <p class="date"><span id="today2">19 января</span>, Олеся</p>
                        </div>
                        <div class="otz">
                            <p class="desc">Пришло время возвращать сетки с зимнего хранения на окна, оказалось что на коробку с ними закинули тяжелый бокс с инструментами и рамки потрескались. Заказали новые, но теперь в Помощь окнам. Пластик не в пример лучше на ощупь, к тому же их можно не снимать на зиму. Мастера вежливые и цена адекватная, что еще нужно? Советую эту организацию.</p>
                            <p class="date"><span id="today3">13 января</span>, Михаил</p>
                        </div>
                    </div>
                </div> 	</div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>