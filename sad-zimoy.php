
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Ремонт зимних садов, герметизация зенитных фонарей				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <div class="img_right"><img src="/files/imges/plastic_windows/6b910182b4268731f8cf9007bc52e264.jpg" alt=""/> </div>

            <p>Поскольку зимний сад – прозрачная конструкция, незащищенная теплыми стенами от внешней среды, качество такого сооружения должно быть безупречным. Теплопотери, протекания, промерзания и накапливание конденсата – эти факторы могут отрицательно сказаться на внешнем виде постройки, нарушат микроклимат благоприятный для растений, и сведут на нет смысл существования зимнего сада, как такового.</p>
            <p class="comment">Технически правильное строительство и поддержание светопрозрачной конструкции в должном состоянии будет залогом долгой и успешной эксплуатации зимнего сада.</p>
            <p>Мы устраняем:</p>
            <ul>
                <li>протечки конструкций зимних садов (замена уплотнительных резинок под прижимными планками, проклейка спецлентами);</li>
                <li>скопление конденсата и теплопотери (регулировка прижатия замков открывающихся секций, дверей, створок, смена старых уплотнителей притвора любых брендов AGS, NewTec, Schuco, ТАТПРОФ и т. п.);</li>
                <li>производим замену битых стеклопакетов ( меняем треснувшие, разбитые стёкла, стеклопакеты на любой высоте, в штате работают альпинисты, спецтехника).</li>
            </ul>



            <h2>Услуги по ремонту окон, реконструкции</h2>
            <p>Бывают такие ситуации, когда капитальный&nbsp;ремонт зимнего сада или срочная модернизация остаются единственным выходом, «реанимацией» сооружений, для их дальнейшей безопасной и комфортной эксплуатации.</p>
            <ul>
                <li>зимних садов;</li>
                <li>витражей;</li>
                <li>зенитных фонарей;</li>
                <li>стекло-алюминиевых &nbsp;фасадов.</li>
            </ul>
            <p class="comment">Всем своим клиентам предоставляем гарантии на проводимые работы от 1 года.</p>


        </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>