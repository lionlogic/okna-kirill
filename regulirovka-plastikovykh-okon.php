
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Регулировка пластиковых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>



	<!-- Работаем в Москве и Подмосковье. -->
	<div class="service section" style="padding-bottom: 0;">
		<div class="container">
            <div id="component">
                <div class="item-page">
                    <div>
                        <h2 id="uslugi">Мы устраняем:</h2>
                        <img class="img_right" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/girl4.png" alt="Менеджер офиса" border="0" />
                        <ul>
                            <li>поломки или провисание створок;</li>
                            <li>перекос створки пластикового окна;</li>
                            <li>неисправности замков фурнитуры известных и малоизвестных производителей евроокон;</li>
                            <li>работаем с &laquo;Лужковскими&raquo; окнами;</li>
                            <li>ремонтируем, меняем детали раздвижных конструкций окон и<a title="Регулировка пластиковых дверей" >&nbsp;дверей</a>&nbsp;Патио (Patio).</li>
                        </ul>
                        <ul>
                            <li>вы получаете гарантию, составляющую 365 дней, даже на простой ремонт;</li>
                            <li>работаем только с сертифицированными уплотнителями;</li>
                            <li>одни из самых низких цен по Москве, наличие скидок и часто проводимые акции;</li>
                            <li>мы не экономим на смазке механизмов фурнитуры, поэтому наши специалисты используют литол, а не жидкие спреи (литол долго сохраняет свои свойства, водостойкий, работоспособен при температуре &minus;40&hellip;+120 С&deg;);</li>
                            <li>работы выполняют специалисты со стажем от 5 лет, прошедшие специальное обучение, они точно знают все проблемы окон.</li>
                        </ul>
                        <h2 id="instruction">Регулировка окон ПВХ - инструкция</h2>
                        <p>С приближением холодов многие владельцы пластиковых окон задумываются об их подготовке к зиме. И это правильно, ведь своевременная регулировка пластиковых окон на зиму позволит избежать их промерзания, сэкономить электричество и продлить срок службы окна.</p>
                        <div class="img_right">
                            <div id="sigplus_1006" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3">
                                <ul>
                                    <li><img src="/files/imges/plastic_windows/036d9543ed39bcf8e1c920ad68d24790.jpg" alt=""></li>
                                </ul>
                            </div>
                        </div>
                        <p>В процессе настройки окна наши мастера:</p>
                        <ol>
                            <li>Проверяют правильность монтажа окна на перекосы.</li>
                            <li>Заменяют уплотнитель.</li>
                            <li>Проводят регулировку прижима окна.</li>
                            <li>Осматривают фурнитуру на предмет точек прижатия.</li>
                            <li>Проверяют положение эксцентриков.</li>
                            <li>Производят смазку подвижных деталей.</li>
                        </ol>
                        <p>Часто в процессе эксплуатации окон возникает необходимость в их регулировке. Например, створки начинают задевать раму, окно пропускает холод с улицы или даже появляется лед и плесень.</p>
                        <p>Вы можете&nbsp;<a class="zakaz" title="Заказать регулировку окон" >заказать услуги по регулировке окон</a>&nbsp;в нашей компании или попытаться выполнить регулировку пластиковых окон самостоятельно на зиму, прочитав наши рекомендации.</p>
                        <h4 id="instr">Скачать инструкции по регулировки фурнитуры</h4>
                        <br /><a class="blue-button pdf" title="Скачать в pdf"  target="_blank" rel="noopener">Регулировки фурнитуры Internika</a>&nbsp;<a class="blue-button pdf" title="Скачать в pdf"  target="_blank" rel="noopener">Регулировка фурнитуры Roto</a>&nbsp;
                        <table class="table table-instr" border="0">
                            <tbody>
                            <tr>
                                <td>Виды регулировки</td>
                                <td>Фото</td>
                            </tr>
                            <tr>
                                <td>
                                    <p><strong>Отладка по вертикали</strong></p>
                                    <p>&nbsp;</p>
                                    <center></center>Вертикальное выравнивание (поднимаем створку вверх) осуществляется подкручиванием винта, который располагается прямо сверху в &nbsp;нижней оконной петле. Выполнив несколько оборотов (по часовой стрелке) следует проверить, свободно ли закрывается окно. В случае &nbsp;если створка начала сильнее задевать раму, регулировку нужно будет сделать в обратном направлении, до получения удовлетворительного результата.
                                    <p>&nbsp;</p>
                                </td>
                                <td><img title="Регулировка окна вверх-вниз" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/stories/regulirovka%20okna%20vverh-vniz.jpg" alt="Регулировка окна вверх-вниз в Москве" border="0" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <p><strong>Отладка по горизонтали<br /></strong></p>
                                    <p>&nbsp;</p>
                                    <center></center>Регулировка пластиковых окон в горизонтальной плоскости происходит при помощи все того же ключа. Понемногу подкручивая винт, нужно следить за тем, чтобы окно без помех закрывалось. Если положение ухудшилось и рама стала сильнее цепляться, придется постепенно сделать регулировку в обратном направлении, пока не будет достигнуто идеальное расположение. На фото верхняя петля, регулировка нижней производится идентично.
                                    <p>&nbsp;</p>
                                </td>
                                <td><img title="Регулировка окна верх влево-вправо" src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/stories/regulirovka%20okna%20verh%20vlevo-vpravo.jpg" alt="Регулировка окна верх влево-вправо в Москве" border="0" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <p><strong>Настройка степени прижима по периметру створки</strong></p>
                                    <p>&nbsp;</p>
                                    <center></center>Герметичность окна и степень прижатия к раме можно контролировать при помощи цилиндрических запорных механизмов. Длительный срок службы резинового уплотнителя, так же, как и срок эксплуатации фурнитуры, зависит от грамотной настройки и качества уплотнителя.<br />В процессе регулирования, подкручивают цилиндры запорного механизма (цапфы), которые находится в торце оконных створок. Бывает, что выполнение этих действий не дает ощутимых результатов, в этом случае возможно требуется&nbsp;<a title="Замена уплотнителей" >замена уплотнителя окон</a>.
                                    <p>&nbsp;</p>
                                </td>
                                <td><img title="Регулировка степени прижатия " src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/stories/regulirovka%20stepeni%20prizhatij.jpg" alt="Регулировка степени прижатия окна" border="0" /></td>
                            </tr>
                            </tbody>
                        </table>
                        <h2>Регулировка фурнитуры пластикового окна КВЕ - фотоотчет</h2>
                                    <img src="/files/imges/plastic_windows/74037d4ebb65a4491db988fd4f0b1a04.jpg" alt="">
                                    <img src="/files/imges/plastic_windows/d8d02c2cac84ca9e4cf2a3b66619c031.jpg" alt="">
                                    <img src="/files/imges/plastic_windows/c38e913ccdabbe41f5b0a98adbfd094a.jpg" alt="">
                                    <img src="/files/imges/plastic_windows/31fd75344373de9c2d9769c6902af103.jpg" alt="">
                        <div class="moduletable furnitur">
                            <div class="custom furnitur">
                                <h3>Работаем с фурнитурой всех известных брендов</h3>
                                <div>
                                    <table class="table-furnitur" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/01.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Fornax в Москве" >Fornax</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/02.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Vorne в Москве" >Vorne</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/03.jpg" alt="" border="0" /></a></td>
                                            <td>MАСО</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/04.jpg" alt="" border="0" /></a></td>
                                            <td>Roto</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/05.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Winkhous в Москве" >Winkhaus</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/06.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Фурнитура Aubi" >AUBI</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/07.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Регулировка фурнитуры Internika в Москве" >Internika</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/09.jpg" alt="" border="0" /></a></td>
                                            <td>GU (Gretsch-Unitas)</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/drhahn.png" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт  регулировка фурнитуры Dr Hahn" >Dr Hahn (Доктор Хан)</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    &nbsp;
                                    <table class="table-furnitur" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/11.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Регулировка фурнитуры KALE" >Kale</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/12.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры VBH в Москве" >VBH</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/13.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры FUHR в Москве" >FUHR</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/14.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Romb" >Romb system</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/16.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Schuco в  Москве" >SCH&Uuml;CO (Шуко)</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/10.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры VHS" >VHS</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/08.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Сатурн плюс" >Сатурн Плюс</a></td>
                                        </tr>
                                        <tr>
                                            <td><img src="https://www.xn----8sb3agdedbbf7iob.xn--p1ai/images/furn-logo/15.jpg" alt="" border="0" /></a></td>
                                            <td><a title="Ремонт и регулировка фурнитуры Schuering" >Schuering</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="right_bottom">
                <div class="moduletable">
                    <div class="custom">
                        <div class="min_price"><strong>Внимание!</strong>&nbsp;В случае не заключения договора на объекте,&nbsp;<strong>выезд специалиста 800 рублей</strong>&nbsp;по Москве. Цена минимального заказа компании "Помощь окнам"&nbsp;<strong>составляет 2000 рублей</strong>. Например, при заказе регулировки 3 створок стоимостью 1500 р., будет необходимо дозаказать услуги на сумму не менее 500 руб.</div>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
        </div>
    </div>

</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>