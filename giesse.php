
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Ремонт алюминиевых окон с фурнитурой Giesse				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <img src="/files/imges/plastic_windows/logo.png" alt="Логотип фурнитуры Giesse" title="Логотип фурнитуры Giesse" class="img_right">
            <p>Компания предлагает услуг по ремонту алюминиевых окон и дверей, оснащенных фурнитурой Giesse.</p>

            <p>Мы ремонтируем различные оконные механизмы, детали от Giesse, заменяем неисправные комплектующие, проводим модернизацию и усовершенствование окон.</p>

            <p class="comment">Обратиться за бесплатной консультацией специалиста по ремонту окон или заказать выезд мастера на дом можно по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a>.</p>

            <h2>Концерн GSG и компания Giesse</h2>

            <p>Итальянский концерн GSG, включающий в себя фирму Giesse, специализируется на выпуске оконной и дверной фурнитуры. Поэтому Giesse – это всегда узкоспециализированные разработки облегченных конструкций окон, дверей, фасадов из алюминия.

            </p><h3>Ассортимент компании Giesse</h3>
            <ul>
                <li>петли, накладки, соединительные элементы;</li>
                <li>детали поворота и откидывания, в т.ч. для арочных и трапециевидных окон;</li>
                <li>решения для распашных и раздвижных и параллельно-сдвижных дверей (механические, автоматические);</li>
                <li>приводы для фрамуг;</li>
                <li>трубчатые моторедукторы для свертывания жалюзи;</li>
                <li>механизмы, обеспечивающие вентиляцию.</li>
            </ul>

            <h2>Особенности фурнитуры Giesse</h2>
            <p>Благодаря легкости в эксплуатации фурнитуры Giesse, открывать и закрывать двери и окна могут даже дети (не требуются особые усилия и нажатия). Также все конструкции обычно снабжаются предохранителями, исключающими случайное открытие.</p>

            <p class="comment">Проекты фирмы Giesse направлены на использование наименьшего количества деталей, облегчая скорость монтажа всего оконного комплекта фурнитуры.</p>


            <h2>Механизмы Giesse для обеспечения вентиляции</h2>
            <p>Запатентованные специальные устройства позволяют открывать створку окна на минимальное расстояние (до 2 мм).</p>
            <p>Узкая щель дает возможность постоянного притока свежего воздуха без изменения температуры в помещении: зимой – без потери тепла, летом – с включенным кондиционером.</p>
            <p class="comment">Важность проветривания актуальна в помещениях повышенной влажностью, для избегания появления грибка. Также механизмы пригодны для балконных дверей.</p>


            <p class="comment">Официальный сайт компании&nbsp;— www.giesse.it</p>

            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong>{{SET PHONE}}</p>
                <p><strong>WhatsApp: </strong>{{SET PHONE}}</p>
                <p><strong>E-mail: </strong><span id="cloakd589b4fd33877ec154a5e56e67b2f6e4"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloakd589b4fd33877ec154a5e56e67b2f6e4').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addyd589b4fd33877ec154a5e56e67b2f6e4 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addyd589b4fd33877ec154a5e56e67b2f6e4 = addyd589b4fd33877ec154a5e56e67b2f6e4 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_textd589b4fd33877ec154a5e56e67b2f6e4 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakd589b4fd33877ec154a5e56e67b2f6e4').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyd589b4fd33877ec154a5e56e67b2f6e4 + '\'>'+addy_textd589b4fd33877ec154a5e56e67b2f6e4+'<\/a>';
                    </script></p>
            </div>


        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>