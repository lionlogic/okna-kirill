
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Тонировка окон 				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">

        <p>У нас вы можете заказать правильную тонировку окон в квартире, офисе или другом помещении по цене от 700 рублей за квадратный метр. В стоимость включена не только сама плёнка, но и весь спектр работ и материалов, необходимых для ее грамотного нанесения.</p>

        <img src="<?=$pathToImg?>/adebb931df42d29f8d329fe657a8627d.jpg" alt=""/>
        <img src="<?=$pathToImg?>/32cb868f3ad1c4f7a3086396f6f9beef.jpg" alt=""/>
        <img src="<?=$pathToImg?>/022c5ea15254549fe53126ca8716b456.jpg" alt=""/>
        <img src="<?=$pathToImg?>/3a86a39799d8f36c2459294998e1b5c8.jpg" alt=""/>

        <h2>Какие окна можно тонировать</h2>
        <p>Как показала наша практика, тонировать можно любые стекла, в том числе и окна со стеклопакетами. На ваш выбор, окно может тонироваться как с одной стороны, так и с двух, если существует такая необходимость.</p>
        <p>Наши специалисты готовы работать на любых объектах, как жилой, так и не жилой недвижимости. При этом мы гарантируем не только низкие цены, но и качественный результат.</p>
        <h3>Сколько времени занимает тонировка</h3>
        <p>Конечно всё зависит от размеров и количества окон, но как показала практика, тонировка одного окна редко занимает более 1 часа, а самые простые работы можно выполнить и за 30 минут.</p>

        <p class="comment">Получить бесплатную консультацию или вызвать специалиста на дом вы можете заполнив <a href="#" class="zakaz">формы обратной связи</a> или по телефону <a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
        <h2 class="desctop">Цены на тонирование окон солнцезащитной плёнкой</h2>
        <div class="pricea price-terr">
            <h3>Цены на тонирование окон солнцезащитной плёнкой</h3>
            <div>
                <p><img src="<?=$pathToImg?>/ton1.jpg" alt="Тонирование окон солнцезащитной плёнкой" class="noborder"></p>
                <table class="table table-text-center">
                    <tbody>
                    <tr>
                        <td>Цвет</td>
                        <td>Пропускание видимого света</td>
                        <td>Отсечение  ИК-излучения</td>
                    </tr>
                    <tr><td colspan="3" class="grei"><h4>Цена за отрез — 700 руб/п.м. <span class="no-wrap">Цена с работой  — 900 руб/м<sup>2</sup></span></h4></td></tr>
                    <tr>
                        <td>Зеркальное серебро</td>
                        <td>15%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Зеркальное серебро</td>
                        <td>35%</td>
                        <td>65%</td>
                    </tr>

                    <tr>
                        <td>Темная не зеркальная</td>
                        <td>5%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Не зеркальная</td>
                        <td>15%</td>
                        <td>75%</td>
                    </tr>

                    <tr>
                        <td>Не зеркальная Gray</td>
                        <td>30%</td>
                        <td>60%</td>
                    </tr>

                    <tr><td colspan="3" class="grei"><h4>Цена за отрез — 900 руб/п.м. <span class="no-wrap">Цена с работой  — 1300 руб/м<sup>2</sup></span></h4></td></tr>

                    <tr>
                        <td>Зеркальная бронза</td>
                        <td>10%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Синее зеркало</td>
                        <td>10%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Зеленое зеркало</td>
                        <td>12%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Серое зеркало</td>
                        <td>10%</td>
                        <td>80%</td>
                    </tr>

                    <tr>
                        <td>Золотое зеркало</td>
                        <td>18%</td>
                        <td>70%</td>
                    </tr>

                    <tr><td colspan="3" class="grei"><h4>Цена за отрез — 1600 руб/п.м. <span class="no-wrap">Цена с работой  — 2300 руб/м<sup>2</sup></span></h4></td></tr>

                    <tr>
                        <td>Нейтральная бронза</td>
                        <td>20%</td>
                        <td>75%</td>
                    </tr>
                    </tbody>
                </table>

                <p class="comment">Тонирование солнцезащитной плёнкой очень популярная услуга, причём заказывают её не только для придания офисам особенной солидности, но и для создания комфортного микроклимата в обычных домах и квартирах.</p>
            </div>
        </div>


        <h2 class="desctop">Цены на тонирование окон декоративной плёнкой</h2>
        <div class="pricea price-terr">
            <h3>Цены на тонирование окон декоративной плёнкой</h3>
            <div>
                <p><img src="<?=$pathToImg?>/ton2.jpg" alt="Тонирование окон декоративной плёнкой" class="noborder"></p>
                <table class="table table-text-center">
                    <tbody>
                    <tr>
                        <td>Цвет пленки</td>
                        <td>Цена, руб/м<sup>2</sup></td>
                    </tr>
                    <tr>
                        <td>Белая матовая</td>
                        <td>650</td>
                    </tr>
                    <tr>
                        <td>Бронза матовая</td>
                        <td>750</td>
                    </tr>
                    <tr>
                        <td>Серая матовая</td>
                        <td>750</td>
                    </tr>
                    <tr>
                        <td>Синяя матовая</td>
                        <td>900</td>
                    </tr>
                    <tr>
                        <td>Рассеивающая 3M (эффект пескоструйной обработки)</td>
                        <td>2500</td>
                    </tr>
                    <tr>
                        <td>Белая матовая 3M (эффект химического травления)</td>
                        <td>2500</td>
                    </tr>
                    <tr>
                        <td>С морозным эффектом (рифленое матовое стекло)</td>
                        <td>1600</td>
                    </tr>
                    <tr>
                        <td>Плёнка Oracal</td>
                        <td>750</td>
                    </tr>
                    <tr>
                        <td>Плёнка Orajet</td>
                        <td>750</td>
                    </tr>
                    </tbody>
                </table>
                <p class="comment">Внимание! Указаны цены за сплошное нанесение плёнки на стекло. При необходимости вырезания на плёнке логотипов, геометрических фигур или узоров будет действовать повышающий коэффициент, значение коэффициента согласовывается индивидуально.</p>
            </div>
        </div>

        <h2 class="desctop">Цены на бронирование прозрачной плёнкой</h2>
        <div class="pricea price-terr">
            <h3>Цены на бронирование прозрачной плёнкой</h3>
            <div>
                <p>Бронирование стёкол это недорогой и очень действенный способ обезопасить ваше имущество, бизнес и детей от проникновения злоумышленников через оконные проёмы, а также разлетающихся кусков стекла.</p>
                <p><img src="<?=$pathToImg?>/ton3.jpg" alt="Бронирование окон прозрачной пленкой" class="noborder"></p>
                <table class="table table-text-center">
                    <tbody>
                    <tr>
                        <td>Тип пленки</td>
                        <td>Артикул</td>
                        <td>Цена, <span class="no-wrap">руб/м<sup>2</sup></span></td>
                    </tr>
                    <tr>
                        <td>Взрывобезопасная по классу К4, ДВ2 (112 микрон)</td>
                        <td>Safety 4 mil</td>
                        <td>650</td>
                    </tr>
                    <tr>
                        <td>Взрывобезопасная по классу К4, Р1А (200 микрон)</td>
                        <td>Safety 7 mil</td>
                        <td>750</td>
                    </tr>
                    <tr>
                        <td>Антивандальная по классу РА1 (200-250 микрон)</td>
                        <td>Safety 7 mil</td>
                        <td>750</td>
                    </tr>
                    <tr>
                        <td>Защитная по классу А1 / Р2А (300-336 микрон)</td>
                        <td>Safety 12 mil</td>
                        <td>1030</td>
                    </tr>
                    <tr>
                        <td>Защитная по классу А2 / Р3А (400-448 микрон)</td>
                        <td>Safety 4 mil</td>
                        <td>1530</td>
                    </tr>
                    <tr>
                        <td>Защитная по классу А3 / Р4А (600-672 микрон)</td>
                        <td>Safety 12 mil</td>
                        <td>1900</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <img src="<?=$pathToImg?>/8a123a462d0f62c4221d59ff3dffd9d7.jpg" alt=""/>
            <img src="<?=$pathToImg?>/01d82a50a2274de00410c80b4c8f8af2.jpg" alt=""/>
            <img src="<?=$pathToImg?>/2fd233a6cd935cf89730f46a0abee97a.jpg" alt=""/>
            <img src="<?=$pathToImg?>/d722bfa36eaa6091cfde741d5e987ea5.jpg" alt=""/>
            <img src="<?=$pathToImg?>/350e225ef8fa418ba90cfa830a75b8e5.jpg" alt=""/>
            <img src="<?=$pathToImg?>/6e177a36565c7d35b37e8f33230f7500.jpg" alt=""/>
        </div>
        <div>

            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
                <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
                <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
                <p><strong>E-mail: </strong><span id="cloak0daa0ab359552d47428f51e7a8d50200"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                        document.getElementById('cloak0daa0ab359552d47428f51e7a8d50200').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy0daa0ab359552d47428f51e7a8d50200 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy0daa0ab359552d47428f51e7a8d50200 = addy0daa0ab359552d47428f51e7a8d50200 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text0daa0ab359552d47428f51e7a8d50200 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak0daa0ab359552d47428f51e7a8d50200').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy0daa0ab359552d47428f51e7a8d50200 + '\'>'+addy_text0daa0ab359552d47428f51e7a8d50200+'<\/a>';
                    </script></p>
            </div>

        </div>



    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
