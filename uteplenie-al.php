
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Утепление алюминиевых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>







	
	<div class="container">
        <div itemprop="articleBody">
            <p>В качестве защиты помещения от продувания, холода и шума сервисная служба предлагает услуги по увеличению теплоизоляционных свойств алюминиевых окон.</p>
            <p>Мы работаем с известными зарубежными и отечественными брендами – AGS, Schuco, ТАТПРОФ, NewTec. Опытные монтажники фирмы располагают специализированным оборудованием и современными теплоизоляционными материалами для осуществления качественного утепления окон.</p>


            <h2>Работы по утеплению алюминиевых окон</h2>
            <p>Перечень работ по утеплению алюминиевых окон включает следующие основные процессы:</p>
            <ol>
                <li><h3>Утепление откосов.</h3>
                    <p>Теплоизоляция откосов. Нарушение технологии отделки откосов приводит к возникновению щелей между стеной и рамой окна. Для обеспечения плотного примыкания всех элементов окна и откосов щели заполняются монтажной пеной или базальтовым волокном.</p>
                </li>
                <li><h3>Отладка или замена фурнитуры.</h3>
                    <p>Специалисты центра сервисной службы осуществляют регулировку степени прижима оконной створки при помощи специализированного оборудования – эксцентрики. Изношенная фурнитура подлежит полной замене.</p>
                </li>
                <li><h3>Замена резинового уплотнителя.</h3>
                    <p>Потерявший эластичность уплотнительный материал заменяется новым.&nbsp; После замены производится регулировка степени прижима створки.</p>
                </li>
                <li><h3>Герметизация швов.</h3>
                    <p>Для повышения герметизации швов работники сервисной службы используют современные материалы:</p>
                    <ul>
                        <li>герметики – при застывании приобретают свойства каучука, с их помощью заделываются щели до 5 см;</li>
                        <li>монтажную пену – применяется для запенивания щелей до 35 см;</li>
                        <li>уплотнительные жгуты Изонел&nbsp;на клеевой основе – материал легко монтируется, и хорошо сохраняют тепло.</li>
                    </ul>
                </li>
            </ol>
            <p class="comment">Все материалы прошли лабораторные испытания и наделены международными сертификаты качества. Профессиональные услуги по увеличению герметизации алюминиевых окон – гарантия комфорта в Вашем доме.</p>

            <div class="bcont bcont-email">
                <p><strong>Консультант: </strong><a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a></p>
                <p><strong>Viber: </strong>{{SET PHONE}}</p>
                <p><strong>WhatsApp: </strong>{{SET PHONE}}</p>
                <p><strong>E-mail: </strong><span id="cloak39db4c380b5a3d6e422069412b74d6f8"><a href="mailto:{{SET EMAIL}}">{{SET EMAIL}}</a></span><script type="text/javascript">
                        document.getElementById('cloak39db4c380b5a3d6e422069412b74d6f8').innerHTML = '';
                        var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                        var path = 'hr' + 'ef' + '=';
                        var addy39db4c380b5a3d6e422069412b74d6f8 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                        addy39db4c380b5a3d6e422069412b74d6f8 = addy39db4c380b5a3d6e422069412b74d6f8 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                        var addy_text39db4c380b5a3d6e422069412b74d6f8 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak39db4c380b5a3d6e422069412b74d6f8').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy39db4c380b5a3d6e422069412b74d6f8 + '\'>'+addy_text39db4c380b5a3d6e422069412b74d6f8+'<\/a>';
                    </script></p>
            </div>



            <h2>Проблемы с алюминиевыми окнами - фото</h2>
            <img src="<?=$pathToImg?>/c04c6ac93c9e1459ae7838427bf95a69.jpg" alt=""/>
            <img src="<?=$pathToImg?>/e3b1e58e04ba1e13f53e70b9e6f8fa57.jpg" alt=""/>
            <img src="<?=$pathToImg?>/687878edd7efb7eea14a193655c8a346.jpg" alt=""/>
            <img src="<?=$pathToImg?>/d6c77f207ea166ed8aee271e1e84a61c.jpg" alt=""/>
            <img src="<?=$pathToImg?>/03d68bb1801b25f94d08439996edea80.jpg" alt=""/>
            <img src="<?=$pathToImg?>/723b4481f67e6e96ca0ecc1613e356fb.jpg" alt=""/>



        </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
</html>