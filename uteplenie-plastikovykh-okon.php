
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Утепление пластиковых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>




	
	<div class="container">
        <div itemprop="articleBody">
            <p class="comment">Наши специалисты работают со всеми известными оконными брендами, такими как: <strong>КВЕ (КБЕ), REHAU (РЕХАУ), VEKA (ВЕКА),</strong> THYSSEN, BRUSBOX, PROPLEX, <strong>TROCAL, SCHUCO (ШУКО),</strong> SALAMANDER, ALUPLAST, LG,&nbsp;MONTBLANC, <strong>SCHTERN, KALEVA (КАЛЕВА).</strong> Также со всеми их российскими и зарубежными аналогами. Получить бесплатную консультацию или бесплатно вызвать специалиста на дом вы можете по телефону <a href="tel:{{SET PHONE_S}} class="tel_text">{{SET PHONE}}</a></p>

            <p><a name="napr"></a></p>

            <h2>Цены на утепление и варианты решения проблем:</h2>
            <p>Указанные ниже расценки не являются публичной офертой, окончательную стоимость услуг определит мастер сервиса после проведения диагностики.</p>

            <table class="table table-3col" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>Фото</td>
                    <td>Причина обращения</td>
                    <td>Меры для устранения, цена</td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/94a2f55a067e439f857ae6f6d45c082f.jpg" alt=""/></td>
                    <td> Вашим окнам менее 5 лет, в помещение холодно и постоянные сквозняки, от створок дует.<br> В зимнее время появление наледи.</td>
                    <td>
                        <ul>
                            <li>Оконная фурнитура некорректно работает;</li>
                            <li>проводим регулировку, перевод в зимний режим, увеличение прижатия, смазка, чистка.</li>
                            <li><b>от 2000 руб.</b></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/3405e3ad6858a1cbb06637357ce38372.jpg" alt=""/></td>
                    <td> Если из закрытых окон, дверей дует, на просвет видны щели, шумоизоляция нарушена.</td>
                    <td>
                        <ul>
                            <li>Проверка размеров, на возможность брака производства, створки по ширине или высоте, сделаны меньше на 4-5 мм. ;</li>
                            <li>нарушение геометрии створок, регулировка расклиниванием стеклопакета;</li>
                            <li>Проверка монтажа рам на ошибки, допуски до 3 мм. ( рама окна или двери растянута долее 5 мм. «бочкой»)  .</li>
                            <li><b>от 2500 руб.</b></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/da9f955149910cbf1b2df6459488ec19.jpg" alt=""/></td>
                    <td>Поддувание из-под уплотнителей по краям створок. Окнам более 4-5 лет.<br></td>
                    <td>
                        <ul>
                            <li>Заменим утеплители окон ПВХ, уплотнительные резино-каучуковые профили.</li>
                            <li><b>от 150 руб/м.</b></li>
                        </ul>
                    </td></tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/2cef5df3ee1c44a6df4e7e25b11bae60.jpg" alt=""/></td>
                    <td>Продувание из-под панелей, уголков и стыков откосов.</td>
                    <td>
                        <ul>
                            <li>Не были должным образом утеплены откосы, проводим, монтаж/демонтаж, пропенка четвертей, укладка минваты, утепление внутренних откосов.</li>
                            <li><b>от 1200 м/п.</b></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/21353d6e80c00bd67ddf455f8840758a.jpg" alt=""/></td>
                    <td>Дует из стыка подоконника и рамы, в зимнее время подоконник холодный и образуется лёд.</td>
                    <td>
                        <ul>
                            <li>Проводим утепление с уличной стороны, снимаем отлив, пропениваем подставочный профиль;</li>
                            <li>Внутри герметизируем стык жидким пластиком или акриловым герметиком.</li>
                            <li><b>от 300 до 1500 м/п.</b></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/51396ab06194be5ff1922da9e039ee49.jpg" alt=""/></td>
                    <td>Стеклопакеты запотевают, установлены однокамерные.</td>
                    <td>
                        <ul>
                            <li>Установим стеклопакеты с более высокими теплосберегающими показателями;</li>
                            <li>Двухкамерные, мультифункциональные.</li>
                            <li><b>от 4000 м.</b></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?=$pathToImg?>/c4048f8e0af7e29a74ee136ce844e456.jpg" alt=""/></td>
                    <td>Откосы внутри помещения сделаны из штукатурки, и из примыкания к окну сквозит.</td>
                    <td>
                        <ul>
                            <li>Выполняем пропенку, утепление наружних откосов;</li>
                            <li>Герметизация монтажных швов.</li>
                            <li><b>от 500 м/п.</b></li>
                        </ul>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>