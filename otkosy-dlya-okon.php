
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Откосы для окон				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">

        <p>Установим откосы на окна из&nbsp;сэндвич-панелей в Москве и области. Выполним ремонт и утепление оконных откосов, быстро, качественно, без пыли, как внутри так и снаружи помещения по доступным ценам.</p>

        <img src="/files/imges/plastic_windows/otkos1.jpg" alt=""/>
        <p class="comment">Для изготовления откосов мы используем только сертифицированные утеплённые панели, толщина 10 мм. Производим установку ламинированных цветных откосов Renolit (Германия) на любые конструкции.</p>
        <h2 id="price">Откосы для окон стоимость работ:</h2>
        <p>Цены указаны в рублях за 1 погонный метр и включают в себя все материалы и работы, в том числе: монтажная пена, минеральная вата, декоративные уголки, герметизацию наружных монтажных швов.</p>
        <table class="table table-text-center">
            <tbody>
            <tr>
                <td colspan="3">Глубина откоса, мм</td>
            </tr>
            <tr class="table-comment">
                <td>Откос из сэндвич-панелей, обычный</td>
                <td>Эркерный откос из сэндвич-панелей</td>
                <td>Ламинированные откосы Renolit (Германия)</td>
            </tr>
            <tr>
                <td colspan="3" class="grei">Шириной до 200 мм</td>
            </tr>
            <tr>
                <td>1200</td>
                <td>1500</td>
                <td>от 2400</td>
            </tr>
            <tr>
                <td colspan="3" class="grei">от&nbsp;200 до&nbsp;400&nbsp;мм</td>
            </tr>
            <tr>
                <td>от 1400</td>
                <td>от 1700</td>
                <td>от 2800</td>
            </tr>
            <tr>
                <td colspan="3" class="grei">от&nbsp;400 до&nbsp;800&nbsp;мм</td>
            </tr>
            <tr>
                <td>от 1600</td>
                <td>от 2000</td>
                <td>от 3200</td>
            </tr>
            <tr>
                <td class="table-comment" colspan="3">При заказе от 20 погонных метров откосов предусмотрены скидки</td>
            </tr>
            </tbody>
        </table>

        <ul>
            <li><b>также не забывайте про подоконники они у нас с установкой 1500 м/п. ( производство Россия);</b></li>
            <li>Стоимость отделки окон арочных проемов на 25% дороже;</li>
            <li>Выезд мастера-замерщика в пределах МКАД бесплатен, при подписании договора;</li>
            <li>Выезд инженера для замера: Московская область: до 7км. от МКАД бесплатно, далее + 50 руб./км, оплачивается дополнительно;</li>
            <li>Доставка материалов включена в стоимость работ.</li>
        </ul>


        <h2>Преимущества откосов из сэндвич-панелей </h2>

        <ul>
            <li>повышенный уровень шумо-и теплоизоляции оконных конструкций;</li>
            <li>окна принимают законченный эстетически выгодный внешний вид;</li>
            <li>почти не требуют ухода и окраски, достаточно изредка протирать влажной тряпкой;</li>
            <li>выгодная стоимость в отличие от других вариантов откосов;</li>
            <li>не портятся от влаги и пыли, не выгорают, не выцветают;</li>
            <li>имеют множество цветовых решений,</li>
            <li>безопасны для здоровья, что подтверждено сертификатом соответствия.</li>
        </ul>




        <h2>Ремонт оконных откосов</h2>
        <p>Также мы готовы предложить ремонт старых (пожелтевших, поцарапанных, не герметичных) откосов:</p>
        <ul>
            <li>В стоимость работ входит, аккуратный демонтаж старых и сборка новых пластиковых откосов, обрамление краёв проёма F- образным профилем;</li>
            <li>если у вас дует из под откосов - производим перборку, разбираем пропениваем, герметизируем;</li>
            <li>отпадает силикон из стыков окно-откос - зачистка старого герметика, заделка швов, спец. герметиком, жидким пластиком;</li>
        </ul>
        <p class="comment">Стоимость ремонта откосов под ключ от 1200 руб./м.п. Все работы проводим в день обращения.</p>






        <h2>Фото-отчет с заказа по установке откосов Renolit</h2>


        <div class="bcont bcont-email">
            <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
            <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
            <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
            <p><strong>E-mail: </strong><span id="cloakd0f888187bcb3d1395909efd4c023d54"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                    document.getElementById('cloakd0f888187bcb3d1395909efd4c023d54').innerHTML = '';
                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                    var path = 'hr' + 'ef' + '=';
                    var addyd0f888187bcb3d1395909efd4c023d54 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                    addyd0f888187bcb3d1395909efd4c023d54 = addyd0f888187bcb3d1395909efd4c023d54 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                    var addy_textd0f888187bcb3d1395909efd4c023d54 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloakd0f888187bcb3d1395909efd4c023d54').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyd0f888187bcb3d1395909efd4c023d54 + '\'>'+addy_textd0f888187bcb3d1395909efd4c023d54+'<\/a>';
                </script></p>
        </div>

    </div>
</div>

</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>