
<!DOCTYPE html>
<html lang="ru">

<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_plastic.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Устраняем запотевание пластиковых окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>





	
	<div class="container">
        <div class="item-page" itemscope="" itemtype="https://schema.org/Article">
            <meta itemprop="inLanguage" content="ru-RU">


            <div class="page-header">
                <h1 itemprop="headline">
                    Устраняем запотевание пластиковых окон			</h1>
            </div>





            <div itemprop="articleBody">
                <p><img src="/files/imges/plastic_windows/810220.jpg" border="0" alt="Запотевание пластикового окна" title="Запотевание пластикового окна" class="img_right">Наша компания оказывает услуги по устранению запотевания окна. Наши мастера работают со специальным оборудованием, различными приборами (к примеру: прибор для замера влажности помещения).

                </p><p>Для успешного устранения повышенного конденсата на пластиковых окнах, мастера проведут тщательное обследование всех помещений по выявлению истинных причин появления нежелательной влаги. Исследование ведётся в нескольких направлениях:</p>
                <ul>
                    <li>проверка тяги вентиляционных шахт,</li>
                    <li>измерение соотношения подоконников к расположению отопительных устройств. Ширина подоконника не должна перекрывать батарею,</li>
                    <li>проверка качества монтажа конструкций окон.</li>
                </ul>
                <p class="comment">Благодаря точно поставленному «диагнозу» мы всегда можем сказать, почему текут пластиковые окна, а также предложить ряд действенных мер, подходящих вашему случаю.</p>
                <p>В нашей компании мы проводим следующие мероприятия «по борьбе» с запотеванием:</p>

                <h3>Герметизация окна</h3>
                <p>Для того чтобы не запотевали окна, можно попробовать просто провести их герметизацию: утепление монтажной пеной (пропенка), гидроизоляция наружных монтажных швов (Стиз-А).</p>
                <img src="/files/imges/plastic_windows/3649028e575d4c83a4ee055c14e8ad1e.jpg" alt=""/>
                <img src="/files/imges/plastic_windows/c8b5ef1b4ff30716779986fd4e99df89.jpg" alt=""/>
                <img src="/files/imges/plastic_windows/502d0448abb0b91e5442069f553b638c.jpg" alt=""/>
                <h3>Установка оконных приточных клапанов</h3>
                <img src="/files/imges/plastic_windows/cc5c1e5ff5a34d5d1c4a72f6867d61e6.jpg"/>
                <p>Климат-клапаны отвечают за обеспечение потока наружного воздуха. Эти устройства восстанавливают естественную вентиляцию, которая часто нарушается усиленной герметичностью современных окон. Клапаны снабжают автоматическими заслонками, которые самостоятельно регулируют поток воздуха, контролируя уровень влажности (приготовление пищи, пользование ванной комнатой), которая и является причиной конденсата.</p>


                <table class="table table-5col table-gall" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Фото</td><td>Марка</td><td>Краткое описание</td><td>Цена с установкой</td></tr>

                    <tr>
                        <td><img src="/files/imges/plastic_windows/b82fb5f8baa003ed32a4532adc77fe2f.jpg" alt=""/></td>
                        <td>
                            АЭРЭКО ЕММ Гигрорегулируемое оконное приточное устройство.
                            <span class="mobile"><div id="sigplus_1004" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/cache/watermarked/72aacfcae158c9f42969154401e354f8.jpg" title="Оконный приточный клапан АЭРЭКО ЕММ (1/1)" data-summary="Оконный приточный клапан АЭРЭКО ЕММ (1/1)" data-fancybox="sigplus_1004"><img class="sigplus-preview" src="/cache/preview/b82fb5f8baa003ed32a4532adc77fe2f.jpg" width="200" height="200" alt="" srcset="/cache/preview/a5d6b41dd55c252c0083800e4e92fc70.jpg 400w, /cache/preview/b82fb5f8baa003ed32a4532adc77fe2f.jpg 200w, /cache/thumbs/d3d9c062ac83ecbd278f5cff7295e3d4.jpg 60w" sizes="200px"></a></li></ul></div></span>
                        </td>
                        <td>2 способа подачи воздуха (вертикальное, наклонное). Комплектуется козырьком ASAM ( защита от воды, насекомых). Цвет белый, коричневый.</td>
                        <td>
                            <p>5600 руб.</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="/files/imges/plastic_windows/b7375a53ec39a3690d452bbd64ea9d22.jpg" alt=""/></td>
                        <td>
                            АЭРЭКО ЕНА2 Гигрорегулируемое оконное приточное устройство.
                            <span class="mobile"><div id="sigplus_1006" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/cache/watermarked/f15adb67e4b32a256b6052d6b2a0c298.jpg" title=" (1/1)" data-summary=" (1/1)" data-fancybox="sigplus_1006"><img class="sigplus-preview" src="/cache/preview/b7375a53ec39a3690d452bbd64ea9d22.jpg" width="200" height="200" alt="" srcset="/cache/preview/ebb6ac31b40fca03d6d70d49c2209c42.jpg 400w, /cache/preview/b7375a53ec39a3690d452bbd64ea9d22.jpg 200w, /cache/thumbs/1421ffaa6e02dc44a76ba6eee076fb98.jpg 60w" sizes="200px"></a></li></ul></div></span>
                        </td>
                        <td>Идеальная установка для окон и рольставен. Оснащен специальной проставкой для снижения шума (акустика до 42 дб.) и автоматическим переключателем открыт/закрыт.</td>
                        <td>
                            <p>6610 руб.</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="/files/imges/plastic_windows/4ef8b399a99f9d8be336fccbdfed48db.jpg" alt=""/></td>
                        <td>
                            АЭРЭКО АС Наружный козырек.
                            <span class="mobile"><div id="sigplus_1008" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/cache/watermarked/827647c2d1a322ac6b0b060e1aa5ccd6.jpg" title="Козырёк АЭРЭКО (1/1)" data-summary="Козырёк АЭРЭКО (1/1)" data-fancybox="sigplus_1008"><img class="sigplus-preview" src="/cache/preview/4ef8b399a99f9d8be336fccbdfed48db.jpg" width="200" height="200" alt="" srcset="/cache/preview/e55cfc91cb90aaae39fec6650010350a.jpg 400w, /cache/preview/4ef8b399a99f9d8be336fccbdfed48db.jpg 200w, /cache/thumbs/98f700afe8b33b0f8db7dad09da2d336.jpg 60w" sizes="200px"></a></li></ul></div><span></span>
</span></td>
                        <td>Защита окна от воды и поддержка оптимального потока воздуха. Внутренняя заслонка предохраняет от перепадов давления (подходит для высотных зданий). Снабжен решёткой от насекомых.</td>
                        <td>
                            <p>500 руб.</p>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="/files/imges/plastic_windows/bf6e0557b4730a900f8a3a201e9f6cda.jpg" alt=""/></td>
                        <td>
                            А-ЕММ, А-ЕНА Наружные козырьки в ассортименте.
                            <span class="mobile"><div id="sigplus_1010" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/cache/watermarked/512a692be150f0babf4fe35dd276b1ca.jpg" title="Козырьки АЭРЭКО разного назначения (1/1)" data-summary="Козырьки АЭРЭКО разного назначения (1/1)" data-fancybox="sigplus_1010"><img class="sigplus-preview" src="/cache/preview/bf6e0557b4730a900f8a3a201e9f6cda.jpg" width="200" height="200" alt="" srcset="/cache/preview/4478e60c1df6d02b946dca8c84c895d9.jpg 400w, /cache/preview/bf6e0557b4730a900f8a3a201e9f6cda.jpg 200w, /cache/thumbs/77499f187bc77dbd0fcc93994d4d1ea9.jpg 60w" sizes="200px"></a></li></ul></div></span>
                        </td>
                        <td>Козырьки акустические. Выполняют шумоизоляцию при максимальном открытии клапана до 47 дб. Два вида цвета, белый, коричневый. </td>
                        <td>
                            <p>1000 руб.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <h3>Установка вентиляционных решеток в подоконник</h3>
                <p>Решетки обеспечивают постоянную циркуляцию воздуха от батареи к оконному проему, нагревают стекло.&nbsp;</p>
                <table class="table table-dost table-gall" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr><td>Фото</td><td>Описание</td><td>Цена</td></tr>
                    <tr>
                        <td><img src="/files/imges/plastic_windows/7d9c0339df789248fc7918b783b6bfd4.jpg" alt=""/></td>
                        <td>
                            <span class="mobile">Вентиляционная решётка, встраиваемая в подоконник. </span>
                            Размер: 400 X 150 мм; Цвет: белый, коричневый; Материал: пластик.
                            <span class="mobile"><div id="sigplus_1012" class="sigplus-gallery sigplus-center sigplus-lightbox-fancybox3"><ul><li><a class="sigplus-image" href="/cache/watermarked/6fecc7cb35090c64617a12d522e7048d.jpg" title="Вентиляционная решетка (1/1)" data-summary="Вентиляционная решетка (1/1)" data-fancybox="sigplus_1012"><img class="sigplus-preview" src="/cache/preview/7d9c0339df789248fc7918b783b6bfd4.jpg" width="200" height="200" alt="" srcset="/cache/preview/74cddfa915e8b646e38e7947223360a7.jpg 400w, /cache/preview/7d9c0339df789248fc7918b783b6bfd4.jpg 200w, /cache/thumbs/ea5a5b1ab402ca8cd814ea76df7047a8.jpg 60w" sizes="200px"></a></li></ul></div></span>
                        </td>
                        <td>1500 руб.</td>
                    </tr>
                    </tbody>
                </table>
                <img src="/files/imges/plastic_windows/8f2f31dc8b95155292e21be57caf05c4.jpg" alt=""/>
                <img src="/files/imges/plastic_windows/6cea3602d5f6afaebf6748a06615b22c.jpg" alt=""/>
                <img src="/files/imges/plastic_windows/33fc80fe6461fab7f63b091bab45eff6.jpg" alt=""/>
                <h3>Установка оконного конвектора</h3>
                <p>Оконный конвектор устанавливается в стенной проем под окном. Благодаря этому образуется целенаправленный поток от батареи к окну, создается «теплая завеса» оконного блока. Наледь, конденсат, грибок навсегда «покинут» ваши окна.</p>

                <p class="comment">Плюсом установки оконного конвектора является еще и то, что рама окна, как и его уплотнитель, остаются нетронутыми.</p>
                <p>Помимо функциональной нагрузки, установка клапанов, решеток, конвектора и прочих устройств не ухудшит внешнего вида помещения. Все устройства выглядят очень стильно, выполнены учитывая современные дизайнерские разработки. Все работы мы проводим аккуратно, качественно, с гарантией.</p>
                <h3>Замена подоконников, на более подходящие по ширине</h3>
                <p>Часто вместе с новыми окнами устанавливают и новые широкие подоконники, взамен узких, старых. Порой он настолько широк, что перекрывает всю батарею. Мы выберем оптимальную ширину подоконника.</p>
                <h3>Замена ПВХ-откосов</h3>
                <p>Производим демонтаж, не качественно смонтированных откосов, утепляем, устанавливаем новые из сэндвич панелей.</p>
                <img src="/files/imges/plastic_windows/convektor.jpg" alt=""/>
                </div>
            </div>
        </div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>