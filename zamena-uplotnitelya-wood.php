
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>
<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_wood.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Замена уплотнителя деревянных окон				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>







	
	<div class="container">
        <div itemprop="articleBody">
            <p>Нужна замена уплотнителя деревянных окон? Наш сервис качественно выполняет услуги по подбору и смене уплотнительных профилей. Производим замену уплотнителей в Москве, Новой Москве и всей области, быстро, чисто, ответственно.</p>
            <p>Готовы предложить надежные уплотнители для деревянных евроокон (деревянных стеклопакетов) из вспененного и классического термоэластопласта (TПE, ТПС) от <strong>российских, польских и германских производителей</strong>.</p>

            <p class="comment">Обращайтесь к нам по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a> или <a class="zakaz" title="Заказать замену уплотнителя для деревянного окна" href="#">заполните форму обратной связи</a> и получите &nbsp;гарантию на все виды ремонтных работ — 365 дней.</p>
            <p>Заводской стандарт качества ГОСТ 30778-2001. Также осуществляем ремонт механизмов и регулировку фурнитуры окон.</p>


            <h2>Таблица цен уплотнителей для деревянных окон</h2>
            <p>У нас представлен большой выбор различных видов уплотнителей в широкой цветовой гамме: белый, слоновая кость, красное дерево, коричневый, светлый дуб, серый, черный. </p>
            <p>Стоимость указана за пагонный метр уплотнителя, с учетом выезда мастера и всех работ по установке.</p>
            <table class="table table-uplot1" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td>Параметры</td>
                    <td class="desctop">Марка и материал</td>
                    <td class="desctop">Цена,&nbsp;руб/п.м</td>
                    <td>Назначение</td>
                </tr>
                <tr class="mobile grei"><td colspan="2">Оконные уплотнители с установкой по цене <strong>от 220&nbsp;рублей/п.м. Россия, 350 р. Германия.</strong></td></tr>
                <tr>
                    <td>
<span class="mobile">
Уплотнитель ОД-01-05 TPE
</span>
                        <img src="<?=$pathToImg?>/od-01-05.jpg" alt="ОД-01-05 TPE" border="0">
                    </td>
                    <td class="desctop">Уплотнитель ОД-01-05 TPE</td>
                    <td rowspan="7" class="desctop">от 220 <strong>Россия</strong></td>
                    <td rowspan="4">под паз в 90°</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-01-06 универсальный (TPE)
</span>
                        <img src="<?=$pathToImg?>/od-01-06.jpg" alt="ОД-01-06 уплотнитель универсальный (TPE)" border="0"></td>
                    <td class="desctop">ОД-01-06 уплотнитель универсальный </td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-02-06 силиконовый уплотнитель для деревянных окон
</span>
                        <img src="<?=$pathToImg?>/od-02-06.jpg" alt="ОД-02-06  уплотнитель (TPE)" border="0"></td>
                    <td class="desctop">ОД-02-06 силиконовый уплотнитель для деревянных окон</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
Д-02-08 термоэластопласт
</span>
                        <img src="<?=$pathToImg?>/od-02-08.jpg" alt="ОД-02-08 уплотнитель" border="0"></td>
                    <td class="desctop">ОД-02-08 уплотнитель притворный</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-03-08 уплотнитель ТЭП
</span>
                        <img src="<?=$pathToImg?>/od-03-08.jpg" alt="ОД-03-08" border="0"></td>
                    <td class="desctop">ОД-03-08 Thermoplaste</td>
                    <td>для деревянных окон из евробруса</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-04-07
</span>
                        <img src="<?=$pathToImg?>/od-04-07.jpg" alt="ОД-04-07 Thermoplaste" border="0"></td>
                    <td class="desctop">ОД-04-07 ТЭП</td>
                    <td rowspan="2">под паз в 45°</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-04-08 утеплитель для деревянных окон
</span>
                        <img src="<?=$pathToImg?>/od-04-08.jpg" alt="ОД-04-08 резиновый (TPE) уплотнитель" border="0"></td>
                    <td class="desctop">ОД-04-08 утеплитель для деревянных окон</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-03-10
</span>
                        <img src="<?=$pathToImg?>/od-03-10.jpg" alt="ОД-03-10 (TPE) " border="0"></td>
                    <td class="desctop">ОД-03-10 ТЭП</td>
                    <td rowspan="7" class="desctop">350 <strong>Германия  DEVENTER</strong></td>
                    <td rowspan="3">для деревянных окон с пазом</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-06-08 TPE
</span>
                        <img src="<?=$pathToImg?>/od-06-08.jpg" alt="ОД-06-08 уплотнитель" border="0"></td>
                    <td class="desctop">ОД-06-08 </td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-07-08 термоэластопласты
</span>
                        <img src="<?=$pathToImg?>/od-07-08.jpg" alt="ОД-07-08 TPE" border="0"></td>
                    <td class="desctop">ОД-07-08 термоэластопласт
                    </td></tr>
                <tr>
                    <td>
<span class="mobile">
ОД-05-08
</span>
                        <img src="<?=$pathToImg?>/od-05-08.jpg" alt="ОД-05-08 резиновый (TPE) уплотнитель" border="0"></td>
                    <td class="desctop">ОД-05-08 </td>
                    <td>для деревянных окон с фальцем</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-05-10ТЭП
</span>
                        <img src="<?=$pathToImg?>/od-05-10.jpg" alt="ОД-05-10 утеплитель" border="0"></td>
                    <td class="desctop">ОД-05-10 Термопласт</td>
                    <td>для деревянных окон и дверей с фальцем</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-06-07
</span>
                        <img src="<?=$pathToImg?>/od-06-07.jpg" alt="ОД-06-07 утеплитель" border="0"></td>
                    <td class="desctop">ОД-06-07 TPE уплотнитель</td>
                    <td>для деревянных окон с европрофилем</td>
                </tr>
                <tr>
                    <td>
<span class="mobile">
ОД-07-07 Эластопласт
</span>
                        <img src="<?=$pathToImg?>/od-07-07.jpg" alt="ОД-07-07 (TPE) уплотнитель" border="0"></td>
                    <td class="desctop">ОД-07-07 </td>
                    <td>для деревянных окон евростандарта</td>
                </tr>

                </tbody>
            </table>




            <p class="comment">Если вы не нашли в таблице похожий уплотнитель, не расстраивайтесь звоните <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a>, инженер замерщик обязательно подберёт оригинал или аналог уплотнителя.</p>


            <h2>Замена уплотнителя нашими специалистами</h2>
            <img src="<?=$pathToImg?>/86668557d147a6d6a546c8135efa08e7.jpg" alt=""/>
            <img src="<?=$pathToImg?>/67edac64889ca9fd6b88ada0714821f7.jpg" alt=""/>
            <img src="<?=$pathToImg?>/835a256c9e4585884b1719f1b462fdf2.jpg" alt=""/>
            <img src="<?=$pathToImg?>/f784fd14922a133f5697d8f2f3d7987b.jpg" alt=""/>
        </div>
    </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>