
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_al.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
		<div class="header">
	<div class="container">
		<div class="header__content">
			<div class="logo">
				<a href="./index.php" class="logo__img">
					<img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
				</a>

				<p class="logo__text lg">
					mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
			</div>

			<div class="burger__wrap" style="display:none">
			  Меню
			  <div class="burger">
			    <span></span>
			  </div>
			</div>

			 

			 


			<div class="header__contacts">
				<div class="text-small">Есть вопрос? Звоните:</div>

				<div class="elem__block">
					<div class="img__block">
						<img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
					</div>

					<div class="elem__wrap">
											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

											<a href="tel:+79253512022" class="elem__item">+79253512022</a>

																<!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
					</div>
				</div>

				<div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
</div>
			</div>
		</div>
	</div>
</div>		 		

		<div class="container">
			<div class="main__block">
				<div class="tooldesc title-first">
					Услуги				</div>

				<h1 class="title title-main video-blog__title title-first">
					Ремонт окон и дверей с фурнитурой GEZE				</h1>

				<ul class="list-none main-list">
					<li class="free">
						Выезд мастера бесплатно!					</li>
					<li>
						Работаем без предоплаты! 					</li>
					<li>
						Диагностика в подарок!					</li>
					<li>
						Повышенная гарантия 2 года по договору!					</li>
				</ul>
			</div>
		</div>
	</div>








	
	<div class="container">
        <div itemprop="articleBody">
            <img src="<?=$pathToImg?>/glogo.png" alt="Логотип фурнитуры GEZE" title="Логотип фурнитуры GEZE" class="img_right">
            <p>Наша компания предлагает услуги по замене и ремонту пластиковых, алюминиевых окон и дверей, балконных ставен оснащенных фурнитурой GEZE.

            </p><h2>Всегда в наличии:</h2>
            <ul>
                <li>всевозможные доводчики (скрытые, скользящие, напольные маятниковые);</li>
                <li>петли;</li>
                <li>фрамужные ножницы и открыватели;</li>
                <li>фурнитура для современной оконной и дверной модернизации – комплекты дистанционного открывания фрамуги;</li>
                <li>механизмы для раздвижных дверей;</li>
                <li>цепные приводы (в т. ч. и электромеханические);</li>
                <li>монтажные консоли.</li>
            </ul>

            <p class="comment">Обратиться за бесплатной консультацией специалиста по ремонту окон или заказать выезд мастера на дом можно по телефону <a href="tel:{{SET PHONE_S}}" class="tel_text">{{SET PHONE}}</a>.</p>



            <h2>Особенности фурнитуры GEZE</h2>
            <p>Концерн GEZE обладает передовыми разработками в области раздвижных дверных систем. Фурнитура от немецкого производителя отличается своей гибкостью, и даже филигранностью изготовления. При этом способна вынести нагрузку тяжелых конструкций раздвижных дверей – металлических, деревянных, стеклянных, в том числе и промышленного и выставочного направления, спецконструкций, ворот.</p>
            <p class="comment">Основа для плавного движения в подвижных деталях – ролики или шарики, которые передвигаются по специальному трек-профилю плавно, стабильно и очень тихо.</p>

            <h3>Доводчики</h3>
            <p>Доводчики GEZE заметно выделяются среди своих аналогов других производителей. Обширный ассортимент неизменно отличает высоким качественным изготовлением от простых до сложнейших со специализированной направленностью (например, пожаробезопасными), различных по внешнему виду, оттенку, конструкции и тяговому усилию.</p>
            <p><img src="<?=$pathToImg?>/dovodchiki-geze.jpg" alt="Доводчики Geze" style="border:none; padding:0;"></p>


        </div>
</div>

<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>