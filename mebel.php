
<!DOCTYPE html>
<html lang="ru">
<? $pathToImg = '/files/imges/plastic_windows' ?>
<!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<? include $_SERVER['DOCUMENT_ROOT'].'/head.php'?>

<? $pathToImg = '/files/imges/plastic_windows' ?><div class="main" style="background: url(<?=$pathToImg?>/cat_access.jpg) no-repeat center; background-size: cover;">
    <style>
        .main__block li{
            background: rgba(255, 255, 255, 0.7);
            border-radius: 5px;
            padding: 10px;
        }
        .header {
            background-color: #2d3439;
        }
    </style>
    <div class="header">
        <div class="container">
            <div class="header__content">
                <div class="logo">
                    <a href="./index.php" class="logo__img">
                        <img src="./wp-content/uploads/2019/05/logo.png" alt="" style="width: 70%">
                    </a>

                    <p class="logo__text lg">
                        mosremokna.ru - сервис по ремонту и обслуживанию оконных систем в Москве и МО. Работаем с 2009 года 				</p>
                </div>

                <div class="burger__wrap" style="display:none">
                    Меню
                    <div class="burger">
                        <span></span>
                    </div>
                </div>






                <div class="header__contacts">
                    <div class="text-small">Есть вопрос? Звоните:</div>

                    <div class="elem__block">
                        <div class="img__block">
                            <img src="./wp-content/themes/src/assets/img/main/phone.png" alt="">
                        </div>

                        <div class="elem__wrap">
                            <a href="tel:+79253512022" class="elem__item">+79253512022</a>

                            <!-- <a href="tel:+7-977-398-92-38" class="elem__item">+7-977-398-92-38</a> -->
                        </div>
                    </div>

                    <div class="text-small">Прием заявок: с 7:00 - 23:00 без выходных<br />
                    <div class="icon-wrapper">
                        <a href="https://api.whatsapp.com/send?phone=79253512022">
                            <img src="/images/what-icon.png" class="icon">
                        </a>
                        
                        <a href="viber://add?number=79253512022">
                            <img src="/images/viber-icon.png" class="icon">
                        </a>
                        <a href="https://t.me/MosRemOkna">
                            <img src="/images/telegram-icon.png" class="icon">
                        </a>
                    </div>
                    <style>
                        .icon-wrapper {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                        }
                        .icon-wrapper img {
                            margin: 5px 5px;
                            width: 32px !important;
                        }
                    </style>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main__block">
            <div class="tooldesc title-first">
                Услуги				</div>

            <h1 class="title title-main video-blog__title title-first">
                Мебель для балкона под ключ				</h1>

            <ul class="list-none main-list">
                <li class="free">
                    Выезд мастера бесплатно!					</li>
                <li>
                    Работаем без предоплаты! 					</li>
                <li>
                    Диагностика в подарок!					</li>
                <li>
                    Повышенная гарантия 2 года по договору!					</li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div itemprop="articleBody">

        <p>Компания "Помощь окнам" предлагает изготовление и установку: шкафов, стеллажей, полок, тумб, столов и стульев. Используем качественные материалы, создаем предметы интерьера по приятным ценам, учитывая все особенности балконов и лоджий.</p>

        <p><img class="noborder_img" src="<?=$pathToImg?>/20.jpg" alt="Мебель на балкон недорого"></p>



        <h2>Мебель для балкона недорого</h2>
        <p>Найти достаточно компактную, но при этом удобную и эргономичную мебель для балкона порой сложно даже в Москве. Чаще всего предлагаемые варианты не подходят по габаритам, цвету или качеству. Наполнение шкафов — стандартное, при том, что каждая семья использует их по – разному, кто-то убирает на лоджию горные лыжи, а кто-то теплые одеяла.</p>
        <p>Поэтому мы рекомендуем изготавливать мебель для балкона на заказ: это экономично и поможет наиболее рационально и эффективно использовать столь небольшое пространство.</p>

        <h3>Средняя цена на мебель для хранения:</h3>
        <ul>
            <li>Большой шкаф до потолка — от 16000 рублей.</li>
            <li>Маленький шкаф — от 10000 рублей.</li>
        </ul>
        <p class="comment">Стоимость включает в себя замер, изготовление, доставку, сборку и установку изделия.</p>


        <img src="<?=$pathToImg?>/36d91c8dba2ee1acb17ff07f53d65a32.jpg" alt=""/>
        <img src="<?=$pathToImg?>/93af2aa83cd54961d7207e5c9fda53ab.jpg" alt=""/>
        <img src="<?=$pathToImg?>/a5163bd040fd3e69b71d17a1c0e5b11d.jpg" alt=""/>
        <img src="<?=$pathToImg?>/be9e2a8bf48b67948e1b9f3fce73688b.jpg" alt=""/>

        <h2>Мебель на балкон на заказ</h2>
        <p>Имея большой опыт, наши специалисты помогут грамотно распланировать пространство, превратив его в эргономичную зону для отдыха или вместительную и функциональную кладовую. Мы поможем превратить небольшое пространство лоджии в универсальное место хранения, удобную рабочую зону или даже уголок для отдыха и чаепитий.</p>

        <p>Вся предлагаемая мебель собирается с учетом размеров и конструктивных особенностей вашего помещения: это позволяет использовать нестандартные ниши, всю высоту помещения и добавлять креативных решений для сохранения свободного пространства. </p>
        <p class="comment">Мы проектируем и воплощаем как корпусные модели мебели, так и складные.</p>

        <p>При проектировании будущей мебели вы сможете:</p>
        <ul>
            <li>самостоятельно определить используемый материал, фурнитуру и наполнение шкафов;</li>
            <li>управлять конечной ценой, комбинируя качественные комплектующие различных производителей;</li>
            <li>получить грамотную консультацию наших специалистов.</li>
        </ul>


        <h2>Бесплатно вызвать консультанта-замерщика</h2>
        <div class="bcont bcont-email">
            <p><strong>Консультант: </strong><a href="tel:+74997558729" class="tel_text">+7 (499) 755-87-29</a></p>
            <p><strong>Viber: </strong>+7 (926) 777-96-26</p>
            <p><strong>WhatsApp: </strong>+7 (926) 777-96-26</p>
            <p><strong>E-mail: </strong><span id="cloak9b1cc2496b5d9631689e1c5762fae0b6"><a href="mailto:tohelp.ok@gmail.com">tohelp.ok@gmail.com</a></span><script type="text/javascript">
                    document.getElementById('cloak9b1cc2496b5d9631689e1c5762fae0b6').innerHTML = '';
                    var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
                    var path = 'hr' + 'ef' + '=';
                    var addy9b1cc2496b5d9631689e1c5762fae0b6 = 't&#111;h&#101;lp.&#111;k' + '&#64;';
                    addy9b1cc2496b5d9631689e1c5762fae0b6 = addy9b1cc2496b5d9631689e1c5762fae0b6 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
                    var addy_text9b1cc2496b5d9631689e1c5762fae0b6 = 't&#111;h&#101;lp.&#111;k' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';document.getElementById('cloak9b1cc2496b5d9631689e1c5762fae0b6').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy9b1cc2496b5d9631689e1c5762fae0b6 + '\'>'+addy_text9b1cc2496b5d9631689e1c5762fae0b6+'<\/a>';
                </script></p>
        </div>
        <h2>Сборка мебели на заказ - фотоотчет</h2>
        <img src="<?=$pathToImg?>/ececeae35178e8f7625adac84ddefa32.jpg" alt=""/>
        <img src="<?=$pathToImg?>/b7bbd0d130bc2a8a3b013a04b87c55c7.jpg" alt=""/>
        <img src="<?=$pathToImg?>/537bd101e7fec0e2300f7b61bf2e043e.jpg" alt=""/>
        <img src="<?=$pathToImg?>/59826cb88461da678f0c3103a8f09af9.jpg" alt=""/>
        <img src="<?=$pathToImg?>/6477e74427f716ecb1563b3f91263bff.jpg" alt=""/>
        <img src="<?=$pathToImg?>/9a7cb4a58b8127bfbbc170836cf49688.jpg" alt=""/>
        <img src="<?=$pathToImg?>/3bac33d3d00b6ada83b3730feda82eea.jpg" alt=""/>
        <img src="<?=$pathToImg?>/e4caf9469604a5beb3bcf7b74c4326e4.jpg" alt=""/>
        <img src="<?=$pathToImg?>/84fe4fc6d1108bf7d43880d914bb82b9.jpg" alt=""/>
        <img src="<?=$pathToImg?>/a81164b3e38ccce089f03c666ff4be7d.jpg" alt=""/>
        <img src="<?=$pathToImg?>/d465c623d3971be4f70ff129650dd386.jpg" alt=""/>
        <img src="<?=$pathToImg?>/9f04aa441a2834d378c55b4f007fb083.jpg" alt=""/>
        <img src="<?=$pathToImg?>/1f092f5bec9d39a6a9e75e4a52c6b030.jpg" alt=""/>
        <img src="<?=$pathToImg?>/4e26f19a7f63ec83fc640ad0af8d37fe.jpg" alt=""/>
        <img src="<?=$pathToImg?>/139035cf820cc43e7ab994394b4e5494.jpg" alt=""/>
        <img src="<?=$pathToImg?>/d0bd22414251dd6f376dc543730153ba.jpg" alt=""/>
    </div>
</div>
</div>
<? include $_SERVER['DOCUMENT_ROOT'].'/footer.php';?>
    <!-- Mirrored from kislorod-ok.ru/uslugi/uteplenie-plastikovyh-okon/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 14:07:10 GMT -->

